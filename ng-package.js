module.exports = {
  deleteDestPath: !process.env.WATCH_MODE,
  lib: {
    entryFile: 'src/public-api.ts',
    cssUrl: 'inline',
    umdModuleIds: {
      // vendors
      tslib: 'tslib',
      lodash: '_',

      // local
      '@avans/package-1': 'inzicht.package-1',
      '@avans/package-1/testing': 'inzicht.package-1.testing'
    }
  },
  whitelistedNonPeerDependencies: ['.']
}

import { Component, ChangeDetectionStrategy, OnInit, Output, EventEmitter, Input } from '@angular/core'
import { FormGroup, FormBuilder, Validators } from '@angular/forms'

@Component({
  selector: 'lib-dropdown-selector',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: `
    <!-- ------------ Korte beschrijving --------------------------- -->
    <form [formGroup]="form">
      <div class="row">
        <div class="form-group col-xs-8">
          <label for="dropdownSelection">{{ title }}</label>
          <select formControlName="dropdownSelection" class="form-control dropdown-selection" required>
            <option *ngFor="let option of availableOptions" [value]="option">
              {{ option.fullName }}
            </option>
          </select>
        </div>
      </div>
    </form>
  `
})
export class DropdownSelectorComponent implements OnInit {
  @Input() availableOptions!: any[]
  // { nr: 2016, fullName: '2016-2017' },
  @Input() title!: string
  @Output() selectedOption = new EventEmitter<any>()

  form: FormGroup

  constructor(private fb: FormBuilder) {}

  get dropdownSelection() {
    return this.form.get('dropdownSelection')
  }

  ngOnInit(): void {
    this.form = this.fb.group({
      dropdownSelection: this.fb.array(
        this.availableOptions.map(options => options.map((option: any) => this.fb.control(option)))
      )
    })

    this.dropdownSelection.valueChanges.subscribe(value => {
      this.selectedOption.emit(value)
      this.form.markAsDirty()
    })
  }
}

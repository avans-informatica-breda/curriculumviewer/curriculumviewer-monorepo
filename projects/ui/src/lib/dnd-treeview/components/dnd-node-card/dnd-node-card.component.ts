import { Component, Input, ChangeDetectionStrategy } from '@angular/core'
import { DndNode } from '../../models'

@Component({
  selector: 'lib-dnd-node-card',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: `
    <div class="dnd__card__container">
      <p class="dnd__card__title">{{ item.name }}</p>
      <div class="dnd__card__content">
        <lib-dnd-node-card *ngFor="let childNode of item.childNodes" [item]="childNode"></lib-dnd-node-card>
      </div>
      <!-- Only leaf nodes have a dnd-drop container. Leafnodes have no childnodes. -->
      <div class="dnd__drop__container" *ngIf="!item.childNodes || item.childNodes.length === 0">
        <div
          class="dnd__drop__area"
          [dragula]="'BOKS_CONTENT'"
          [dragulaModel]="item.contentNodes"
          [id]="item.id"
        >
          <div *ngIf="!item.contentNodes || item.contentNodes.length === 0">
            <p>No content yet!</p>
          </div>
          <lib-dnd-content-card *ngFor="let content of item.contentNodes" [content]="content">
          </lib-dnd-content-card>
        </div>
        <lib-dnd-content-card-add [parentId]="item.id"></lib-dnd-content-card-add>
      </div>
    </div>
  `,
  styleUrls: ['dnd-node-card.component.scss', '../../../assets/css/dragula.css']
})
export class DndNodeCardComponent {
  @Input() item: DndNode
}

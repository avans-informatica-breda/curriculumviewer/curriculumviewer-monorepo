import { Component, Output, EventEmitter, Input } from '@angular/core'
import { FormControl, FormGroup } from '@angular/forms'
import { DnDService } from '../../../services/dnd.service'

@Component({
  selector: 'lib-dnd-content-card-add',
  template: `
    <div class="dnd__content__container">
      <div class="dnd__content__name">
        <form [formGroup]="addForm">
          <input
            class="dnd__content__input"
            placeholder="Add boks content"
            formControlName="contentName"
            (keyup.enter)="onSubmit()"
          />
        </form>
      </div>
    </div>
  `,
  styleUrls: ['../dnd-content-card.component.scss', 'dnd-content-card-add.component.scss']
})
export class DnDContentCardAddComponent {
  @Input() parentId!: number

  constructor(private dndService: DnDService) {}

  addForm = new FormGroup({
    contentName: new FormControl()
  })

  onSubmit() {
    this.dndService.onContentAdded.next({
      name: this.addForm.get('contentName').value,
      parentId: this.parentId
    })
    this.addForm.reset()
  }
}

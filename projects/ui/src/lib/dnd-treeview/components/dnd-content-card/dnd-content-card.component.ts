import { Component, Input, ChangeDetectionStrategy, Output, EventEmitter } from '@angular/core'
import { DndContent } from '../../models/dnd-content.model'

@Component({
  selector: 'lib-dnd-content-card',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: `
    <div class="dnd__content__container">
      <div class="dnd__content__name">
        <p>{{ content.name }}</p>
      </div>
      <!-- ToDo: add statusicon per contentitem
      <div [id]="content.id" class="dnd-content__dnd-status" [class.success]="">
        <i class="fa fa-check "></i>
      </div>
      -->
    </div>
  `,
  styleUrls: ['dnd-content-card.component.scss', '../../../assets/css/dragula.css']
})
export class DndContentCardComponent {
  @Input() content: DndContent
}

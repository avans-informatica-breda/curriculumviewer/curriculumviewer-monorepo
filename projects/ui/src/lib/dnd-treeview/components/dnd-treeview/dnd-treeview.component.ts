import {
  Component,
  OnInit,
  Input,
  ChangeDetectionStrategy,
  Output,
  EventEmitter,
  OnDestroy
} from '@angular/core'
import { DndTree } from '../../models/dnd-tree.model'

import { Subscription } from 'rxjs'
import { DragulaService } from 'ng2-dragula'
import { DndContent } from '../../models'
import { DnDService } from '../../services/dnd.service'

@Component({
  selector: 'lib-dnd-treeview',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: `
    <div class="dnd__tree__container">
      <div *ngFor="let node of tree">
        <lib-dnd-node-card [item]="node"></lib-dnd-node-card>
      </div>
      <!-- <div [dragula]="'BOKS_CONTENT'" name="WASTE_BIN" class="dnd__wastebin__container">Wastebin</div> -->
    </div>
  `,
  styleUrls: ['dnd-treeview.component.scss', '../../../assets/css/dragula.css']
})
export class DndTreeViewComponent implements OnInit, OnDestroy {
  @Input() tree: DndTree

  @Output() itemDropped = new EventEmitter<{ item: DndContent; targetNodeId: number }>()
  @Output() itemDeleted = new EventEmitter<{ item: DndContent; targetNodeId: number }>()
  @Output() contentAdded = new EventEmitter<{ name: string; parentId: number }>()

  // RxJS Subscription is an excellent API for managing many unsubscribe calls.
  subs = new Subscription()

  constructor(private dndService: DnDService, private dragulaService: DragulaService) {}

  ngOnInit() {
    this.subs.add(
      this.dragulaService.dropModel('BOKS_CONTENT').subscribe(({ item, target }) => {
        // console.log('Dropping', item.name, 'id =', item.id, ' on target.id', target.id)
        this.itemDropped.emit({ item, targetNodeId: parseInt(target.id, 10) })
      })
    )
    this.subs.add(
      this.dragulaService.over('BOKS_CONTENT').subscribe(({ el, container }) => {
        console.log('over', container)
        // this.addClass(container, 'ex-over')
      })
    )

    this.subs.add(this.dndService.onContentAdded.subscribe(content => this.contentAdded.emit(content)))
  }

  ngOnDestroy() {
    // destroy all the subscriptions at once
    this.subs.unsubscribe()
  }
}

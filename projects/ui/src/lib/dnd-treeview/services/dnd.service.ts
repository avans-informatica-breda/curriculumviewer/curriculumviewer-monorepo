import { Injectable } from '@angular/core'
// import { map, catchError } from 'rxjs/operators'
import { Subject } from 'rxjs'

@Injectable({
  providedIn: 'root'
})
export class DnDService {
  public onContentAdded = new Subject<{ name: string; parentId: number }>()

  constructor() {}
}

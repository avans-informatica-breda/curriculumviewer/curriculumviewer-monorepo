export class FilterValue {
  // public constructor(public id: number, public name: string, public selected = true) {}

  public id!: number
  public name!: string
  public selected!: boolean

  public constructor(values: any = {}) {
    this.selected = values.selected || true
    Object.assign(this, values)
  }
}

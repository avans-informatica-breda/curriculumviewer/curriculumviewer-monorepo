import { Component, Input, Output, OnInit, ChangeDetectionStrategy, EventEmitter } from '@angular/core'
import { FormGroup, FormArray, FormBuilder } from '@angular/forms'
import { FilterValue } from '../../models/filter.model'

@Component({
  selector: 'lib-filter',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: `
    <div class="filter__container">
      <form [formGroup]="filterForm">
        <fieldset class="filters">
          <h3 class="filters__header">{{ title }}</h3>
          <ul class="checkbox__list">
            <li class="checkbox__item" *ngFor="let filter of filtersArray.controls; let i = index">
              <label class="checkbox__item-container"
                >{{ filter.get('name').value }}
                <input
                  id="filter{{ i }}"
                  name="filter{{ i }}"
                  type="checkbox"
                  (change)="onSelectionChanged(i, filter.value)"
                  [formControl]="filter.get('selected')"
                />
                <span class="checkmark"></span>
              </label>
            </li>
          </ul>
        </fieldset>
      </form>
    </div>
    <!-- <pre>{{ filterForm.value | json }}</pre> -->
  `,
  styleUrls: ['./filter.component.scss']
})
export class FilterComponent implements OnInit {
  @Input() title!: string
  @Input() filters!: FilterValue[]
  @Output() selectionChanged = new EventEmitter<any>()

  filterForm: FormGroup

  private entities: { [id: number]: any } = {}

  constructor(private readonly fb: FormBuilder) {}

  ngOnInit() {
    // filterForm is an object with one property: the filtersArray. This array contains FilterValue objects.
    this.filterForm = this.fb.group({
      filtersArray: this.fb.array(
        // Each item in the group is a FilterValue object.
        // The actual form displays each object's name and changes the 'selected' property.
        this.filters.map(item => this.fb.group(item))
      )
    })

    /**
     * We could simply return an array here. The user of this component would have to
     * filter through the array to find the selected values, and map that onto their dataset.
     * But it is more performant to use an object for this! We create an object using the id's as
     * properties, and the corresponding FilterValue as its value. This creates:
     * { 1: { id: 1, name: 'somename'}, 3: { id: 3, name 'othername'}}
     * Then we can directly access the id as an index, without having to filter.
     */
    this.entities = this.filtersArray.value.reduce(
      (items: { [id: number]: any }, item: any) => {
        return {
          ...items,
          [item.id]: item
        }
      },
      {
        ...this.entities
      }
    )
  }

  /**
   * Getter for access to the filtersArray property in the filterForm.
   * Used in the actual form for displaying values and managing selection state.
   */
  get filtersArray() {
    return this.filterForm.get('filtersArray') as FormArray
  }

  onSelectionChanged(index: number, filter: any): void {
    this.entities = { ...this.entities, [filter.id]: filter }
    this.selectionChanged.emit(this.entities)
  }
}

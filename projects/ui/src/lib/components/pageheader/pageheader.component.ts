import { Component, Input, Output, EventEmitter } from '@angular/core'

@Component({
  selector: 'lib-pageheader',
  template: `
    <div class="row bottom-xs">
      <div class="col-xs-8">
        <h4 class="sub-title">{{ superTitle }}</h4>
        <h1>{{ pageTitle }}</h1>
        <h4 class="sub-title">{{ subTitle }}</h4>
      </div>
      <div class="col-xs-4 end-xs">
        <a *ngIf="actionIsAllowed" class="btn btn-outline-primary" (click)="onButtonClick()">
          {{ buttonText }} <i [ngClass]="buttonImageClass"></i>
        </a>
      </div>
    </div>
  `
})
export class PageHeaderComponent {
  @Input() pageTitle: string
  @Input() superTitle: string
  @Input() subTitle: string
  @Input() buttonText: string
  @Input() buttonImageClass: string
  @Input() actionIsAllowed = false
  @Output() action = new EventEmitter()

  onButtonClick() {
    this.action.emit()
  }
}

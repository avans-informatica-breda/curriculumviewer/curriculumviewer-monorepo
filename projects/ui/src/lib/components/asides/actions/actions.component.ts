/**
 * Deze compoment beheert de action die in een aside op het scherm getoond kunnen worden.
 *
 */
import { Component, Input } from '@angular/core'
import { ActionsGroup } from './actions.model'

@Component({
  selector: 'lib-actions',
  templateUrl: './actions.component.html',
  styleUrls: ['./actions.component.css']
})
export class ActionsComponent {
  @Input() actionsGroup!: ActionsGroup[]
}

import { ActionsComponent } from './actions/actions.component'
import { FilterActiveComponent } from './filter-active/filter-active.component'

export const components: any[] = [ActionsComponent, FilterActiveComponent]

export * from './actions/actions.component'
export * from './actions/actions.model'
export * from './filter-active/filter-active.component'

import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'
import { FormsModule, ReactiveFormsModule } from '@angular/forms'
import { NgbCollapseModule } from '@ng-bootstrap/ng-bootstrap'
import { RouterModule } from '@angular/router'
import { DragulaModule } from 'ng2-dragula'

import { DndContentCardComponent } from './lib/dnd-treeview/components/dnd-content-card/dnd-content-card.component'
import { DndNodeCardComponent } from './lib/dnd-treeview/components/dnd-node-card/dnd-node-card.component'
import { DndTreeViewComponent } from './lib/dnd-treeview/components/dnd-treeview/dnd-treeview.component'

import { PageHeaderComponent } from './lib/components/pageheader/pageheader.component'
import { ActionsComponent } from './lib/components/asides/actions/actions.component'
import { FilterComponent } from './lib/filter/components/filter/filter.component'
import { FilterActiveComponent } from './lib/components/asides/filter-active/filter-active.component'
import { DnDContentCardAddComponent } from './lib/dnd-treeview/components/dnd-content-card/dnd-add-card/dnd-content-card-add.component'
import { DropdownSelectorComponent } from './lib/dropdown-selector/components/dropdown-selector.component'

@NgModule({
  declarations: [
    DndTreeViewComponent,
    DndNodeCardComponent,
    DndContentCardComponent,
    DnDContentCardAddComponent,
    PageHeaderComponent,
    ActionsComponent,
    FilterComponent,
    FilterActiveComponent,
    DropdownSelectorComponent
  ],
  imports: [
    FormsModule,
    ReactiveFormsModule,
    RouterModule,
    NgbCollapseModule,
    DragulaModule.forRoot(),
    CommonModule
  ],
  exports: [
    DndTreeViewComponent,
    PageHeaderComponent,
    ActionsComponent,
    FilterComponent,
    FilterActiveComponent,
    DropdownSelectorComponent
  ]
})
export class UiModule {}

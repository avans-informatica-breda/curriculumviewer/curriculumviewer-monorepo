/*
 * Public API Surface of ui
 */

import {
  DndNodeCardComponent,
  DndContentCardComponent,
  DndTreeViewComponent
} from './lib/dnd-treeview/components'
import { PageHeaderComponent } from './lib/components/pageheader/pageheader.component'
import { ActionsComponent } from './lib/components/asides/actions/actions.component'
import { FilterComponent } from './lib/filter/components/filter/filter.component'
import { FilterActiveComponent } from './lib/components/asides/filter-active/filter-active.component'

export const components: any[] = [
  DndContentCardComponent,
  DndNodeCardComponent,
  DndTreeViewComponent,
  PageHeaderComponent,
  ActionsComponent,
  FilterComponent,
  FilterActiveComponent
]

export * from './lib/components/asides/actions/actions.component'
export * from './lib/components/asides/actions/actions.model'
export * from './lib/components/asides/filter-active/filter-active.component'
export * from './lib/components/pageheader/pageheader.component'

export { DndTreeViewComponent } from './lib/dnd-treeview/components/dnd-treeview/dnd-treeview.component'

export * from './lib/dnd-treeview/models'
export * from './lib/filter/components/filter/filter.component'
export * from './lib/filter/models/filter.model'
export * from './ui.module'

import { AppPage } from './app.po'
import { browser, element, by } from 'protractor'

describe('providers App', () => {
  let page: AppPage
  const buttons = element.all(by.css('button'))
  const customersButton = buttons.get(0)
  const ordersButton = buttons.get(1)
  const homeButton = buttons.get(2)

  beforeEach(() => {
    page = new AppPage()
  })

  test('should display message saying app works', () => {
    page.navigateTo()
    expect(page.getTitleText()).toEqual('Lazy loading feature modules')
  })

  describe('Customers list', () => {
    beforeEach(() => {
      customersButton.click()
    })

    test('should show customers list when the button is clicked', () => {
      const customersMessage = element(by.css('app-customer-list > p'))
      expect(customersMessage.getText()).toBe('customer-list works!')
    })
  })

  describe('Orders list', () => {
    beforeEach(() => {
      ordersButton.click()
    })

    test('should show orders list when the button is clicked', () => {
      const ordersMessage = element(by.css('app-order-list > p'))
      expect(ordersMessage.getText()).toBe('order-list works!')
    })
  })
})

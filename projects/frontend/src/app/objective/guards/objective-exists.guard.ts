import { Injectable } from '@angular/core'
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router'
import { Store } from '@ngrx/store'
import { Observable } from 'rxjs'
import { tap, filter, take, switchMap, map, flatMap } from 'rxjs/operators'

import { getRouterState } from '@avans/core'
import * as fromStore from '../store'
import { Objective } from '../models'
import { ApplicationState } from '../../app/store/reducers/app.reducer'

@Injectable()
export class ObjectiveExistsGuard implements CanActivate {
  constructor(private store: Store<ApplicationState>) {}

  /**
   * Check wether the item that we navigated to exists.
   */
  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> {
    return this.checkStore().pipe(
      switchMap(() => this.store.select(getRouterState)),
      flatMap(router => router.params.filter((param: any) => param.objectiveId)),
      switchMap((param: any) => this.hasObjective(+param.objectiveId))
    )
  }

  canActivateChild(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> {
    return this.canActivate(route, state)
  }

  hasObjective(id: number): Observable<boolean> {
    return this.store.select(fromStore.getObjectivesEntities).pipe(
      // Get the entity with given id, and convert to boolean
      // true or false indicates wether the item exists.
      map((entities: { [key: number]: Objective }) => !!entities[id]),
      tap(result => console.log(`hasObjective ${id} = ${result}`)),
      take(1)
    )
  }

  checkStore(): Observable<boolean> {
    return this.store.select(fromStore.getObjectivesLoaded).pipe(
      tap(loaded => {
        if (!loaded) {
          // Not loaded, so reload from the store
          this.store.dispatch(new fromStore.LoadObjectives())
        }
      }),
      // this filter construct waits for loaded to become true
      filter(loaded => loaded),
      // this take completes the observable and unsubscribes
      take(1)
    )
  }
}

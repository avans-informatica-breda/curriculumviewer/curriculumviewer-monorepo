import { ObjectiveGuard } from './objective.guard'
import { ObjectiveExistsGuard } from './objective-exists.guard'

export const guards: any[] = [ObjectiveGuard, ObjectiveExistsGuard]

export * from './objective.guard'
export * from './objective-exists.guard'

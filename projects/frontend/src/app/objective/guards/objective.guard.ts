import { Injectable } from '@angular/core'
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router'

import { Store } from '@ngrx/store'
import { Observable, of } from 'rxjs'
import { map, tap, filter, take, switchMap, catchError } from 'rxjs/operators'

import * as fromStore from '../store'
import { ApplicationState } from '../../app/store/reducers/app.reducer'

@Injectable()
export class ObjectiveGuard implements CanActivate {
  constructor(private store: Store<ApplicationState>) {}

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> {
    return this.checkStore().pipe(
      switchMap(() => of(true)),
      catchError(() => of(false))
    )
  }

  canActivateChild(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> {
    return this.canActivate(route, state)
  }

  checkStore(): Observable<boolean> {
    return this.store.select(fromStore.getObjectivesLoaded).pipe(
      tap(loaded => {
        if (!loaded) {
          // load items from the store
          this.store.dispatch(new fromStore.LoadObjectives())
        }
      }),
      // this filter construct waits for loaded to become true
      filter(loaded => loaded),
      // this take completes the observable and unsubscribes
      take(1)
    )
  }
}

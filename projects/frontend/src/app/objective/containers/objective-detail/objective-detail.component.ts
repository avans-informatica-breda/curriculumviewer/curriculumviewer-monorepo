import { Component, OnInit, OnDestroy, ChangeDetectionStrategy } from '@angular/core'
import { Router } from '@angular/router'
import { Observable, Subscription } from 'rxjs'
import { Store } from '@ngrx/store'

import { BaseComponent, getIsAuthenticated } from '@avans/core'
import { Objective } from '../../models'

import * as fromStore from '../../store'
import * as fromConstants from '../../objective.constants'
import { environment } from '../../../../environments/environment'
import { ApplicationState } from '../../../app/store/reducers/app.reducer'

@Component({
  selector: 'inzicht-objective-detail',
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './objective-detail.component.html'
})
export class ObjectiveDetailComponent extends BaseComponent implements OnInit, OnDestroy {
  // Logging classname tag
  readonly TAG = ObjectiveDetailComponent.name

  // Page title
  title: string

  // The core element of this Component
  // objective$: Observable<Objective>
  objective: Objective

  // User authentication role
  userMayEdit$: Observable<boolean>

  // Subscription on observable
  subscription: Subscription

  constructor(private readonly store: Store<ApplicationState>, private readonly router: Router) {
    super({ level: 1, serverUrl: environment.API_BASE_URL, production: environment.production })
  }

  ngOnInit() {
    this.userMayEdit$ = this.store.select(getIsAuthenticated)

    this.subscription = this.store.select(fromStore.getSelectedObjective).subscribe((result: Objective) => {
      console.dir('Selected node is', result && result.id)
      this.objective = result
    })
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe()
  }

  onEdit(id: number) {
    this.router.navigate([fromConstants.BASE_ROUTE, id, 'edit'])
  }
}

import { Component } from '@angular/core'

@Component({
  selector: 'inzicht-objective',
  template: `
    <inzicht-objective-topnav></inzicht-objective-topnav>
    <div class="app">
      <div class="app__content">
        <div class="app__container">
          <router-outlet></router-outlet>
        </div>
      </div>
    </div>
  `
})
export class ObjectiveComponent {}

import { Component, OnInit, ChangeDetectionStrategy, ViewChild } from '@angular/core'
import { ActivatedRoute, Router } from '@angular/router'
import { Store } from '@ngrx/store'
import { Observable } from 'rxjs'
import { tap, map } from 'rxjs/operators'

import { ActionsGroup } from '@avans/ui'
import { DialogService } from '@avans/core'

import { Endqualification } from '../../../endqualification/models'
import { BoksNodeElement } from '../../../boks/models'
import * as fromModels from '../../models'
import * as fromStore from '../../store'
import * as fromBoksStore from '../../../boks/store'
import { ObjectiveFormComponent } from '../../../objective/components'
import { ApplicationState } from '../../../app/store/reducers/app.reducer'

@Component({
  selector: 'inzicht-objective-edit',
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './objective-edit.component.html',
  styleUrls: []
})
export class ObjectiveEditComponent implements OnInit {
  //
  @ViewChild(ObjectiveFormComponent, { static: false })
  objectiveForm: ObjectiveFormComponent

  title: string

  // Item to be edited
  objective$: Observable<fromModels.Objective>

  // The list of available learning lines to select from.
  // Edited in separate component and merged into new result upon save.
  availableLearninglines$: Observable<BoksNodeElement[]>

  // Endqualifications of this objective
  // Edited in separate component and merged into new result upon save.
  endqualifications: Endqualification[]

  /**
   * Actions op deze page
   */
  actions: ActionsGroup[] = [
    new ActionsGroup({
      title: 'Acties',
      actions: [
        {
          name: 'Annuleren',
          routerLink: '..'
        }
      ]
    })
  ]

  constructor(
    private readonly store: Store<ApplicationState>,
    private readonly dialogService: DialogService,
    private readonly route: ActivatedRoute,
    private readonly router: Router
  ) {}

  ngOnInit() {
    this.title = this.route.snapshot.data.title || 'Edit Leerdoel node element'
    this.objective$ = this.store.select(fromStore.getSelectedObjective).pipe(
      tap((objective: fromModels.Objective = null) => {
        const objectiveExists = !!objective
      })
    )
    this.availableLearninglines$ = this.store
      .select(fromBoksStore.getAllBoksElements)
      .pipe(map(entities => entities.filter((item: any) => item.isLearningLine)))
  }

  onCreate(newObjective: fromModels.Objective) {
    console.log('onCreate', newObjective)
    this.store.dispatch(new fromStore.CreateObjective(newObjective))
  }

  onUpdate(newObjective: fromModels.Objective) {
    console.log('onUpdate', newObjective)
    this.store.dispatch(new fromStore.UpdateObjective(newObjective))
  }

  onRemove(event: fromModels.Objective) {
    const remove = window.confirm('Are you sure?')
    if (remove) {
      this.store.dispatch(new fromStore.DeleteObjective(event))
    }
  }

  onCancel() {
    this.router.navigate(['..'], { relativeTo: this.route })
  }

  canDeactivate() {
    if (this.objectiveForm.form.touched && !this.objectiveForm.form.dirty) {
      console.log('touched but not dirty')
      return this.dialogService.confirm(
        'Je hebt mogelijk wijzigingen in het formulier aangebracht. Wijzigingen weggooien?'
      )
    }
    return true
  }
}

import { Injectable } from '@angular/core'
import { Objective } from '../models/objective.model'
import { EntityService } from '@avans/core'
import { environment } from '../../../environments/environment'
import { HttpClient } from '@angular/common/http'

@Injectable()
export class ObjectiveService extends EntityService<Objective> {
  constructor(httpClient: HttpClient) {
    super(httpClient, environment.API_BASE_URL, 'objectives')
  }
}

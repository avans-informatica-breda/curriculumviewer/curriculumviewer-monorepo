//
// Constant definitions
//
export const BASE_ROUTE = 'leerdoelen'
export const ROUTE_NOT_FOUND = '/not-found'
export const ROUTE_NOT_ALLOWED = '/not-allowed'

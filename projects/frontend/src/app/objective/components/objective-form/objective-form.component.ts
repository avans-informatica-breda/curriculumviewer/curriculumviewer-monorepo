import { Component, Input, Output, EventEmitter, OnInit, ChangeDetectionStrategy } from '@angular/core'
import { FormControl, FormGroup, FormArray, FormBuilder, Validators } from '@angular/forms'
import { Store } from '@ngrx/store'

import { BaseComponent, ContextState, getYear } from '@avans/core'

import { BoksNodeElement } from '../../../boks/models'
import { Objective, bloomTaxonomyLevels } from '../../models'
import { Endqualification } from '../../../endqualification/models'
import { environment } from '../../../../environments/environment'

interface CheckBoxItem {
  id: number
  name: string
  selected: boolean
}

@Component({
  selector: 'inzicht-objective-form',
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './objective-form.component.html'
})
export class ObjectiveFormComponent extends BaseComponent implements OnInit {
  exists: boolean

  @Input() objective: Objective
  @Input() availableLearningLines: BoksNodeElement[]

  @Output() selected = new EventEmitter<Objective>()
  @Output() create = new EventEmitter<Objective>()
  @Output() update = new EventEmitter<Objective>()
  @Output() remove = new EventEmitter<Objective>()
  @Output() cancel = new EventEmitter<void>()

  // @ViewChild(EndqualificationSelectorComponent)
  // endqualificationSelector: EndqualificationSelectorComponent

  bloomTaxonomyLevels = bloomTaxonomyLevels
  bloomTaxonomyDescription = 'Selecteer een Bloom taxonomieniveau.'

  form: FormGroup

  // the currently selected year
  selectedYear: number

  constructor(private readonly fb: FormBuilder, private readonly store: Store<ContextState>) {
    super({ level: 1, serverUrl: environment.API_BASE_URL, production: environment.production })
  }

  ngOnInit(): void {
    this.exists = this.objective ? !!this.objective.id : false
    this.store.select(getYear).subscribe(year => (this.selectedYear = year))

    // Reduce the learninglines to their ids. The serverside will return their full objects.
    const selectedLearningLines: number[] =
      this.objective && this.objective.learningLines
        ? this.objective.learningLines.map((item: any) => item.id)
        : []

    // Map the available learninglines into our CheckBoxItem format for selection.
    const availableLearningLines = this.availableLearningLines.map(item => {
      return this.fb.group({
        id: item.id,
        name: item.name,
        selected: selectedLearningLines.includes(item.id)
      } as CheckBoxItem)
    })

    //
    // Build the form, which fields matches the Objective to create/edit.
    //
    this.form = this.fb.group({
      name: ['', Validators.required],
      year: [this.selectedYear, Validators.required],
      shortDescription: [''],
      longDescription: [''],
      administrativeRemark: [''],
      bloomTaxonomy: ['', Validators.required],
      module: [''],
      endqualifications: [[], Validators.required],
      learningLines: this.fb.array(availableLearningLines, Validators.required)
    })

    if (this.exists) {
      // Patch all existing Objecive values into the form
      this.form.patchValue(this.objective)
      this.form.controls.learningLines.patchValue(selectedLearningLines)
    }

    //
    // Listen for changes on the taxonomy selectbox
    // to show the description on value change
    //
    this.form.controls.bloomTaxonomy.valueChanges.subscribe(value => {
      this.form.markAsDirty()
      this.bloomTaxonomyDescription = bloomTaxonomyLevels
        .filter((item: any) => item.name === value)
        .map((item: any) => item.description)
        .reduce((a, b) => a + b)
    })
  }

  get nameControl() {
    return this.form.get('name') as FormControl
  }

  get nameControlInvalid() {
    return this.nameControl.hasError('required') && this.nameControl.touched
  }

  get shortDescriptionControl() {
    return this.form.get('shortDescription') as FormControl
  }

  get shortDescriptionControlInvalid() {
    return this.shortDescriptionControl.hasError('required') && this.shortDescriptionControl.touched
  }

  get bloomTaxonomyControl() {
    return this.form.get('bloomTaxonomy') as FormControl
  }

  get bloomTaxonomyControlInvalid() {
    return this.bloomTaxonomyControl.hasError('required') && this.bloomTaxonomyControl.touched
  }

  get administrativeRemark() {
    return this.form.get('administrativeRemark') as FormControl
  }

  get learningLines() {
    return this.form.get('learningLines') as FormArray
  }

  get endqualifications() {
    return this.form.get('endqualifications') as FormArray
  }

  onCreate(form: FormGroup) {
    console.log('createItem')
    const { value, valid } = form
    if (valid) {
      value.learningLines = value.learningLines
        .filter((item: CheckBoxItem) => item.selected)
        .map((item: CheckBoxItem) => item.id)
      value.endqualifications = value.endqualifications.map((item: Endqualification) => item.id)
      this.create.emit(value)
    }
  }

  onUpdate(form: FormGroup) {
    const { value, valid } = form
    if (valid) {
      value.learningLines = value.learningLines
        .filter((item: CheckBoxItem) => item.selected)
        .map((item: CheckBoxItem) => item.id)
      value.endqualifications =
        value.endqualifications &&
        value.endqualifications.length > 0 &&
        typeof value.endqualifications[0] !== 'number'
          ? value.endqualifications.map((item: Endqualification) => item.id)
          : value.endqualifications
      this.update.emit({ ...this.objective, ...value })
    }
  }

  onDelete(form: FormGroup) {
    const { value } = form
    this.remove.emit({ ...this.objective, ...value })
  }

  onCancel() {
    this.cancel.emit()
  }

  onQualificatonsUpdated(event: any[]) {
    console.log('onQualificatonsUpdated', event)
    this.endqualifications.setValue(event)
    this.form.markAsDirty()
  }
}

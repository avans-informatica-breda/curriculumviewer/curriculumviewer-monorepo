import { createSelector, MemoizedSelector } from '@ngrx/store'

import { getRouterState } from '@avans/core'
import * as fromApp from '../../../app/store/reducers/app.reducer'
import * as fromObjectives from '../reducers/objective.reducer'
import { Objective } from '../../models'

//
// Selectors - zijn nodig om door delen van de state tree te navigeren
// They return slices of the state tree.
//
export const getObjectiveState = createSelector(
  fromApp.getAppState,
  (state: fromApp.ApplicationState) => state.objectives
)

export const getObjectivesEntities = createSelector(
  getObjectiveState,
  fromObjectives.getObjectivesEntities
)

// Get the selected item based on id from the route
export const getSelectedObjective = createSelector(
  getObjectivesEntities,
  getRouterState,
  (entities, router): Objective => {
    const params: any[] = router.params.filter((item: any) => item.objectiveId)
    return router && params && params[0] && entities[params[0].objectiveId]
  }
)

export const getAllObjectives = createSelector(
  getObjectivesEntities,
  entities => {
    // Return an array version of our entities object
    // so that we can iterate over it via ngFor in HTML.
    return Object.keys(entities).map(id => entities[parseInt(id, 10)])
  }
)

export const getObjectivesLoading = createSelector(
  getObjectiveState,
  fromObjectives.getObjectivesLoading
)

export const getObjectivesLoaded = createSelector(
  getObjectiveState,
  fromObjectives.getObjectivesLoaded
)

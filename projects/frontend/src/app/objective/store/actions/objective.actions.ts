import { Action } from '@ngrx/store'
import { Objective } from '../../models'

export const RESET_OBJECTIVES = '[Objectives] Reset Objectives'

// Action creators
export class ResetObjectives implements Action {
  readonly type = RESET_OBJECTIVES
}

//
// Load actions
//
export const LOAD_OBJECTIVES = '[Objectives] Load Objectives'
export const LOAD_OBJECTIVES_FAIL = '[Objectives] Load Objectives Fail'
export const LOAD_OBJECTIVES_SUCCESS = '[Objectives] Load Objectives Success'

// Action creators
export class LoadObjectives implements Action {
  readonly type = LOAD_OBJECTIVES
}

export class LoadObjectivesFail implements Action {
  readonly type = LOAD_OBJECTIVES_FAIL
  constructor(public payload: any) {}
}

export class LoadObjectivesSuccess implements Action {
  readonly type = LOAD_OBJECTIVES_SUCCESS
  constructor(public payload: Objective[]) {}
}

//
// Create Actions
//
export const CREATE_OBJECTIVE = '[Objectives] Create Objective'
export const CREATE_OBJECTIVE_FAIL = '[Objectives] Create Objective Fail'
export const CREATE_OBJECTIVE_SUCCESS = '[Objectives] Create Objective Success'

// Action creators
export class CreateObjective implements Action {
  readonly type = CREATE_OBJECTIVE
  constructor(public payload: Objective) {}
}

export class CreateObjectiveFail implements Action {
  readonly type = CREATE_OBJECTIVE_FAIL
  constructor(public payload: any) {}
}

export class CreateObjectiveSuccess implements Action {
  readonly type = CREATE_OBJECTIVE_SUCCESS
  constructor(public payload: Objective) {}
}

//
// Update Actions
//
export const UPDATE_OBJECTIVE = '[Objectives] Update Objective'
export const UPDATE_OBJECTIVE_FAIL = '[Objectives] Update Objective Fail'
export const UPDATE_OBJECTIVE_SUCCESS = '[Objectives] Update Objective Success'

// Action creators
export class UpdateObjective implements Action {
  readonly type = UPDATE_OBJECTIVE
  constructor(public payload: Objective) {}
}

export class UpdateObjectiveFail implements Action {
  readonly type = UPDATE_OBJECTIVE_FAIL
  constructor(public payload: any) {}
}

export class UpdateObjectiveSuccess implements Action {
  readonly type = UPDATE_OBJECTIVE_SUCCESS
  constructor(public payload: Objective) {}
}

//
// Delete Actions
//
export const DELETE_OBJECTIVE = '[Objectives] Delete Objective'
export const DELETE_OBJECTIVE_FAIL = '[Objectives] Delete Objective Fail'
export const DELETE_OBJECTIVE_SUCCESS = '[Objectives] Delete Objective Success'

// Action creators
export class DeleteObjective implements Action {
  readonly type = DELETE_OBJECTIVE
  constructor(public payload: Objective) {}
}

export class DeleteObjectiveFail implements Action {
  readonly type = DELETE_OBJECTIVE_FAIL
  constructor(public payload: any) {}
}

export class DeleteObjectiveSuccess implements Action {
  readonly type = DELETE_OBJECTIVE_SUCCESS
  constructor(public payload: Objective) {}
}

// action types
export type ObjectivesAction =
  | ResetObjectives
  | LoadObjectives
  | LoadObjectivesFail
  | LoadObjectivesSuccess
  | CreateObjective
  | CreateObjectiveFail
  | CreateObjectiveSuccess
  | UpdateObjective
  | UpdateObjectiveFail
  | UpdateObjectiveSuccess
  | DeleteObjective
  | DeleteObjectiveFail
  | DeleteObjectiveSuccess

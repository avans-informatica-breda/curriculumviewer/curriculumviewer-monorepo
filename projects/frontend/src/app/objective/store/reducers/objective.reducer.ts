import * as fromObjective from '../actions'
import { Objective } from '../../models'

//
// ObjectiveState interface
//
export interface ObjectiveState {
  entities: {
    [id: number]: Objective
  }
  loaded: boolean
  loading: boolean
}

//
// Initialisation
//
export const initialState: ObjectiveState = {
  entities: {},
  loaded: false,
  loading: false
}

//
//
//
export function reducer(
  state: ObjectiveState = initialState,
  action: fromObjective.ObjectivesAction
): ObjectiveState {
  switch (action.type) {
    case fromObjective.RESET_OBJECTIVES: {
      return initialState
    }

    case fromObjective.LOAD_OBJECTIVES: {
      return {
        ...state,
        loading: true
      }
    }

    case fromObjective.LOAD_OBJECTIVES_SUCCESS: {
      const objectiveElements = action.payload
      const entities = objectiveElements.reduce(
        (
          entities: { [id: number]: Objective },
          objectiveElement: Objective
          // entities, objectiveElement
        ) => {
          return {
            ...entities,
            [objectiveElement.id]: objectiveElement
          }
        },
        {
          ...state.entities
        }
      )
      return {
        ...state,
        loading: false,
        loaded: true,
        entities
      }
    }

    case fromObjective.LOAD_OBJECTIVES_FAIL: {
      return {
        ...state,
        loading: false,
        loaded: false
      }
    }

    case fromObjective.CREATE_OBJECTIVE_SUCCESS:
    case fromObjective.UPDATE_OBJECTIVE_SUCCESS: {
      const objectiveElement = action.payload

      console.dir('Got objective:', objectiveElement)

      const entities = {
        ...state.entities,
        [objectiveElement.id]: objectiveElement
      }
      return {
        ...state,
        entities
      }
    }

    case fromObjective.DELETE_OBJECTIVE_SUCCESS: {
      const objectiveElement = action.payload
      // destructure the objectiveElement from the state object
      // select the objectiveElement by id and name that 'removed'
      // the remainder are the enteties, without the removed.
      // ES6 destructuring syntax!
      const { [objectiveElement.id]: removed, ...entities } = state.entities

      return {
        ...state,
        entities
      }
    }

    default: {
      return state
    }
  }
}

// Selector functions: get the pieces of our state that we need
export const getObjectivesEntities = (state: ObjectiveState) => state.entities
export const getObjectivesLoading = (state: ObjectiveState) => state.loading
export const getObjectivesLoaded = (state: ObjectiveState) => state.loaded

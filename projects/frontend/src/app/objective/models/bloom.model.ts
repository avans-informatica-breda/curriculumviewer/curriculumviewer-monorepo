export interface BloomTaxonomyLevel {
  level: number
  name: string
  description: string
}

export const bloomTaxonomyLevels: BloomTaxonomyLevel[] = [
  { level: 1, name: 'Onthouden', description: 'Informatie herinneren. Herkennen, beschrijven, benoemen.' },
  {
    level: 2,
    name: 'Begrijpen',
    description:
      'Ideeën of concepten uitleggen. Interpreteren, samenvatten, hernoemen, classificeren, uitleggen.'
  },
  {
    level: 3,
    name: 'Toepassen',
    description:
      'Informatie in een andere context gebruiken. Bewerkstelligen, uitvoeren, gebruiken, toepassen.'
  },
  {
    level: 4,
    name: 'Analyseren',
    description:
      'Informatie in stukken opdelen om verbanden en relaties te onderzoeken. Vergelijken, organiseren, ondervragen, vinden.'
  },
  {
    level: 5,
    name: 'Evalueren',
    description:
      'Motiveren of rechtvaardigen van een besluit of gebeurtenis. Controleren, bekritiseren, experimenteren, beoordelen.'
  },
  {
    level: 6,
    name: 'Creëren',
    description: 'Nieuwe ideeën, producten of gezichtspunten genereren. Ontwerpen, maken, uitvinden, bouwen.'
  }
]

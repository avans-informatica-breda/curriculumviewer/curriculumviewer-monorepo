import { BrowserModule } from '@angular/platform-browser'
import { BrowserAnimationsModule } from '@angular/platform-browser/animations'
import { NgModule, LOCALE_ID } from '@angular/core'
import { registerLocaleData, APP_BASE_HREF } from '@angular/common'
import localeNl from '@angular/common/locales/nl'
import { NgbModule } from '@ng-bootstrap/ng-bootstrap'
import { FormsModule, ReactiveFormsModule } from '@angular/forms'
import { HttpClientModule, HttpClientXsrfModule } from '@angular/common/http'
import { StoreDevtoolsModule } from '@ngrx/store-devtools'
import { storeFreeze } from 'ngrx-store-freeze'
import { environment } from '../environments/environment'
import { LoggerModule } from 'ngx-logger'
import { QuillModule } from 'ngx-quill'
import { StoreRouterConnectingModule, RouterStateSerializer } from '@ngrx/router-store'
import { StoreModule, MetaReducer } from '@ngrx/store'
import { EffectsModule } from '@ngrx/effects'
import { ToastrModule } from 'ngx-toastr'

import { AppRoutingModule } from './app-routing.module'
import { StyleExampleColumnsComponent } from './style-examples/style-example-columns.component'
import { StyleExampleWideComponent } from './style-examples/style-example-wide.component'
import { StyleExampleFullWidthComponent } from './style-examples/style-example-fullwidth.component'
import { StyleExample2Component } from './style-examples/style-example-2.component'

import { reducers as routerReducer, RouterEffects, CustomSerializer } from '@avans/core/src/lib/router/store'
import { UiModule } from '@avans/ui'
import { CoreModule } from '@avans/core'

import { EndqualificationModule } from './endqualification/endqualification.module'
import { BoksModule } from './boks/boks.module'
import { BoksNodeEffects } from './boks/store/effects'
import { ObjectiveModule } from './objective/objective.module'
import { CourseModule } from './course/course.module'
import { ModuleModule } from './modules/module.module'

import * as fromApp from './app'
import { reducers as appReducer } from './app/store/reducers/app.reducer'
import { CourseEffects } from './course/store'
import { ObjectiveEffects } from './objective/store'
import { ModuleEffects } from './modules/store'
import { EndqualificationEffects } from './endqualification/store'

export const metaReducers: MetaReducer<any>[] = !environment.production ? [storeFreeze] : []

registerLocaleData(localeNl, 'nl')

@NgModule({
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    ToastrModule.forRoot({
      timeOut: 2000,
      positionClass: 'toast-top-right',
      preventDuplicates: false,
      maxOpened: 5
    }),
    NgbModule,
    CoreModule.forRoot({ level: 1, serverUrl: environment.API_BASE_URL, production: environment.production }),
    UiModule,
    LoggerModule.forRoot(environment.logConfig),
    QuillModule.forRoot(environment.quillConfig),
    // Always import the HttpClientInMemoryWebApiModule after the HttpClientModule
    // to ensure that the in-memory backend provider supersedes the Angular version.
    HttpClientModule,
    HttpClientXsrfModule.withOptions({
      cookieName: 'My-Xsrf-Cookie',
      headerName: 'My-Xsrf-Header'
    }),
    // Conditional import! Production does not use the in-memory web api.
    // environment.production ? [] :
    // HttpClientInMemoryWebApiModule.forRoot(InMemoryDataService, {
    //   dataEncapsulation: false,
    //   passThruUnknownUrl: true,
    //   put204: false // return entity after PUT/update
    // }),
    //

    //
    //
    StoreModule.forFeature('application', appReducer),
    StoreModule.forRoot(routerReducer, { metaReducers }),
    EffectsModule.forRoot([
      RouterEffects,
      BoksNodeEffects,
      CourseEffects,
      ObjectiveEffects,
      ModuleEffects,
      EndqualificationEffects
    ]),

    StoreRouterConnectingModule.forRoot(),
    environment.production ? [] : StoreDevtoolsModule.instrument(),

    BoksModule.forRoot(),
    ObjectiveModule.forRoot(),
    EndqualificationModule.forRoot(),
    CourseModule.forRoot(),
    ModuleModule.forRoot(),
    AppRoutingModule
  ],
  declarations: [
    ...fromApp.components,
    StyleExampleColumnsComponent,
    StyleExampleWideComponent,
    StyleExampleFullWidthComponent,
    StyleExample2Component
  ],
  providers: [
    { provide: RouterStateSerializer, useClass: CustomSerializer },
    { provide: APP_BASE_HREF, useValue: '/' },
    { provide: LOCALE_ID, useValue: 'nl' }
  ],
  bootstrap: [fromApp.MainComponent]
})
export class AppModule {}

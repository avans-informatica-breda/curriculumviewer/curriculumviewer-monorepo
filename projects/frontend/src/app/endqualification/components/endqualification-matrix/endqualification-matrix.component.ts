import { Component, OnInit, Input, ChangeDetectionStrategy, OnDestroy } from '@angular/core'
import { Observable, of } from 'rxjs'
import { Store } from '@ngrx/store'

import { Endqualification } from '../../models'
import * as fromStore from '../../store'
import { ApplicationState } from '../../../app/store/reducers/app.reducer'

export const CURRICULUM = [
  { year: 1, trimester: 1 },
  { year: 1, trimester: 2 },
  { year: 1, trimester: 3 },
  { year: 1, trimester: 4 },
  { year: 2, trimester: 1 },
  { year: 2, trimester: 2 },
  { year: 2, trimester: 3 },
  { year: 2, trimester: 4 },
  // stage: year 3, 2 trimesters, but 1 proficiency level
  { year: 3, trimester: 1 },
  { year: 3, trimester: 3 },
  { year: 3, trimester: 4 },
  { year: 4, trimester: 1 },
  { year: 4, trimester: 2 },
  // thesis: year 4, 2 trimesters, but 1 proficiency level
  { year: 4, trimester: 3 }
]

@Component({
  selector: 'inzicht-endqualification-matrix',
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './endqualification-matrix.component.html',
  styleUrls: ['./endqualification-matrix.component.scss']
})
export class EndqualificationMatrixComponent implements OnInit, OnDestroy {
  // Logging classname tag
  readonly TAG = EndqualificationMatrixComponent.name

  @Input() endqualifications: Endqualification[]

  // Page title
  title: string

  curriculum = CURRICULUM

  areaTags$: Observable<string[]>

  constructor(private readonly store: Store<ApplicationState>) {}

  ngOnInit() {
    this.areaTags$ = of([...new Set(this.endqualifications.map(item => item.areaTag))])
  }

  ngOnDestroy() {}
}

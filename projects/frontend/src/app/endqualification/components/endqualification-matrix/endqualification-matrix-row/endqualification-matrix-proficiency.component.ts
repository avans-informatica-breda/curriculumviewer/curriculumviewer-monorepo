import { Component, OnInit, ChangeDetectionStrategy, Input, OnDestroy } from '@angular/core'
import { Store } from '@ngrx/store'
import { map, switchMap, flatMap } from 'rxjs/operators'
import { Endqualification } from '../../../models'

import * as fromCourseStore from '../../../../course/store'
import * as fromModuleStore from '../../../../modules/store'
import { Course } from 'projects/frontend/src/app/course/models'
import { Subscription } from 'rxjs'
import { ApplicationState } from 'projects/frontend/src/app/app/store/reducers/app.reducer'

@Component({
  selector: 'inzicht-endqualification-matrix-proficiency',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: `
    <div class="col-xs proficiency level-{{ obtainedProficiencyLevel }}">
      {{ obtainedProficiencyLevel }}
    </div>
  `,
  styleUrls: ['../endqualification-matrix.component.scss']
})
export class EndqualificationMatrixProficiencyComponent implements OnInit, OnDestroy {
  @Input() competence: Endqualification
  @Input() studyyear: number
  @Input() trimester: number

  obtainedProficiencyLevel = 0

  subs = new Subscription()

  constructor(private readonly store: Store<ApplicationState>) {}

  ngOnInit() {
    this.subs.add(
      this.store
        .select(fromCourseStore.getAllCourses)
        .pipe(
          flatMap(courses =>
            courses.filter(
              course => +course.studyYear === this.studyyear && +course.trimester === this.trimester
            )
          ),
          switchMap((course: Course) =>
            this.store.select(fromModuleStore.getAllModules).pipe(
              map(modules =>
                modules
                  .filter(moduleFromStore => moduleFromStore.courseId === course.id)
                  .map(module => module.objectives)
                  .map(objectives => objectives.map(objective => objective.endqualifications))
                  .reduce((a, b) => a.concat(b), [])
                  .reduce((a, b) => a.concat(b), [])
              )
            )
          ),
          map(endqualifications =>
            endqualifications.filter(
              item =>
                item.areaTag === this.competence.areaTag &&
                item.competenceLevel === this.competence.competenceLevel
            )
          ),
          map(endqualifications => endqualifications.map(item => item.proficiencyLevel))
        )
        .subscribe(
          result =>
            (this.obtainedProficiencyLevel =
              result && result.length > 0 ? result.reduce((a, b) => (a > b ? a : b)) : 0)
        )
    )
  }

  ngOnDestroy() {
    this.subs.unsubscribe()
  }
}

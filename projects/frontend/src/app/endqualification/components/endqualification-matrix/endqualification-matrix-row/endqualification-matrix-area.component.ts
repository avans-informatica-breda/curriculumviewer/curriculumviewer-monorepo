import { Component, OnInit, ChangeDetectionStrategy, Input } from '@angular/core'
import { Store } from '@ngrx/store'

import { Endqualification } from '../../../models'
import * as fromStore from '../../../store'
import { map } from 'rxjs/operators'
import { ApplicationState } from 'projects/frontend/src/app/app/store/reducers/app.reducer'

@Component({
  selector: 'inzicht-endqualification-matrix-area',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: `
    <div class="row">
      <div class="col-xs-2 area">
        <span class="area-tag">
          {{ areaTag }}
        </span>
        <span class="area-name">
          {{ (competences$ | async)[0].areaName | titlecase }}
        </span>
      </div>
      <div class="col-xs-10 competences">
        <div class="box">
          <inzicht-endqualification-matrix-competence
            *ngFor="let competence of competences$ | async"
            [competence]="competence"
          ></inzicht-endqualification-matrix-competence>
        </div>
      </div>
    </div>
  `,
  styleUrls: ['../endqualification-matrix.component.scss']
})
export class EndqualificationMatrixAreaComponent implements OnInit {
  @Input() areaTag: string

  competences$ = this.store.select(fromStore.getAllEndqualifications).pipe(
    map(items => items.filter(item => item.areaTag === this.areaTag)),
    map((items): Endqualification[] =>
      items
        .map(item => ({
          id: item.id,
          areaTag: item.areaTag,
          areaName: item.areaName,
          areaDescription: item.areaDescription,
          competenceLevel: item.competenceLevel,
          competenceName: item.competenceName
        }))
        .reduce((a, b) => (a.some(i => i.competenceLevel === b.competenceLevel) ? a : [...a, b]), [])
    )
  )

  constructor(private readonly store: Store<ApplicationState>) {}

  ngOnInit() {}
}

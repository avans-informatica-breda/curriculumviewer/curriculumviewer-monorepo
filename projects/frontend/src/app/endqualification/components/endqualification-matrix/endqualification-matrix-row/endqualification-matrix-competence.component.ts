import { Component, OnInit, ChangeDetectionStrategy, Input } from '@angular/core'
import { Endqualification } from '../../../models'
import { CURRICULUM } from '../endqualification-matrix.component'

@Component({
  selector: 'inzicht-endqualification-matrix-competence',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: `
    <div class="row competence">
      <div class="col-xs-3 description">
        <span class="areatag">
          {{ competence.areaTag }}
        </span>
        <span class="competencelevel"> {{ competence.competenceLevel }}</span>
        <span class="competencename">
          {{ competence.competenceName }}
        </span>
      </div>
      <div class="col-xs-9">
        <div class="box">
          <div class="row proficiencies">
            <inzicht-endqualification-matrix-proficiency
              *ngFor="let curr of curriculum"
              [competence]="competence"
              [studyyear]="curr.year"
              [trimester]="curr.trimester"
            ></inzicht-endqualification-matrix-proficiency>
          </div>
        </div>
      </div>
    </div>
  `,
  styleUrls: ['../endqualification-matrix.component.scss']
})
export class EndqualificationMatrixCompetenceComponent implements OnInit {
  @Input() competence: Endqualification

  curriculum = CURRICULUM

  constructor() {}

  ngOnInit() {}
}

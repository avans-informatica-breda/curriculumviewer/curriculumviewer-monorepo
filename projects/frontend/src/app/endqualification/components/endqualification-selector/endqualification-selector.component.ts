import {
  Component,
  Input,
  Output,
  EventEmitter,
  OnInit,
  ChangeDetectionStrategy,
  OnDestroy
} from '@angular/core'
import { Subscription } from 'rxjs'
import { Store } from '@ngrx/store'

import * as fromStore from '../../store'
import { Endqualification, Qualification } from '../../models'
import { ApplicationState } from '../../../app/store/reducers/app.reducer'

@Component({
  selector: 'inzicht-endqualification-selector',
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './endqualification-selector.component.html',
  styles: [
    `
      #btnAdd {
        margin-bottom: 1.5rem;
      }
      .btn-delete {
        margin-bottom: 0px;
        margin-right: 0px;
      }
    `
  ]
})
export class EndqualificationSelectorComponent implements OnInit, OnDestroy {
  //
  @Input() endqualifications: Endqualification[]
  @Output() update = new EventEmitter<number[]>()

  subscription$: Subscription

  availableEndqualifications: Endqualification[]
  availableAreas: Endqualification[] = []
  availableCompetences: Endqualification[] = []
  availableProficiencies: Endqualification[] = []

  selectedArea: string
  selectedCompetence: number
  selectedEndqualificationId: number

  selection: Endqualification[]
  selectie: Endqualification[]

  constructor(private readonly store: Store<ApplicationState>) {}

  ngOnInit(): void {
    this.selection = this.endqualifications ? this.endqualifications : []
    this.subscription$ = this.store.select(fromStore.getAllEndqualifications).subscribe(result => {
      this.availableEndqualifications = result
      //
      // Filter unique areas from the array of endqualifications
      // to fill the selectbox.
      //
      const map = new Map()
      for (const item of this.availableEndqualifications) {
        if (!map.has(item.areaTag)) {
          map.set(item.areaTag, true)
          this.availableAreas.push(item)
        }
      }
    })
  }

  /**
   * The area was chosen. Add the corresponding values to the next selectbox.
   *
   * @param event The selected value.
   */
  onAreaChanged(event: Event) {
    // Reset the next selections
    this.selectedCompetence = undefined
    this.availableCompetences = []
    this.availableProficiencies = []
    this.selectedEndqualificationId = undefined
    //
    // Filter unique competences from the array of endqualifications,
    // based on selectedArea, to fill the selectbox.
    //
    const map = new Map()
    for (const item of this.availableEndqualifications.filter(area => area.areaTag === this.selectedArea)) {
      if (!map.has(item.competenceLevel)) {
        map.set(item.competenceLevel, true)
        this.availableCompetences.push(item)
      }
    }
  }

  /**
   * The competenve was chosen. Add the corresponding values to the next selectbox.
   *
   * @param event The selected value.
   */
  onCompetenceChanged(event: Event) {
    this.availableProficiencies = []
    this.selectedEndqualificationId = undefined
    //
    // Filter unique proficiencies from the array of endqualifications,
    // based on selectedCompetence, to fill the selectbox.
    //
    const map = new Map()
    for (const item of this.availableEndqualifications.filter(
      element => element.areaTag === this.selectedArea && element.competenceLevel === +this.selectedCompetence
    )) {
      if (!map.has(item.proficiencyLevel)) {
        map.set(item.proficiencyLevel, true)
        this.availableProficiencies.push(item)
      }
    }
  }

  /**
   * The last value to be selected. Currently no further action.
   *
   * @param event The selected value.
   */
  onProficiencyChanged(event: Event) {
    // Display the proficiency description to the user?
    this.selectie = this.availableProficiencies.filter(
      (item: any) => item.id === +this.selectedEndqualificationId
    )
  }

  /**
   * Add the given selection to the list of selected qualifications and emit that value.
   */
  addQualification() {
    this.selection = [...this.selection, ...this.selectie]
    console.log('selectie: ', this.selection)
    if (this.selection) {
      this.update.emit(this.selection.map((item: any) => item.id))
    }
    // Reset all selections to zero for the next choice
    this.selectedArea = undefined
    this.selectedCompetence = undefined
    this.availableCompetences = []
    this.availableProficiencies = []
    this.selectedEndqualificationId = undefined
  }

  /**
   * Remove the selected qualification from the list of selected qualifications, and
   * emit the new list.
   *
   * @param toRemove The qualification to be removed.
   */
  removeQualification(toRemove: Endqualification) {
    console.log('removeEq', toRemove)
    this.selection = [...this.selection.filter((item: any) => item.id !== toRemove.id)]
    console.log('eqs', this.selection)
    this.update.emit(this.selection.map((item: any) => item.id))
  }

  ngOnDestroy() {
    this.subscription$.unsubscribe()
  }
}

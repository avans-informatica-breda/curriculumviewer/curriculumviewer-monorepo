import { EndqualificationFormComponent } from './endqualification-form/endqualification-form.component'
import { EndqualificationSelectorComponent } from './endqualification-selector/endqualification-selector.component'
import { EndqualificationDisplayComponent } from './endqualification-display/endqualification-display.component'
import { topnavComponents } from './endqualification-topnav'
import { EndqualificationMatrixComponent } from './endqualification-matrix/endqualification-matrix.component'
import { EndqualificationMatrixAreaComponent } from './endqualification-matrix/endqualification-matrix-row/endqualification-matrix-area.component'
import { EndqualificationMatrixCompetenceComponent } from './endqualification-matrix/endqualification-matrix-row/endqualification-matrix-competence.component'
import { EndqualificationMatrixProficiencyComponent } from './endqualification-matrix/endqualification-matrix-row/endqualification-matrix-proficiency.component'

export const components: any[] = [
  topnavComponents,
  EndqualificationDisplayComponent,
  EndqualificationFormComponent,
  EndqualificationSelectorComponent,
  EndqualificationMatrixComponent,
  EndqualificationMatrixAreaComponent,
  EndqualificationMatrixCompetenceComponent,
  EndqualificationMatrixProficiencyComponent
]

export * from './endqualification-topnav'
export * from './endqualification-display/endqualification-display.component'
export * from './endqualification-form/endqualification-form.component'
export * from './endqualification-selector/endqualification-selector.component'

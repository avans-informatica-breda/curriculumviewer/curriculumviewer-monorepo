import { NgModule } from '@angular/core'
import { RouterModule, Routes } from '@angular/router'

import * as fromAuth from '@avans/core/src/lib/auth/guards'
import * as fromGuards from './guards'
import * as fromCourseGuards from '../course/guards'
import * as fromModuleGuards from '../modules/guards'
import * as fromObjectiveGuards from '../objective/guards'
import * as fromContainers from './containers'
import * as fromConstants from './endqualification.constants'

const routes: Routes = [
  {
    path: fromConstants.BASE_ROUTE,
    component: fromContainers.EndqualificationComponent,
    canActivate: [fromGuards.EndqualificationGuard],
    children: [
      {
        path: '',
        pathMatch: 'full',
        component: fromContainers.EndqualificationListComponent
      },
      {
        path: 'matrix',
        component: fromContainers.EndqualificationCoverageMatrixComponent,
        canActivate: [
          fromGuards.EndqualificationGuard,
          fromCourseGuards.CourseGuard,
          fromModuleGuards.ModuleGuard,
          fromObjectiveGuards.ObjectiveGuard
        ]
      },
      {
        path: 'new',
        component: fromContainers.EndqualificationEditComponent,
        canActivate: [fromAuth.IsLoggedInGuard],
        data: {
          title: 'Nieuw Leerdoel',
          optionalLearningline: true
        }
      },
      {
        path: ':endqualificationId',
        component: fromContainers.EndqualificationDetailComponent,
        canActivate: [fromGuards.EndqualificationExistsGuard]
      },
      {
        path: ':endqualificationId/edit',
        component: fromContainers.EndqualificationEditComponent,
        canActivate: [fromGuards.EndqualificationExistsGuard],
        data: {
          title: 'Wijzig Eindkwalificatie'
        },
        children: [
          {
            path: '',
            component: fromContainers.EndqualificationEditComponent,
            // canDeactivate: [CanDeactivateGuard],
            data: {
              title: undefined,
              // Item kan een leerlijn zijn, Maar alleen als het een top-level
              // element is.De component moet hierop checken. De optie
              // staat hier wel aan.
              optionalLearningline: true
            }
          }
        ]
      }
    ]
  },
  {
    path: fromConstants.BASE_ROUTE + '**',
    redirectTo: fromConstants.BASE_ROUTE
  }
]

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EndqualificationRoutingModule {}

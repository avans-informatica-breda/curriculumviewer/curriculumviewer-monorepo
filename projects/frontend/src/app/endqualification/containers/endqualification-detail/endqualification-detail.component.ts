import { Component, OnInit, OnDestroy, ChangeDetectionStrategy } from '@angular/core'
import { Observable } from 'rxjs'
import { Store } from '@ngrx/store'

import { Endqualification } from '../../models'
import * as fromStore from '../../store'
import { ApplicationState } from '../../../app/store/reducers/app.reducer'

@Component({
  selector: 'inzicht-endqualification-detail',
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './endqualification-detail.component.html'
})
export class EndqualificationDetailComponent implements OnInit {
  // Logging classname tag
  readonly TAG = EndqualificationDetailComponent.name

  // Page title
  title: string

  // The core element of this Component
  endqualification$: Observable<Endqualification>

  constructor(private readonly store: Store<ApplicationState>) {}

  ngOnInit() {
    this.endqualification$ = this.store.select(fromStore.getSelectedEndqualification)
  }
}

import { Component } from '@angular/core'

@Component({
  selector: 'inzicht-endqualification',
  template: `
    <inzicht-endqualification-topnav></inzicht-endqualification-topnav>
    <div class="app">
      <div class="app__content">
        <div class="app__container">
          <router-outlet></router-outlet>
        </div>
      </div>
    </div>
  `
})
export class EndqualificationComponent {}

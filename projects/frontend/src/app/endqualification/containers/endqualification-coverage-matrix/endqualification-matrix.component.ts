import { Component, OnInit, OnDestroy, ChangeDetectionStrategy } from '@angular/core'
import { Store } from '@ngrx/store'
import * as fromStore from '../../store'
import { ApplicationState } from '../../../app/store/reducers/app.reducer'

@Component({
  selector: 'inzicht-endqualification-coverage-matrix',
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './endqualification-matrix.component.html',
  styleUrls: ['./endqualification-matrix.component.scss']
})
export class EndqualificationCoverageMatrixComponent implements OnInit {
  // Logging classname tag
  readonly TAG = EndqualificationCoverageMatrixComponent.name

  // Page title
  title: string

  endqualifications$ = this.store.select(fromStore.getAllEndqualifications)

  constructor(private readonly store: Store<ApplicationState>) {}

  ngOnInit() {}
}

//
// Constant definitions
//
export const BASE_ROUTE = 'eindkwalificaties'
export const ROUTE_NOT_FOUND = '/not-found'
export const ROUTE_NOT_ALLOWED = '/not-allowed'

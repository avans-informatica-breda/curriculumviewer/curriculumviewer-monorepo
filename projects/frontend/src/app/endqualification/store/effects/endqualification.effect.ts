import { Injectable } from '@angular/core'
import { Actions, ofType, Effect } from '@ngrx/effects'
import { switchMap, map, catchError } from 'rxjs/operators'
import { of } from 'rxjs'

import { AlertError } from '@avans/core'
import { AlertInfo } from '@avans/core'

import * as endqualificationActions from '../actions'
import * as fromServices from '../../services'

@Injectable()
export class EndqualificationEffects {
  constructor(
    private actions$: Actions,
    private endqualificationService: fromServices.EndqualificationService
  ) {}

  @Effect()
  loadEndqualifications$ = this.actions$.pipe(
    ofType(endqualificationActions.LOAD_ENDQUALIFICATIONS),
    switchMap(() =>
      this.endqualificationService.list().pipe(
        map(endqualifications => new endqualificationActions.LoadEndqualificationsSuccess(endqualifications)),
        catchError(error => of(new endqualificationActions.LoadEndqualificationsFail(error)))
      )
    )
  )

  @Effect()
  handleFail$ = this.actions$.pipe(
    ofType(endqualificationActions.LOAD_ENDQUALIFICATIONS_FAIL),
    map((action: any) => new AlertError(new AlertInfo(action.payload.title, action.payload.message)))
  )
}

import { createSelector } from '@ngrx/store'

import { getRouterState } from '@avans/core'
import * as fromApplication from '../../../app/store/reducers/app.reducer'
import * as fromEndqualifications from '../reducers/endqualification.reducer'
import { Endqualification } from '../../models'

//
// Selectors - zijn nodig om door delen van de state tree te navigeren
// They return slices of the state tree.
//
export const getEndqualificationState = createSelector(
  fromApplication.getAppState,
  (state: fromApplication.ApplicationState) => state.endqualifications
)

export const getEndqualificationsEntities = createSelector(
  getEndqualificationState,
  fromEndqualifications.getEndqualificationsEntities
)

// Get the selected item based on id from the route
export const getSelectedEndqualification = createSelector(
  getEndqualificationsEntities,
  getRouterState,
  (entities, router): Endqualification => {
    const params: any[] = router.params.filter((item: any) => item.endqualificationId)
    return router && params && params[0] && entities[params[0].endqualificationId]
  }
)

export const getAllEndqualifications = createSelector(
  getEndqualificationsEntities,
  entities => {
    // Return an array version of our entities object
    // so that we can iterate over it via ngFor in HTML.
    return Object.keys(entities).map(id => entities[parseInt(id, 10)])
  }
)

export const getEndqualificationsLoading = createSelector(
  getEndqualificationState,
  fromEndqualifications.getEndqualificationsLoading
)

export const getEndqualificationsLoaded = createSelector(
  getEndqualificationState,
  fromEndqualifications.getEndqualificationsLoaded
)

import { EndqualificationGuard } from './endqualification.guard'
import { EndqualificationExistsGuard } from './endqualification-exists.guard'

export const guards: any[] = [
  EndqualificationGuard,
  EndqualificationExistsGuard
]

export * from './endqualification.guard'
export * from './endqualification-exists.guard'

import { NgModule } from '@angular/core'
import { HttpModule } from '@angular/http'
import { ModuleWithProviders } from '@angular/core'
import { CommonModule } from '@angular/common'
import { FormsModule, ReactiveFormsModule } from '@angular/forms'
import { NgbModule } from '@ng-bootstrap/ng-bootstrap'
import { QuillModule } from 'ngx-quill'
import { NgxDatatableModule } from '@swimlane/ngx-datatable'

import * as fromAuth from '@avans/core'
import { CoreModule } from '@avans/core'
import { UiModule } from '@avans/ui'

import { ModuleRoutingModule } from './module.routing'
import * as fromComponents from './components'
import * as fromContainers from './containers'
import * as fromGuards from './guards'
import * as fromServices from './services'
import { ObjectiveModule } from '../objective/objective.module'
import { CourseModule } from '../course/course.module'
import { environment } from '../../environments/environment'

@NgModule({
  imports: [
    CommonModule,
    HttpModule,
    FormsModule,
    ReactiveFormsModule,
    NgxDatatableModule,
    CoreModule.forRoot({ level: 1, serverUrl: environment.API_BASE_URL, production: environment.production }),
    UiModule,
    CourseModule,
    ObjectiveModule,
    QuillModule,
    NgbModule,
    ModuleRoutingModule
  ],
  declarations: [...fromContainers.containers, ...fromComponents.components],
  providers: [...fromServices.services, ...fromGuards.guards],
  exports: [...fromContainers.containers, ...fromComponents.components]
})
export class ModuleModule {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: ModuleModule,
      providers: [...fromServices.services]
    }
  }
}

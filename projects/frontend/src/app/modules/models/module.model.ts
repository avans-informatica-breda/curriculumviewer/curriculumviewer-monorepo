/**
 *
 */
import { Entity } from '@avans/core'
import { Person } from '@avans/core'

import { BoksContentElement } from '../../boks/models'
import { Objective } from '../../objective/models'
import { Course } from '../../course/models'
// import { Module } from '../../modules/module.model';

export class Module extends Entity {
  // Benaming van de periode, bv. 'Basisvaardigheden 1'.
  name: string

  // Korte beschrijving, 1 zin.
  shortDescription: string

  // Uitgebreide toelichting zoals in de periodewijzer staat.
  longDescription: string

  // Periode-eigenaar
  responsiblePerson: Person

  // credits
  credits: number

  // studievorm - regulier of verkort
  form: string

  // Id van course waar deze module onderdeel van is
  // wordt door database geleverd na query.
  courseId?: number

  // Course waar deze module onderdeel van is
  course: Course

  // Leerdoelen die deze module realiseert
  objectives: Objective[]

  // BOKS inhoud die deze module realiseert
  boksContent: BoksContentElement[]

  constructor(values: any) {
    super(values)
    try {
      this.name = values.name
      this.shortDescription = values.shortDescription
      this.longDescription = values.longDescription
      this.credits = values.credits
      this.responsiblePerson = new Person(values.responsiblePerson)
      this.course = new Course(values.course)
      this.objectives = values.objectives
      this.boksContent = values.boksContent
    } catch (e) {
      console.error('Error in Module constructor: ' + e.toString())
    }
  }
}

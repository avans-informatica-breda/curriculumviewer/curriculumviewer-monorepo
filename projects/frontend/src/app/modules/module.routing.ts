import { NgModule } from '@angular/core'
import { RouterModule, Routes } from '@angular/router'

import * as fromAuth from '@avans/core'
import { PersonGuard } from '@avans/core'

import * as fromGuards from './guards'
import * as fromCourseGuards from '../course/guards'
import * as fromObjectiveGuards from '../objective/guards'
import * as fromContainers from './containers'
import * as fromConstants from './module.constants'

const routes: Routes = [
  {
    path: fromConstants.BASE_ROUTE,
    component: fromContainers.ModuleComponent,
    canActivate: [fromGuards.ModuleGuard],
    children: [
      {
        path: '',
        pathMatch: 'full',
        component: fromContainers.ModuleListComponent
      },
      {
        path: 'new',
        component: fromContainers.ModuleEditComponent,
        canActivate: [
          fromAuth.IsLoggedInGuard,
          fromCourseGuards.CourseGuard,
          fromObjectiveGuards.ObjectiveGuard,
          PersonGuard
        ],
        canDeactivate: [fromAuth.CanDeactivateGuard],
        data: {
          title: 'Nieuwe Onderwijseenheid',
          optionalLearningline: true
        }
      },
      {
        path: ':moduleId',
        component: fromContainers.ModuleDetailComponent,
        canActivate: [fromGuards.ModuleExistsGuard]
      },
      {
        path: ':moduleId/edit',
        component: fromContainers.ModuleEditComponent,
        canActivate: [
          fromGuards.ModuleExistsGuard,
          fromCourseGuards.CourseGuard,
          fromObjectiveGuards.ObjectiveGuard,
          PersonGuard
        ],
        canDeactivate: [fromAuth.CanDeactivateGuard],
        data: {
          title: 'Wijzig Onderwijseenheid'
        }
      }
    ]
  },
  {
    path: fromConstants.BASE_ROUTE + '**',
    redirectTo: fromConstants.BASE_ROUTE
  }
]

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ModuleRoutingModule {}

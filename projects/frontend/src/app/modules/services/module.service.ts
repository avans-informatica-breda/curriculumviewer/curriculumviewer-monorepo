import { Injectable } from '@angular/core'
import { Module } from '../models/module.model'
import { EntityService } from '@avans/core'
import { environment } from '../../../environments/environment'
import { HttpClient } from '@angular/common/http'

@Injectable()
export class ModuleService extends EntityService<Module> {
  constructor(httpClient: HttpClient) {
    super(httpClient, environment.API_BASE_URL, 'modules')
  }
}

import { Action } from '@ngrx/store'
import { Module } from '../../models'

//
// Reset actions
//
export const RESET_MODULES = '[Modules] Reset Modules'

// Action creators
export class ResetModules implements Action {
  readonly type = RESET_MODULES
}

//
// Load actions
//
export const LOAD_MODULES = '[Modules] Load Modules'
export const LOAD_MODULES_FAIL = '[Modules] Load Modules Fail'
export const LOAD_MODULES_SUCCESS = '[Modules] Load Modules Success'

// Action creators
export class LoadModules implements Action {
  readonly type = LOAD_MODULES
}

export class LoadModulesFail implements Action {
  readonly type = LOAD_MODULES_FAIL
  constructor(public payload: any) {}
}

export class LoadModulesSuccess implements Action {
  readonly type = LOAD_MODULES_SUCCESS
  constructor(public payload: Module[]) {}
}

//
// Create Actions
//
export const CREATE_MODULE = '[Modules] Create Module'
export const CREATE_MODULE_FAIL = '[Modules] Create Module Fail'
export const CREATE_MODULE_SUCCESS = '[Modules] Create Module Success'

// Action creators
export class CreateModule implements Action {
  readonly type = CREATE_MODULE
  constructor(public payload: Module) {}
}

export class CreateModuleFail implements Action {
  readonly type = CREATE_MODULE_FAIL
  constructor(public payload: any) {}
}

export class CreateModuleSuccess implements Action {
  readonly type = CREATE_MODULE_SUCCESS
  constructor(public payload: Module) {}
}

//
// Update Actions
//
export const UPDATE_MODULE = '[Modules] Update Module'
export const UPDATE_MODULE_FAIL = '[Modules] Update Module Fail'
export const UPDATE_MODULE_SUCCESS = '[Modules] Update Module Success'

// Action creators
export class UpdateModule implements Action {
  readonly type = UPDATE_MODULE
  constructor(public payload: Module) {}
}

export class UpdateModuleFail implements Action {
  readonly type = UPDATE_MODULE_FAIL
  constructor(public payload: any) {}
}

export class UpdateModuleSuccess implements Action {
  readonly type = UPDATE_MODULE_SUCCESS
  constructor(public payload: Module) {}
}

//
// Delete Actions
//
export const DELETE_MODULE = '[Modules] Delete Module'
export const DELETE_MODULE_FAIL = '[Modules] Delete Module Fail'
export const DELETE_MODULE_SUCCESS = '[Modules] Delete Module Success'

// Action creators
export class DeleteModule implements Action {
  readonly type = DELETE_MODULE
  constructor(public payload: Module) {}
}

export class DeleteModuleFail implements Action {
  readonly type = DELETE_MODULE_FAIL
  constructor(public payload: any) {}
}

export class DeleteModuleSuccess implements Action {
  readonly type = DELETE_MODULE_SUCCESS
  constructor(public payload: Module) {}
}

// action types
export type ModulesAction =
  | ResetModules
  | LoadModules
  | LoadModulesFail
  | LoadModulesSuccess
  | CreateModule
  | CreateModuleFail
  | CreateModuleSuccess
  | UpdateModule
  | UpdateModuleFail
  | UpdateModuleSuccess
  | DeleteModule
  | DeleteModuleFail
  | DeleteModuleSuccess

import { createSelector } from '@ngrx/store'

import { getRouterState } from '@avans/core'
import * as fromAppStore from '../../../app/store/reducers/app.reducer'
import * as fromModules from '../reducers/module.reducer'
import { Module } from '../../models'

//
// Selectors - zijn nodig om door delen van de state tree te navigeren
// They return slices of the state tree.
//
export const getModuleState = createSelector(
  fromAppStore.getAppState,
  (state: fromAppStore.ApplicationState) => state.modules
)

export const getModulesEntities = createSelector(
  getModuleState,
  fromModules.getModulesEntities
)

// Get the selected item based on id from the route
export const getSelectedModule = createSelector(
  getModulesEntities,
  getRouterState,
  (entities, router): Module => {
    const params: any[] = router.params.filter((item: any) => item.moduleId)
    return router && params && params[0] && entities[params[0].moduleId]
  }
)

export const getAllModules = createSelector(
  getModulesEntities,
  entities => {
    // Return an array version of our entities object
    // so that we can iterate over it via ngFor in HTML.
    return Object.keys(entities).map(id => entities[parseInt(id, 10)])
  }
)

export const getModulesLoading = createSelector(
  getModuleState,
  fromModules.getModulesLoading
)

export const getModulesLoaded = createSelector(
  getModuleState,
  fromModules.getModulesLoaded
)

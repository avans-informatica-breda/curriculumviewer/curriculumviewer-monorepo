import { Injectable } from '@angular/core'
import { Actions, ofType, Effect } from '@ngrx/effects'
import { switchMap, map, catchError } from 'rxjs/operators'
import { of } from 'rxjs'

import { getYear, AlertSuccess, AlertError, ContextState } from '@avans/core'
import * as fromRouter from '@avans/core/src/lib/router/store/actions'

import * as moduleActions from '../actions'
import * as fromServices from '../../services'
import * as fromConstants from '../../module.constants'
import { AlertInfo } from '@avans/core'
import { HttpParams } from '@angular/common/http'
import { Store } from '@ngrx/store'

@Injectable()
export class ModuleEffects {
  constructor(
    private actions$: Actions,
    private readonly store: Store<ContextState>,
    private moduleService: fromServices.ModuleService
  ) {}

  @Effect()
  loadModules$ = this.actions$.pipe(
    ofType(moduleActions.LOAD_MODULES),
    switchMap(() => this.store.select(getYear)),
    switchMap(year => {
      const params = new HttpParams().set('year', '' + year)
      return this.moduleService.list(params).pipe(
        map(modules => new moduleActions.LoadModulesSuccess(modules)),
        catchError(error => of(new moduleActions.LoadModulesFail(error)))
      )
    })
  )

  @Effect()
  createModule$ = this.actions$.pipe(
    ofType(moduleActions.CREATE_MODULE),
    map((action: moduleActions.CreateModule) => action.payload),
    switchMap(fromPayload =>
      this.moduleService.create(fromPayload).pipe(
        map(module => new moduleActions.CreateModuleSuccess(module)),
        catchError(error => of(new moduleActions.CreateModuleFail(error)))
      )
    )
  )

  @Effect()
  createModuleSuccess$ = this.actions$.pipe(
    ofType(moduleActions.CREATE_MODULE_SUCCESS),
    map((action: moduleActions.CreateModuleSuccess) => action.payload),
    switchMap(module => [
      new AlertSuccess(new AlertInfo('Success', 'Item was aangemaakt.')),
      new fromRouter.Go({
        path: [fromConstants.BASE_ROUTE, module.id]
      })
    ])
  )

  @Effect()
  updateModule$ = this.actions$.pipe(
    ofType(moduleActions.UPDATE_MODULE),
    map((action: moduleActions.UpdateModule) => action.payload),
    switchMap(fromPayload =>
      this.moduleService.update(fromPayload).pipe(
        map(module => new moduleActions.UpdateModuleSuccess(module)),
        catchError(error => of(new moduleActions.UpdateModuleFail(error)))
      )
    )
  )

  @Effect()
  updateModuleSuccess$ = this.actions$.pipe(
    ofType(moduleActions.UPDATE_MODULE_SUCCESS),
    map((action: moduleActions.UpdateModuleSuccess) => action.payload),
    switchMap(module => [
      new AlertSuccess(new AlertInfo('Success', 'Item was bijgewerkt.')),
      new fromRouter.Go({
        path: [fromConstants.BASE_ROUTE, module.id]
      })
    ])
  )

  @Effect()
  deleteModule$ = this.actions$.pipe(
    ofType(moduleActions.DELETE_MODULE),
    map((action: moduleActions.DeleteModule) => action.payload),
    switchMap(module =>
      this.moduleService.delete(module.id).pipe(
        // moduleService.remove returns nothing, so we return
        // the deleted module ourselves on success
        map(() => new moduleActions.DeleteModuleSuccess(module)),
        catchError(error => of(new moduleActions.DeleteModuleFail(error)))
      )
    )
  )

  @Effect()
  handleModuleSuccess$ = this.actions$.pipe(
    ofType(moduleActions.UPDATE_MODULE_SUCCESS, moduleActions.DELETE_MODULE_SUCCESS),
    map(() => {
      return new fromRouter.Go({
        path: [fromConstants.BASE_ROUTE]
      })
    })
  )

  @Effect()
  handleModuleFail$ = this.actions$.pipe(
    ofType(
      moduleActions.LOAD_MODULES_FAIL,
      moduleActions.CREATE_MODULE_FAIL,
      moduleActions.UPDATE_MODULE_FAIL,
      moduleActions.DELETE_MODULE_FAIL
    ),
    map((action: any) => action.payload),
    map(error => new AlertError(new AlertInfo(error.title, error.message)))
  )
}

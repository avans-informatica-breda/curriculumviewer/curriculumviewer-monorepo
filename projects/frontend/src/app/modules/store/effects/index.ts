import { ModuleEffects } from './module.effect'

export const effects: any[] = [ModuleEffects]

export * from './module.effect'

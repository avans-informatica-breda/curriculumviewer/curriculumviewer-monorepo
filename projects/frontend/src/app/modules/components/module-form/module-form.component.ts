import {
  Component,
  Input,
  Output,
  EventEmitter,
  OnInit,
  OnChanges,
  SimpleChanges,
  ChangeDetectionStrategy
} from '@angular/core'
import { FormControl, FormGroup, FormBuilder, Validators } from '@angular/forms'

import { Person } from '@avans/core'
import { Module } from '../../models/module.model'

@Component({
  selector: 'inzicht-module-form',
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './module-form.component.html'
})
export class ModuleFormComponent implements OnInit, OnChanges {
  exists = false

  @Input() module: Module

  @Output() selected = new EventEmitter<Module>()
  @Output() create = new EventEmitter<Module>()
  @Output() update = new EventEmitter<Module>()
  @Output() remove = new EventEmitter<Module>()
  @Output() cancel = new EventEmitter<void>()

  form = this.fb.group({
    // Vaknaam
    name: ['', Validators.required],

    // Korte beschrijving
    shortDescription: ['', Validators.required],

    // Lange beschrijving
    longDescription: [''],

    // Opmerking voor docenten
    administrativeRemark: [''],

    // Studiepunten (ECs)
    credits: [0.0, [Validators.min(0), Validators.max(15)]],

    // Vakeigenaar
    responsiblePersonId: [0, Validators.required],

    // Periode waar dit vak onderdeel van is
    courseId: [0, Validators.required],

    // Leerdoelen
    objectiveIds: [[]],

    // BOKS content in deze onderwijseenheid
    boksContentIds: [[]]
  })

  constructor(private fb: FormBuilder) {}

  ngOnInit(): void {}

  get nameControl() {
    return this.form.get('name') as FormControl
  }

  get nameControlInvalid() {
    return this.nameControl.hasError('required') && this.nameControl.touched
  }

  get shortDescriptionControl() {
    return this.form.get('shortDescription') as FormControl
  }

  get shortDescriptionControlInvalid() {
    return this.shortDescriptionControl.hasError('required') && this.shortDescriptionControl.touched
  }

  get longDescriptionControl() {
    return this.form.get('longDescription') as FormControl
  }

  get longDescriptionControlInvalid() {
    return this.longDescriptionControl.hasError('required') && this.longDescriptionControl.touched
  }

  get creditsControl() {
    return this.form.get('credits') as FormControl
  }

  get creditsControlInvalid() {
    return this.creditsControl.errors && this.creditsControl.touched
  }

  get administrativeRemark() {
    return this.form.get('administrativeRemark') as FormControl
  }

  get objectivesControl() {
    return this.form.get('objectiveIds') as FormControl
  }

  // get courseIdControl() {
  //   return this.form.get('courseId') as FormControl
  // }

  set courseId(id: number) {
    this.form.controls.courseId.setValue(id)
  }

  // get responsiblePersonControl() {
  //   return this.form.get('responsiblePersonId') as FormControl
  // }

  set responsiblePersonId(id: number) {
    this.form.controls.responsiblePersonId.setValue(id)
  }

  get boksContentControl() {
    return this.form.get('boksContentIds') as FormControl
  }

  set boksContentIds(ids: number[]) {
    this.form.controls.boksContentIds.setValue(ids)
  }

  ngOnChanges(changes: SimpleChanges) {
    if (this.module && this.module.id) {
      this.exists = true
      this.form.patchValue(this.module)
      if (this.module.responsiblePerson) {
        this.responsiblePersonId = this.module.responsiblePerson.id
      }
      if (this.module.course) {
        this.courseId = this.module.course.id
      }
      if (this.module.boksContent) {
        this.boksContentIds = this.module.boksContent.map((item: any) => item.id)
      }
    }
  }

  createItem(form: FormGroup) {
    console.log('createItem')
    const { value, valid } = form
    if (valid) {
      this.create.emit(value)
    }
  }

  updateItem(form: FormGroup) {
    const { value, valid, touched } = form
    if (touched && valid) {
      this.update.emit({ ...this.module, ...value })
    }
  }

  removeItem(form: FormGroup) {
    const { value } = form
    this.remove.emit({ ...this.module, ...value })
  }

  onCancel() {
    this.cancel.emit()
  }

  onPersonSelected(id: number) {
    this.responsiblePersonId = id
    this.form.markAsTouched()
  }

  onCourseSelected(id: number) {
    this.courseId = id
    this.form.markAsTouched()
  }
}

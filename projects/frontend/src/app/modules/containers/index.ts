import { ModuleComponent } from './module.component'
import { ModuleDetailComponent } from './module-detail/module-detail.component'
import { ModuleListComponent } from './module-list/module-list.component'
import { ModuleEditComponent } from './module-edit/module-edit.component'

export const containers: any[] = [
  ModuleComponent,
  ModuleDetailComponent,
  ModuleListComponent,
  ModuleEditComponent
]

export * from './module.component'
export * from './module-detail/module-detail.component'
export * from './module-edit/module-edit.component'
export * from './module-list/module-list.component'

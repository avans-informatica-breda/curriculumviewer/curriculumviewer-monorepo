import { Component } from '@angular/core'

@Component({
  selector: 'inzicht-module',
  template: `
    <inzicht-module-topnav></inzicht-module-topnav>
    <div class="app">
      <div class="app__content">
        <div class="app__container">
          <router-outlet></router-outlet>
        </div>
      </div>
    </div>
  `
})
export class ModuleComponent {}

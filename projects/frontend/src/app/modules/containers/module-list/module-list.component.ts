import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core'
import { Router, ActivatedRoute } from '@angular/router'
import { Observable } from 'rxjs'
import { NGXLogger } from 'ngx-logger'
import { Store } from '@ngrx/store'

import { ActionsGroup } from '@avans/ui'

import { getFullYear, getIsAuthenticated } from '@avans/core'
import * as fromStore from '../../store'
import * as fromConstants from '../../module.constants'

import { Module } from '../../models'
import { ApplicationState } from '../../../app/store/reducers/app.reducer'

@Component({
  selector: 'inzicht-module-list',
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './module-list.component.html'
})
export class ModuleListComponent implements OnInit {
  // Class name for logging
  static TAG = ModuleListComponent.name

  title = 'Onderwijseenheden'

  selectedYear$: Observable<string>

  // The core element of this Component
  modules$: Observable<Module[]>

  // User authentication role
  userMayEdit$: Observable<boolean>

  columnHeaders = [
    { prop: 'course.studyYear', name: 'Jaar', flexGrow: 1 },
    { prop: 'course.trimester', name: 'Periode', flexGrow: 1 },
    { prop: 'name', name: 'Naam', flexGrow: 3 },
    { prop: 'credits', name: 'ECs', flexGrow: 1 },
    { prop: 'responsiblePerson.firstName', name: 'Eigenaar', flexGrow: 2 }
  ]

  selected: any = []

  /**
   * Actions op deze page
   */
  actions: ActionsGroup[] = [
    new ActionsGroup({
      title: 'Acties',
      actions: [
        {
          name: 'Element toevoegen',
          routerLink: 'new'
        },
        {
          name: 'Leerdoel overzicht',
          routerLink: 'todo'
        }
      ]
    }),
    new ActionsGroup({
      title: 'Andere acties',
      actions: [
        {
          name: 'Elementen verwijderen',
          routerLink: 'edit'
        }
      ]
    })
  ]

  constructor(
    private readonly store: Store<ApplicationState>,
    private readonly logger: NGXLogger,
    private readonly router: Router,
    private readonly route: ActivatedRoute
  ) {}

  ngOnInit() {
    this.userMayEdit$ = this.store.select(getIsAuthenticated)
    this.modules$ = this.store.select(fromStore.getAllModules)
    this.selectedYear$ = this.store.select(getFullYear)
  }

  onAddElement() {
    // this.logger.debug(ModuleListComponent.TAG, 'Add element')
    this.router.navigate([fromConstants.BASE_ROUTE, 'new'])
  }

  onSelect({ selected }: any) {
    if (selected.length === 1) {
      const element = selected[0] as Module
      this.router.navigate([element.id], { relativeTo: this.route })
    }
  }
}

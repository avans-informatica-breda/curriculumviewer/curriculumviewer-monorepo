//
// Constant definitions
//
export const BASE_ROUTE = 'modules'
export const ROUTE_NOT_FOUND = '/not-found'
export const ROUTE_NOT_ALLOWED = '/not-allowed'

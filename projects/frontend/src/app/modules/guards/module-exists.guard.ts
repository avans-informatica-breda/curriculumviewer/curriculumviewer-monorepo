import { Injectable } from '@angular/core'
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router'
import { Store } from '@ngrx/store'
import { Observable } from 'rxjs'
import { tap, filter, take, switchMap, map, flatMap } from 'rxjs/operators'

import * as fromStore from '../store'
import { getRouterState } from '@avans/core'
import { Module } from '../models'
import { ApplicationState } from '../../app/store/reducers/app.reducer'

@Injectable()
export class ModuleExistsGuard implements CanActivate {
  constructor(private store: Store<ApplicationState>) {}

  /**
   * Check wether the item that we navigated to exists.
   */
  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> {
    return this.checkStore().pipe(
      switchMap(() => this.store.select(getRouterState)),
      flatMap(router => router.params.filter((param: any) => param.moduleId)),
      switchMap((param: any) => this.hasModule(+param.moduleId))
    )
  }

  canActivateChild(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> {
    return this.canActivate(route, state)
  }

  hasModule(id: number): Observable<boolean> {
    return this.store.select(fromStore.getModulesEntities).pipe(
      // Get the entity with given id, and convert to boolean
      // true or false indicates wether the item exists.
      map((entities: { [key: number]: Module }) => !!entities[id]),
      // tap(result => console.log(`hasModule ${id} = ${result}`)),
      take(1)
    )
  }

  checkStore(): Observable<boolean> {
    return this.store.select(fromStore.getModulesLoaded).pipe(
      tap(loaded => {
        if (!loaded) {
          // Not loaded, so reload from the store
          this.store.dispatch(new fromStore.LoadModules())
        }
      }),
      // this filter construct waits for loaded to become true
      filter(loaded => loaded),
      // this take completes the observable and unsubscribes
      take(1)
    )
  }
}

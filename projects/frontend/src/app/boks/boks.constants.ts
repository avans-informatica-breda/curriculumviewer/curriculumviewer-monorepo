//
// Constant definitions
//
export const BASE_ROUTE = 'boks'
export const ROUTE_NODES = 'boks/nodes'
export const ROUTE_CONTENT = 'boks/content'
export const ROUTE_LEARNING_LINES = 'boks/leerlijnen'
export const ROUTE_NOT_FOUND = '/not-found'
export const ROUTE_NOT_ALLOWED = '/not-allowed'

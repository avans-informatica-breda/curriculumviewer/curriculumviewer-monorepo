import { NgModule } from '@angular/core'
import { HttpModule } from '@angular/http'
import { ModuleWithProviders } from '@angular/core'
import { CommonModule } from '@angular/common'
import { FormsModule, ReactiveFormsModule } from '@angular/forms'
import { NgbModule, NgbCollapseModule } from '@ng-bootstrap/ng-bootstrap'
import { QuillModule } from 'ngx-quill'
import { NgxDatatableModule } from '@swimlane/ngx-datatable'
import { StoreModule } from '@ngrx/store'
import { EffectsModule } from '@ngrx/effects'

import { CoreModule } from '@avans/core'
import { UiModule } from '@avans/ui'

import { BoksRoutingModule } from './boks.routing'
import { /* reducers, */ effects } from './store'
import * as fromComponents from './components'
import * as fromContainers from './containers'
import * as fromGuards from './guards'
import * as fromServices from './services'
// import * as fromActions from './store/actions'
import { environment } from '../../environments/environment'

@NgModule({
  imports: [
    CommonModule,
    HttpModule,
    NgbCollapseModule,
    FormsModule,
    ReactiveFormsModule,
    NgxDatatableModule,
    CoreModule.forRoot({ level: 1, serverUrl: environment.API_BASE_URL, production: environment.production }),
    UiModule,
    QuillModule,
    // EffectsModule.forFeature(effects),
    BoksRoutingModule
  ],
  declarations: [...fromContainers.containers, ...fromComponents.components],
  providers: [...fromServices.services, ...fromGuards.guards],
  exports: [...fromContainers.containers, ...fromComponents.components]
})
export class BoksModule {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: BoksModule,
      providers: [...fromServices.services]
    }
  }
}

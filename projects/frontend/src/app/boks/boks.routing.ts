import { NgModule } from '@angular/core'
import { RouterModule, Routes } from '@angular/router'

import * as fromCourse from '../course/guards'
import * as fromAuth from '@avans/core/src/lib/auth/guards'
import * as fromGuards from './guards'
import * as fromContainers from './containers'
import * as fromConstants from './boks.constants'

const routes: Routes = [
  {
    path: fromConstants.ROUTE_NODES,
    component: fromContainers.BoksComponent,
    canActivate: [fromGuards.BoksGuard],
    children: [
      {
        path: '',
        pathMatch: 'full',
        component: fromContainers.BoksListComponent
      },
      {
        path: 'new',
        component: fromContainers.BoksNodeEditComponent,
        canActivate: [fromAuth.IsLoggedInGuard],
        canDeactivate: [fromAuth.CanDeactivateGuard],
        data: {
          title: 'Nieuw BOKS element',
          optionalLearningline: true
        }
      },
      {
        path: ':boksId',
        component: fromContainers.BoksDetailComponent,
        canActivateChild: [fromGuards.BoksNodeExistsGuard],
        canActivate: [fromGuards.BoksNodeExistsGuard],
        canDeactivate: [fromAuth.CanDeactivateGuard],
        children: [
          {
            path: 'addnode',
            component: fromContainers.BoksSubnodeCreateComponent,
            // canDeactivate: [CanDeactivateGuard],
            data: {
              title: 'Nieuw onderliggend BOKS element',
              // Onderliggende BOKS elementen zijn nooit een leerlijn.
              optionalLearningline: false,
              // Bij onderliggende items linken we via ID direct aan de parent.
              getIdFromParent: true
            }
          }
        ]
      },
      {
        path: ':boksId/addcontent',
        component: fromContainers.BoksContentCreateComponent,
        // canDeactivate: [CanDeactivateGuard],
        data: {
          title: 'Content bij ',
          // Bij onderliggende items linken we via ID direct aan de parent.
          getIdFromParent: true
        }
      },
      {
        path: ':boksId/edit',
        component: fromContainers.BoksNodeEditComponent,
        canActivate: [fromGuards.BoksNodeExistsGuard],
        canDeactivate: [fromAuth.CanDeactivateGuard],
        data: {
          title: 'Wijzig BOKS element'
        },
        children: [
          {
            path: '',
            component: fromContainers.BoksNodeEditComponent,
            // canDeactivate: [CanDeactivateGuard],
            data: {
              title: undefined,
              // Item kan een leerlijn zijn, Maar alleen als het een top-level
              // element is.De component moet hierop checken. De optie
              // staat hier wel aan.
              optionalLearningline: true
            }
          }
        ]
      }
    ]
  },
  {
    path: fromConstants.ROUTE_CONTENT,
    component: fromContainers.BoksComponent,
    canActivate: [fromGuards.BoksGuard],
    children: [
      {
        path: '',
        pathMatch: 'full',
        component: fromContainers.BoksContentMoveComponent,
        data: {
          title: 'BOKS Inhoud'
        }
      },
      {
        path: 'relate',
        pathMatch: 'full',
        component: fromContainers.BoksContentRelateComponent,
        canActivate: [fromCourse.CourseGuard],
        data: {
          title: 'Relateer BOKS inhoud aan onderwijseenheid'
        }
      }
    ]
  },
  {
    path: fromConstants.ROUTE_LEARNING_LINES,
    redirectTo: fromConstants.ROUTE_NODES
  },
  {
    path: fromConstants.BASE_ROUTE,
    redirectTo: fromConstants.ROUTE_NODES
  },
  {
    path: fromConstants.BASE_ROUTE + '**',
    redirectTo: fromConstants.ROUTE_NODES
  }
]

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BoksRoutingModule {}

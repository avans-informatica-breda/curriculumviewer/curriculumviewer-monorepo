import * as fromBoks from '../actions'
import { BoksNodeElement, BoksContentElement } from '../../models'

//
// BoksState interface
//
export interface BoksState {
  entities: {
    [id: number]: BoksNodeElement
  }
  loaded: boolean
  loading: boolean
}

//
// Initialisation
//
export const initialState: BoksState = {
  entities: {},
  loaded: false,
  loading: false
}

//
// Reducer function
//
export function reducer(state: BoksState = initialState, action: fromBoks.BoksAction): BoksState {
  switch (action.type) {
    case fromBoks.RESET_BOKS_NODES: {
      return initialState
    }

    case fromBoks.LOAD_BOKS_NODES: {
      return {
        ...state,
        loading: true
      }
    }

    case fromBoks.LOAD_BOKS_NODES_SUCCESS: {
      const boksElements = action.payload
      const entities = boksElements.reduce(
        (_entities: { [id: number]: BoksNodeElement }, boksElement: BoksNodeElement) => {
          return {
            ..._entities,
            [boksElement.id]: boksElement
          }
        },
        {
          ...state.entities
        }
      )
      return {
        ...state,
        loading: false,
        loaded: true,
        entities
      }
    }

    case fromBoks.LOAD_BOKS_NODES_FAIL: {
      return {
        ...state,
        loading: false,
        loaded: false
      }
    }

    case fromBoks.CREATE_BOKS_NODE_SUCCESS:
    case fromBoks.UPDATE_BOKS_NODE_SUCCESS: {
      const boksElement = new BoksNodeElement(action.payload)
      const entities = {
        ...state.entities,
        [boksElement.id]: boksElement
      }
      return {
        ...state,
        entities
      }
    }

    case fromBoks.CREATE_BOKS_SUBNODE_SUCCESS: {
      const boksSubNode = new BoksNodeElement(action.payload)
      const parentId = +boksSubNode.parentId
      const parent = state.entities[parentId]
      const updatedParent = {
        ...parent,
        childNodes: [...parent.childNodes, boksSubNode]
      }

      const entities = {
        ...state.entities,
        [parentId]: updatedParent,
        [boksSubNode.id]: boksSubNode
      }
      return {
        ...state,
        entities
      }
    }

    case fromBoks.DELETE_BOKS_NODE_SUCCESS: {
      const boksElement = action.payload
      // destructure the boksElement from the state object
      // select the boksElement by id and name that 'removed'
      // the remainder are the enteties, without the removed.
      // ES6 destructuring syntax!
      const { [boksElement.id]: removed, ...entities } = state.entities

      return {
        ...state,
        entities
      }
    }

    case fromBoks.CREATE_BOKS_CONTENT_SUCCESS:
    case fromBoks.UPDATE_BOKS_CONTENT_SUCCESS: {
      const boksContent = new BoksContentElement(action.payload)
      const parentId = +boksContent.parentId
      const parent = state.entities[parentId]
      const updatedParent = {
        ...parent,
        contentNodes: [...parent.contentNodes, boksContent]
      }

      console.log('updatedParent = ', updatedParent)
      console.log('boksContent = ', boksContent)

      const entities = {
        ...state.entities,
        [parentId]: updatedParent
      }
      return {
        ...state,
        entities
      }
    }

    case fromBoks.DELETE_BOKS_CONTENT_SUCCESS: {
      const boksContent = action.payload
      // destructure the boksContent from the state object
      // select the boksContent by id and name that 'removed'
      // the remainder are the enteties, without the removed.
      // ES6 destructuring syntax!
      const { [boksContent.id]: removed, ...entities } = state.entities

      return {
        ...state,
        entities
      }
    }

    case fromBoks.MOVE_BOKS_CONTENT: {
      // tslint:disable-next-line: prefer-const
      let { item, targetNodeId } = action.payload
      const oldParentId = item.parentId

      // Remove item from old parent's contentNodes
      let oldParent = state.entities[+item.parentId]
      oldParent = {
        ...oldParent,
        contentNodes: oldParent.contentNodes.filter(node => node.id !== item.id)
      }

      // Insert item's parentId and insert it at new parent's contentnodes
      item = { ...item, parentId: targetNodeId }
      let newParent = state.entities[targetNodeId]
      newParent = {
        ...newParent,
        contentNodes: [...newParent.contentNodes, item]
      }

      const entities = {
        ...state.entities,
        [oldParentId]: oldParent,
        [targetNodeId]: newParent
      }
      return {
        ...state,
        entities,
        loading: true
      }
    }

    //
    // Move Boks Content items from one parent to another
    //
    case fromBoks.MOVE_BOKS_CONTENT_SUCCESS: {
      return {
        ...state,
        loading: false
      }
    }

    case fromBoks.MOVE_BOKS_CONTENT_FAIL: {
      return {
        ...state,
        loading: false
      }
    }

    default: {
      // console.log('Default action! No state change.')
      return state
    }
  }
}

// Selector functions: get the pieces of our state that we need
export const getBoksElementsEntities = (state: BoksState) => state.entities
export const getBoksElementsLoading = (state: BoksState) => state.loading
export const getBoksElementsLoaded = (state: BoksState) => state.loaded

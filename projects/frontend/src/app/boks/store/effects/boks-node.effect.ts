import { Injectable } from '@angular/core'
import { Store } from '@ngrx/store'
import { Actions, ofType, Effect } from '@ngrx/effects'
import { of, forkJoin } from 'rxjs'
import { switchMap, map, catchError, tap, filter } from 'rxjs/operators'
import { HttpParams } from '@angular/common/http'

import { getYear, ContextState, AlertInfo, AlertSuccess, AlertError, AlertWarn } from '@avans/core'
import * as fromRouter from '@avans/core/src/lib/router/store/actions'

import * as boksActions from '../actions'
import * as fromServices from '../../services'
import * as fromSelectors from '../selectors'
import * as fromConstants from '../../boks.constants'
import { BoksContentElement } from '../../models'
import { BoksState } from '../reducers/boks-node.reducer'

@Injectable()
export class BoksNodeEffects {
  constructor(
    private actions$: Actions,
    private readonly contextStore: Store<ContextState>,
    private readonly boksStore: Store<BoksState>,
    private readonly boksNodeService: fromServices.BoksNodeService,
    private readonly boksContentService: fromServices.BoksContentService
  ) {}

  @Effect()
  loadBoksNodes$ = this.actions$.pipe(
    ofType(boksActions.LOAD_BOKS_NODES),
    switchMap(() => this.contextStore.select(getYear)),
    switchMap(year => {
      // Create query param '?year=2018'
      const params = new HttpParams().set('year', '' + year)
      // call service method using query params as argument
      return this.boksNodeService.list(params).pipe(
        map(boksNodes => new boksActions.LoadBoksNodesSuccess(boksNodes)),
        catchError(error => of(new boksActions.LoadBoksNodesFail(error)))
      )
    })
  )

  @Effect()
  createBoksNode$ = this.actions$.pipe(
    ofType(boksActions.CREATE_BOKS_NODE),
    map((action: boksActions.CreateBoksNode) => action.payload),
    switchMap(fromPayload =>
      this.boksNodeService.create(fromPayload).pipe(
        map(boksElement => new boksActions.CreateBoksNodeSuccess(boksElement)),
        catchError(error => of(new boksActions.CreateBoksNodeFail(error)))
      )
    )
  )

  @Effect()
  createBoksNodeSuccess$ = this.actions$.pipe(
    ofType(boksActions.CREATE_BOKS_NODE_SUCCESS),
    map((action: boksActions.CreateBoksNodeSuccess) => action.payload),
    switchMap(boksElement => [
      new AlertSuccess(new AlertInfo('Success', `${boksElement.name} aangemaakt`)),
      new fromRouter.Go({
        path: [fromConstants.ROUTE_NODES, boksElement.id]
      })
    ])
  )

  @Effect()
  createBoksSubNode$ = this.actions$.pipe(
    ofType(boksActions.CREATE_BOKS_SUBNODE),
    map((action: boksActions.CreateBoksSubNode) => action.payload),
    switchMap(fromPayload =>
      this.boksNodeService.createSubNode(fromPayload).pipe(
        map(boksElement => new boksActions.CreateBoksSubNodeSuccess(boksElement)),
        catchError(error => of(new boksActions.CreateBoksSubNodeFail(error)))
      )
    )
  )

  @Effect()
  createBoksSubNodeSuccess$ = this.actions$.pipe(
    ofType(boksActions.CREATE_BOKS_SUBNODE_SUCCESS),
    map((action: boksActions.CreateBoksSubNodeSuccess) => action.payload),
    switchMap(boksSubNode => [
      new AlertSuccess(new AlertInfo('Success', `${boksSubNode.name} aangemaakt`)),
      new fromRouter.Go({
        path: [fromConstants.ROUTE_NODES, boksSubNode.parentId]
      })
    ])
  )

  @Effect()
  createBoksContent$ = this.actions$.pipe(
    ofType(boksActions.CREATE_BOKS_CONTENT),
    map((action: boksActions.CreateBoksContent) => action.payload),
    switchMap(fromPayload =>
      this.boksNodeService.createContentNode(fromPayload).pipe(
        map((boksContent: BoksContentElement) => new boksActions.CreateBoksContentSuccess(boksContent)),
        catchError(error => of(new boksActions.CreateBoksContentFail(error)))
      )
    )
  )

  @Effect()
  createBoksContentSuccess$ = this.actions$.pipe(
    ofType(boksActions.CREATE_BOKS_CONTENT_SUCCESS),
    map((action: boksActions.CreateBoksContentSuccess) => action.payload),
    switchMap(boksContent => [
      new AlertSuccess(new AlertInfo('Success', `${boksContent.name} aangemaakt`))
      // new fromRouter.Go({
      //   path: [fromConstants.ROUTE_NODES, boksContent.parentId]
      // })
    ])
  )

  @Effect()
  updateBoksContent$ = this.actions$.pipe(
    ofType(boksActions.UPDATE_BOKS_CONTENT),
    map((action: boksActions.UpdateBoksContent) => action.payload),
    switchMap(fromPayload =>
      this.boksNodeService.updateContentNode(fromPayload).pipe(
        map((boksContent: BoksContentElement) => new boksActions.CreateBoksContentSuccess(boksContent)),
        catchError(error => of(new boksActions.CreateBoksContentFail(error)))
      )
    )
  )

  @Effect()
  updateBoksContentSuccess$ = this.actions$.pipe(
    ofType(boksActions.UPDATE_BOKS_CONTENT_SUCCESS),
    map((action: boksActions.CreateBoksContentSuccess) => action.payload),
    switchMap(boksContent => [
      new AlertSuccess(new AlertInfo('Success', `${boksContent.name} aangemaakt`))
      // new fromRouter.Go({
      //   path: [fromConstants.ROUTE_NODES, boksContent.parentId]
      // })
    ])
  )

  @Effect()
  updateBoksNode$ = this.actions$.pipe(
    ofType(boksActions.UPDATE_BOKS_NODE),
    map((action: boksActions.UpdateBoksNode) => action.payload),
    switchMap(fromPayload =>
      this.boksNodeService.update(fromPayload).pipe(
        map(boksElement => new boksActions.UpdateBoksNodeSuccess(boksElement)),
        catchError(error => of(new boksActions.UpdateBoksNodeFail(error)))
      )
    )
  )

  @Effect()
  deleteBoksNode$ = this.actions$.pipe(
    ofType(boksActions.DELETE_BOKS_NODE),
    map((action: boksActions.DeleteBoksNode) => action.payload),
    switchMap(boksElement =>
      this.boksNodeService.delete(boksElement.id).pipe(
        // boksNodeService.remove returns nothing, so we return
        // the deleted boksElement ourselves on success
        map(() => new boksActions.DeleteBoksNodeSuccess(boksElement)),
        catchError(error => of(new boksActions.DeleteBoksNodeFail(error)))
      )
    )
  )

  @Effect()
  moveBoksContent$ = this.actions$.pipe(
    ofType(boksActions.MOVE_BOKS_CONTENT),
    map((action: boksActions.MoveBoksContent) => action.payload),
    switchMap(fromPayload => {
      const { item, targetNodeId } = fromPayload
      return this.boksStore.select(fromSelectors.getAllBoksElements).pipe(
        map(elements => {
          return {
            item,
            oldParent: elements.filter(element => element.id === item.parentId)[0],
            targetNodeId
          }
        })
      )
    }),
    tap(result =>
      console.log('move item', result.item.name, 'from', result.oldParent.name, 'to', result.targetNodeId)
    ),
    switchMap(elements => {
      // tslint:disable-next-line: prefer-const
      let { item, oldParent, targetNodeId } = elements
      item = { ...item, parentId: targetNodeId }
      oldParent = { ...oldParent, contentNodes: oldParent.contentNodes.filter(node => node.id !== item.id) }
      return forkJoin([this.boksNodeService.update(oldParent), this.boksContentService.update(item)]).pipe(
        map(() => new boksActions.MoveBoksContentSuccess(item)),
        catchError(error => of(new boksActions.MoveBoksContentFail(error)))
      )
    })
  )

  @Effect()
  moveBoksContentSuccess$ = this.actions$.pipe(
    ofType(boksActions.MOVE_BOKS_CONTENT_SUCCESS),
    map((action: boksActions.MoveBoksContentSuccess) => action.payload),
    switchMap(item => [new AlertSuccess(new AlertInfo('Success', `Item aangemaakt`))])
  )

  @Effect()
  moveBoksContentFail$ = this.actions$.pipe(
    ofType(boksActions.MOVE_BOKS_CONTENT_FAIL),
    map((action: boksActions.MoveBoksContentFail) => action.payload),
    switchMap(fromPayload => [new AlertError(new AlertInfo('Error', `Could not move item`))])
  )

  @Effect()
  loadBoksNodesFail$ = this.actions$.pipe(
    ofType(
      boksActions.LOAD_BOKS_NODES_FAIL,
      boksActions.CREATE_BOKS_NODE_FAIL,
      boksActions.UPDATE_BOKS_NODE_FAIL,
      boksActions.DELETE_BOKS_NODE_FAIL,
      boksActions.CREATE_BOKS_SUBNODE_FAIL,
      boksActions.CREATE_BOKS_CONTENT_FAIL,
      boksActions.UPDATE_BOKS_CONTENT_FAIL,
      boksActions.DELETE_BOKS_CONTENT_FAIL
    ),
    map((action: any) => action.payload),
    map(info => new AlertError(new AlertInfo(info.title, info.message)))
  )

  @Effect()
  handleBoksNodeSuccess$ = this.actions$.pipe(
    ofType(boksActions.UPDATE_BOKS_NODE_SUCCESS, boksActions.DELETE_BOKS_NODE_SUCCESS),
    switchMap(() => [
      new AlertSuccess(new AlertInfo('Success', 'Item was updated.')),
      new fromRouter.Go({
        path: [fromConstants.ROUTE_NODES]
      })
    ])
  )
}

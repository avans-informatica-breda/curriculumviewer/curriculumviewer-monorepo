import { Injectable } from '@angular/core'
import { HttpClient, HttpParams } from '@angular/common/http'
import { environment } from '../../../environments/environment'

import { EntityService } from '@avans/core'
import { BoksContentElement } from '../models/boks.model'
import { Observable } from 'rxjs'
import { map, catchError } from 'rxjs/operators'

@Injectable({
  providedIn: 'root'
})
export class BoksContentService extends EntityService<BoksContentElement> {
  constructor(httpClient: HttpClient) {
    super(httpClient, environment.API_BASE_URL, 'boksnodes')
  }

  /**
   * Update (put) new info.
   *
   * @param item The new item.
   */
  public update(item: BoksContentElement, params?: HttpParams): Observable<BoksContentElement> {
    const endpoint = `${this.url}${this.endpoint}/${item.parentId}/content/${item.id}`
    // console.log(`update ${endpoint}`)
    return this.http.put(endpoint, item, { params, observe: 'response' }).pipe(
      map(response => response.body as BoksContentElement),
      catchError(super.handleError)
    )
  }
}

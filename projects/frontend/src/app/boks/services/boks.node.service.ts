import { Injectable } from '@angular/core'
import { HttpClient, HttpParams } from '@angular/common/http'
import { Observable } from 'rxjs'
import { map, catchError } from 'rxjs/operators'

import { EntityService } from '@avans/core'
import { environment } from '../../../environments/environment'

import { BoksNodeElement, BoksContentElement } from '../models/boks.model'

@Injectable({
  providedIn: 'root'
})
export class BoksNodeService extends EntityService<BoksNodeElement> {
  constructor(httpClient: HttpClient) {
    super(httpClient, environment.API_BASE_URL, 'boksnodes')
  }

  /**
   *
   * @param boksNode
   */
  public createSubNode(boksNode: BoksNodeElement): Observable<BoksNodeElement> {
    const apiUrl = this.url + this.endpoint + '/' + boksNode.parentId + '/subnode'
    console.log(`apiUrl = ${apiUrl}`)

    const httpParams: HttpParams = new HttpParams()

    return this.http.post(apiUrl, boksNode, { params: httpParams, observe: 'response' }).pipe(
      map(response => response.body as BoksNodeElement),
      catchError(this.handleError)
    )
  }

  /**
   * Create content
   *
   * @param boksContent
   */
  public createContentNode(boksContent: BoksContentElement): Observable<BoksContentElement> {
    const apiUrl = this.url + this.endpoint + '/' + boksContent.parentId + '/content'
    console.log(`apiUrl = ${apiUrl}`)

    const httpParams: HttpParams = new HttpParams()

    return this.http.post(apiUrl, boksContent, { params: httpParams, observe: 'response' }).pipe(
      map(response => response.body as BoksContentElement),
      catchError(this.handleError)
    )
  }

  /**
   * Update content element
   *
   * @param boksContent
   */
  public updateContentNode(boksContent: BoksContentElement): Observable<BoksContentElement> {
    const apiUrl = this.url + this.endpoint + '/' + boksContent.parentId + '/content'
    console.log(`apiUrl = ${apiUrl}`)

    const httpParams: HttpParams = new HttpParams()

    return this.http.put(apiUrl, boksContent, { params: httpParams, observe: 'response' }).pipe(
      map(response => response.body as BoksContentElement),
      catchError(this.handleError)
    )
  }

  /**
   * Delete boks content element
   *
   * @param boksContent
   */
  public deleteContentNode(boksContent: BoksContentElement): Observable<BoksContentElement> {
    const apiUrl = this.url + this.endpoint + '/' + boksContent.parentId + '/content'
    console.log(`apiUrl = ${apiUrl}`)

    const httpParams: HttpParams = new HttpParams()

    return this.http.delete(apiUrl, { params: httpParams, observe: 'response' }).pipe(
      map(response => response.body as BoksContentElement),
      catchError(this.handleError)
    )
  }
}

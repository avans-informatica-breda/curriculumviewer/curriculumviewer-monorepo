import { BoksNodeService } from './boks.node.service'
import { BoksContentService } from './boks.content.service'

export const services: any[] = [BoksNodeService, BoksContentService]

export * from './boks.node.service'
export * from './boks.content.service'

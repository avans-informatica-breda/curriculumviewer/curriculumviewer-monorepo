import {
  Component,
  Input,
  Output,
  EventEmitter,
  OnInit,
  OnChanges,
  SimpleChanges,
  ChangeDetectionStrategy
} from '@angular/core'
import { FormControl, FormGroup, FormBuilder, Validators } from '@angular/forms'

import { BoksNodeElement } from '../../models/boks.model'

@Component({
  selector: 'inzicht-boks-form',
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './boks-form.component.html'
})
export class BoksFormComponent implements OnInit, OnChanges {
  exists = false

  @Input() boksElement: BoksNodeElement
  @Input() isLearningLine = false

  @Output() selected = new EventEmitter<BoksNodeElement>()
  @Output() create = new EventEmitter<BoksNodeElement>()
  @Output() update = new EventEmitter<BoksNodeElement>()
  @Output() remove = new EventEmitter<BoksNodeElement>()
  @Output() cancel = new EventEmitter<void>()

  form = this.fb.group({
    name: ['', Validators.required],
    shortDescription: ['', Validators.required],
    longDescription: [''],
    administrativeRemark: [''],
    isLearningLine: [false],
    parent: [undefined],
    childNodes: [[]],
    contentNodes: [[]],
    level: [0]
  })

  constructor(private fb: FormBuilder) {}

  ngOnInit(): void {}

  get nameControl() {
    return this.form.get('name') as FormControl
  }

  get nameControlInvalid() {
    return this.nameControl.hasError('required') && this.nameControl.touched
  }

  get shortDescriptionControl() {
    return this.form.get('shortDescription') as FormControl
  }

  get shortDescriptionControlInvalid() {
    return this.shortDescriptionControl.hasError('required') && this.shortDescriptionControl.touched
  }

  get longDescriptionControl() {
    return this.form.get('longDescription') as FormControl
  }

  get longDescriptionControlInvalid() {
    return this.longDescriptionControl.hasError('required') && this.longDescriptionControl.touched
  }

  get administrativeRemark() {
    return this.form.get('administrativeRemark') as FormControl
  }

  ngOnChanges(changes: SimpleChanges) {
    if (this.boksElement && this.boksElement.id) {
      this.exists = true
      this.form.patchValue(this.boksElement)
    }
  }

  createItem(form: FormGroup) {
    console.log('createItem')
    const { value, valid } = form
    if (valid) {
      this.create.emit(value)
    }
    // Reset the form values
    this.exists = false
    this.form.reset()
    this.form.markAsPristine()
    this.form.markAsUntouched()
  }

  updateItem(form: FormGroup) {
    const { value, valid, touched } = form
    if (touched && valid) {
      this.update.emit({ ...this.boksElement, ...value })
    }
    // Reset the form values
    this.exists = false
    this.form.reset()
    this.form.markAsPristine()
    this.form.markAsUntouched()
  }

  removeItem(form: FormGroup) {
    const { value } = form
    this.remove.emit({ ...this.boksElement, ...value })
  }

  onCancel() {
    this.cancel.emit()
  }
}

import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core'
import { ActivatedRoute, Router } from '@angular/router'
import { Store } from '@ngrx/store'
import { Observable } from 'rxjs'
import { tap } from 'rxjs/operators'
// import { NGXLogger } from 'ngx-logger'

import { ActionsGroup } from '@avans/ui'
import { DialogService } from '@avans/core'

import * as fromStore from '../../store'
import { BoksNodeElement } from '../../models'
import { ApplicationState } from '../../../app/store/reducers/app.reducer'

@Component({
  selector: 'inzicht-boks-node-edit',
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './boks-node-edit.component.html',
  styleUrls: []
})
export class BoksNodeEditComponent implements OnInit {
  title: string

  // Element to be edited
  boks$: Observable<BoksNodeElement>

  // Option to select element as learning line.
  isLearningLine: boolean

  /**
   * Actions op deze page
   */
  actions: ActionsGroup[] = [
    new ActionsGroup({
      title: 'Acties',
      actions: [
        {
          name: 'Annuleren',
          routerLink: '..'
        }
      ]
    })
  ]

  constructor(
    private readonly store: Store<ApplicationState>,
    private readonly dialogService: DialogService,
    private readonly route: ActivatedRoute,
    private readonly router: Router
  ) {}

  ngOnInit() {
    this.title = this.route.snapshot.data['title'] || 'Edit BOKS node element'
    this.isLearningLine = this.route.snapshot.data['optionalLearningline'] || false

    this.boks$ = this.store.select(fromStore.getSelectedBoksElement).pipe(
      tap((boksElement: BoksNodeElement = null) => {
        const boksElementExists = !!boksElement
        // const toppings = boksElementExists
        //   ? boksElement.toppings.map(topping => topping.id)
        //   : []
        // this.store.dispatch(new fromStore.VisualizeToppings(toppings))
      })
    )
    // this.toppings$ = this.store.select(fromStore.getAllToppings)
    // this.visualise$ = this.store.select(fromStore.getPizzaVisualized)
  }

  onSelect(event: number[]) {}

  onCreate(event: BoksNodeElement) {
    this.store.dispatch(new fromStore.CreateBoksNode(event))
  }

  onUpdate(event: BoksNodeElement) {
    console.log('onUpdate')
    this.store.dispatch(new fromStore.UpdateBoksNode(event))
  }

  onRemove(event: BoksNodeElement) {
    const remove = window.confirm('Are you sure?')
    if (remove) {
      this.store.dispatch(new fromStore.DeleteBoksNode(event))
    }
  }

  onCancel() {
    this.router.navigate(['..'], { relativeTo: this.route })
  }

  canDeactivate() {
    // Allow synchronous navigation (`true`) if no crisis or the crisis is unchanged
    //   if (!this.form.dirty || this.form.untouched) {
    //     console.log('not dirty or untouched')
    //     return true
    //   }
    return this.dialogService.confirm(
      'Je hebt mogelijk wijzigingen in het formulier aangebracht. Wijzigingen weggooien?'
    )
    // }
  }
}

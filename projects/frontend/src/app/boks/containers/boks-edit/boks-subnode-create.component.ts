import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core'
import { ActivatedRoute, Router } from '@angular/router'
import { Observable } from 'rxjs'
import { Store } from '@ngrx/store'

import { DialogService, getYear } from '@avans/core'

import * as fromStore from '../../store'
import { BoksNodeElement } from '../../models'
import { ApplicationState } from '../../../app/store/reducers/app.reducer'

@Component({
  selector: 'inzicht-boks-subnode-create',
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './boks-subnode-create.component.html'
})
export class BoksSubnodeCreateComponent implements OnInit {
  title: string

  // Element to be edited
  boks$: Observable<BoksNodeElement>

  parentId: number

  constructor(
    private readonly store: Store<ApplicationState>,
    private readonly dialogService: DialogService,
    private readonly route: ActivatedRoute,
    private readonly router: Router
  ) {}

  ngOnInit() {
    this.title = this.route.snapshot.data['title'] || 'Create BOKS subnode element'
    this.route.parent.paramMap.subscribe(params => (this.parentId = +params.get('boksId')))
  }

  onCreate(subNode: BoksNodeElement) {
    this.store.select(getYear).subscribe(year => {
      subNode.parentId = this.parentId
      subNode.year = '' + year
      this.store.dispatch(new fromStore.CreateBoksSubNode(subNode))
    })
  }

  onCancel() {
    console.log('onCancel')
    this.router.navigate(['..'], { relativeTo: this.route })
  }

  canDeactivate() {
    // Allow synchronous navigation (`true`) if no crisis or the crisis is unchanged
    //   if (!this.form.dirty || this.form.untouched) {
    //     console.log('not dirty or untouched')
    //     return true
    //   }
    return this.dialogService.confirm(
      'Je hebt mogelijk wijzigingen in het formulier aangebracht. Wijzigingen weggooien?'
    )
    // }
  }
}

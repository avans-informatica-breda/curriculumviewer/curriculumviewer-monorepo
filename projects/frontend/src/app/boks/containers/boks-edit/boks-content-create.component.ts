import { Component, OnInit, ChangeDetectionStrategy, OnDestroy } from '@angular/core'
import { ActivatedRoute, Router } from '@angular/router'
import { ActionsGroup } from '@avans/ui'
import { Observable, from, of, Subscription } from 'rxjs'
import { Store } from '@ngrx/store'

import * as fromStore from '../../store'
import { BoksNodeElement, BoksContentElement } from '../../models'
import { getContext, DialogService } from '@avans/core'
import { ApplicationState } from '../../../app/store/reducers/app.reducer'

@Component({
  selector: 'inzicht-boks-content-create',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: `
    <main class="viewer__container">
      <div class="viewer__wide">
        <header>
          <lib-pageheader [pageTitle]="title" [superTitle]="superTitle"></lib-pageheader>
        </header>

        <nav>
          <lib-actions [actionsGroup]="actions"></lib-actions>
        </nav>

        <article>
          <section>
            <p>
              Een BOKS element representeert een element uit de 'body of knowledge and skills' van de
              opleiding. Door content toe te voegen aan BOKS elementen specificeer je concrete methodieken en
              technieken bij dat element.
            </p>
          </section>
          <div class="row">
            <div class="col-xs">
              <h3>Content informatie</h3>
              <section>
                <inzicht-boks-form
                  [boksElement]="boksContentElement$ | async"
                  [isLearningLine]="false"
                  (create)="onCreate($event)"
                  (update)="onUpdate($event)"
                  (remove)="onRemove($event)"
                  (cancel)="onCancel()"
                >
                </inzicht-boks-form>
              </section>
            </div>
            <div class="col-xs">
              <h3>Content</h3>
              <ngx-datatable
                class="bootstrap"
                [rows]="contentElements$ | async"
                [columns]="columnHeaders"
                [columnMode]="'force'"
                [headerHeight]="50"
                [rowHeight]="50"
                [selected]="selectedContentElement"
                [selectionType]="'single'"
                (select)="onSelect($event)"
              >
              </ngx-datatable>
            </div>
          </div>
        </article>
      </div>
    </main>
  `
})
export class BoksContentCreateComponent implements OnInit, OnDestroy {
  title: string

  superTitle = 'Nieuwe content toevoegen'

  // Element to be created/edited
  boksContentElement$: Observable<BoksContentElement>

  // The parent of the element that we're creating
  parentElement: BoksNodeElement = undefined

  contentElements$ = this.store.select(fromStore.getSelectedBoksElementContent)

  columnHeaders = [{ prop: 'name', name: 'Naam' }, { prop: 'shortDescription', name: 'Toelichting' }]

  selectedContentElement: any = []

  year: number

  subs = new Subscription()

  /**
   * Actions displayed on this page
   */
  actions: ActionsGroup[] = [
    new ActionsGroup({
      title: 'Acties',
      actions: [
        {
          name: 'Annuleren',
          routerLink: '..'
        }
      ]
    })
  ]

  constructor(
    private readonly store: Store<ApplicationState>,
    private readonly dialogService: DialogService,
    private readonly route: ActivatedRoute,
    private readonly router: Router
  ) {}

  ngOnInit() {
    let event = this.store.select(fromStore.getSelectedBoksElement).subscribe(element => {
      this.parentElement = element
      this.title = this.route.snapshot.data.title + this.parentElement.name
    })
    this.subs.add(event)

    event = this.store.select(getContext).subscribe(context => (this.year = context.year.year))
    this.subs.add(event)
  }

  onSelect(selected: any) {
    if (selected.length === 1) {
      this.boksContentElement$ = of(selected[0] as BoksContentElement)
    }
  }

  onCreate(boksContent: BoksNodeElement) {
    boksContent.parentId = this.parentElement.id
    boksContent.year = '' + this.year
    this.store.dispatch(new fromStore.CreateBoksContent(boksContent))
  }

  onUpdate(boksContent: BoksNodeElement) {
    this.store.dispatch(new fromStore.UpdateBoksContent(boksContent))
  }

  onRemove(boksContent: BoksNodeElement) {
    boksContent.parentId = this.parentElement.id
    this.store.dispatch(new fromStore.DeleteBoksContent(boksContent))
  }

  onCancel() {
    console.log('onCancel')
    this.router.navigate(['..'], { relativeTo: this.route })
  }

  ngOnDestroy() {
    this.subs.unsubscribe()
  }

  canDeactivate() {
    // Allow synchronous navigation (`true`) if no crisis or the crisis is unchanged
    //   if (!this.form.dirty || this.form.untouched) {
    //     console.log('not dirty or untouched')
    //     return true
    //   }
    return this.dialogService.confirm(
      'Je hebt mogelijk wijzigingen in het formulier aangebracht. Wijzigingen weggooien?'
    )
    // }
  }
}

import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core'
import { Observable } from 'rxjs'
import { Store } from '@ngrx/store'

import { getIsAuthenticated } from '@avans/core/src/lib/auth/store'
import { BaseComponent } from '@avans/core/src/lib/entity/components'
import { BoksNodeElement, BoksContentElement } from '../../models'

import { ApplicationState } from '../../../app/store/reducers/app.reducer'
import * as fromStore from '../../store'
import { environment } from '../../../../environments/environment.prod'
import { map } from 'rxjs/operators'

@Component({
  selector: 'inzicht-boks-content-edit',
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './boks-content.move.component.html'
})
export class BoksContentMoveComponent extends BaseComponent implements OnInit {
  // Page title
  title: string

  // The core element of this Component
  boksNodeElements$: Observable<BoksNodeElement[]>
  filterElements$: Observable<any>

  // User authentication role
  userMayEdit$: Observable<boolean>

  constructor(private readonly store: Store<ApplicationState>) {
    super({ level: 1, serverUrl: environment.API_BASE_URL, production: false })
  }

  ngOnInit() {
    this.userMayEdit$ = this.store.select(getIsAuthenticated)
    this.boksNodeElements$ = this.store.select(fromStore.getBoksElementsAsTree)
    this.filterElements$ = this.store.select(fromStore.getToplevelElements).pipe(
      map((items: BoksNodeElement[]) =>
        items.map((item: BoksNodeElement) => {
          return { id: item.id, name: item.name, selected: true }
        })
      )
    )
  }

  onFilterSelectionChanged(filterSelection: any) {
    // console.log('onFilterSelectionChanged', filterSelection)
    this.boksNodeElements$ = this.store
      .select(fromStore.getBoksElementsAsTree)
      .pipe(map(items => items.filter(item => filterSelection[item.id].selected)))
  }

  onItemDropped(event: any) {
    const { item, targetNodeId } = event
    // console.log('onItemDropped item.id: ', item.id, 'targetId:', targetNodeId)
    this.store.dispatch(new fromStore.MoveBoksContent({ item, targetNodeId }))
  }

  onContentAdded(event: { name: string; parentId: number }) {
    console.log('onContentAdded content:', event)
    const item = new BoksContentElement({ ...event, shortDescription: 'Volgt nog', year: '2018' })
    this.store.dispatch(new fromStore.CreateBoksContent(item))
  }
}

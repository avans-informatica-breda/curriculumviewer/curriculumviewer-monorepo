import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core'
import { Observable } from 'rxjs'
import { Store } from '@ngrx/store'

import { getIsAuthenticated } from '@avans/core/src/lib/auth/store'
import { BaseComponent } from '@avans/core/src/lib/entity/components'
import { BoksNodeElement, BoksContentElement } from '../../models'

import * as fromStore from '../../store'
import * as fromCourseStore from '../../../course/store'
import { environment } from '../../../../environments/environment.prod'
import { map } from 'rxjs/operators'
import { Course } from '../../../course/models'
import { ApplicationState } from '../../../app/store/reducers/app.reducer'

/**
 * Component to relate BoksContent to a Course by drag n drop.
 *
 *
 */
@Component({
  selector: 'inzicht-boks-content-relate',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: `
    <main class="viewer__container" *ngIf="courses$ | async as courses">
      <div class="viewer__fullwidth">
        <header>
          <lib-pageheader
            [pageTitle]="'Koppel BOKS inhoud aan onderwijseenheden'"
            [superTitle]="'Overzicht'"
            [actionIsAllowed]="false"
          ></lib-pageheader>
          <section>
            <p>
              Hier zie je een overzicht van onderwijseenheden en de bijbehorende BOKS content. Je kunt hier
              nieuwe BOKS content aanmaken, en je kunt bestaande content verplaatsen door deze naar een andere
              BOKS node te slepen.
            </p>
          </section>
        </header>

        <lib-filter
          [title]="'Leerlijnen'"
          [filters]="filterElements$ | async"
          (selectionChanged)="onFilterSelectionChanged($event)"
        ></lib-filter>

        <article>
          <section>
            <lib-dnd-treeview
              [tree]="courses$ | async"
              (itemDropped)="onItemDropped($event)"
              (contentAdded)="onContentAdded($event)"
            ></lib-dnd-treeview>
          </section>
        </article>

        <footer>
          <p>Ruimte voor footer.</p>
        </footer>
      </div>
    </main>
  `
})
export class BoksContentRelateComponent extends BaseComponent implements OnInit {
  // Page title
  title: string
  courses$: Observable<Course[]>
  filterElements$: Observable<any>
  userMayEdit$: Observable<boolean>

  constructor(private readonly store: Store<ApplicationState>) {
    super({ level: 1, serverUrl: environment.API_BASE_URL, production: environment.production })
  }

  ngOnInit() {
    this.userMayEdit$ = this.store.select(getIsAuthenticated)
    this.courses$ = this.store.select(fromCourseStore.getAllCourses)
    this.filterElements$ = this.store.select(fromStore.getToplevelElements).pipe(
      map((items: BoksNodeElement[]) =>
        items.map((item: BoksNodeElement) => {
          return { id: item.id, name: item.name, selected: true }
        })
      )
    )
  }

  onFilterSelectionChanged(filterSelection: any) {
    console.log('onFilterSelectionChanged', filterSelection)
    // this.boksNodeElements$ = this.store
    //   .select(fromStore.getBoksElementsAsTree)
    //   .pipe(map(items => items.filter(item => filterSelection[item.id].selected)))
  }

  onItemDropped(event: any) {
    const { item, targetNodeId } = event
    console.log('onItemDropped item.id: ', item.id, 'targetId:', targetNodeId)
    // this.store.dispatch(new fromStore.MoveBoksContent({ item, targetNodeId }))
  }

  onContentAdded(event: { name: string; parentId: number }) {
    console.log('onContentAdded content:', event)
    // const item = new BoksContentElement({ ...event, shortDescription: 'Volgt nog', year: '2018' })
    // this.store.dispatch(new fromStore.CreateBoksContent(item))
  }
}

import { Component } from '@angular/core'

@Component({
  selector: 'inzicht-boks',
  template: `
    <inzicht-boks-topnav></inzicht-boks-topnav>
    <div class="app">
      <div class="app__content">
        <div class="app__container">
          <router-outlet></router-outlet>
        </div>
      </div>
    </div>
  `,
  styleUrls: ['boks.component.scss']
})
export class BoksComponent {}

import { NgModule } from '@angular/core'
import { RouterModule, Routes } from '@angular/router'
import { StyleExampleColumnsComponent } from './style-examples/style-example-columns.component'
import { StyleExampleWideComponent } from './style-examples/style-example-wide.component'
import { StyleExampleFullWidthComponent } from './style-examples/style-example-fullwidth.component'
import { StyleExample2Component } from './style-examples/style-example-2.component'
import { AppDashboardComponent } from './app/containers/dashboard/dashboard.component'

const appRoutes: Routes = [
  { path: '', redirectTo: '/dashboard', pathMatch: 'full' },
  { path: 'dashboard', component: AppDashboardComponent, pathMatch: 'full' },
  { path: 'example-columns', component: StyleExampleColumnsComponent },
  { path: 'example-wide', component: StyleExampleWideComponent },
  { path: 'example-fullwidth', component: StyleExampleFullWidthComponent },
  { path: 'example-2', component: StyleExample2Component },
  { path: '**', redirectTo: '/dashboard' }
]

@NgModule({
  imports: [RouterModule.forRoot(appRoutes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}

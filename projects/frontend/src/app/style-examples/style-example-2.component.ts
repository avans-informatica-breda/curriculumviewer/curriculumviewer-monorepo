import { Component } from '@angular/core'

@Component({
  selector: 'inzicht-style-example-2',
  templateUrl: './style-example-2.component.html'
})
export class StyleExample2Component {
  page = 2
  pageSize = 10

  items = [{ name: 'test' }, { name: 'test' }, { name: 'test' }, { name: 'test' }, { name: 'test' }]
}

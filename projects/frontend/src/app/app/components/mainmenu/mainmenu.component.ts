import { Component, OnInit } from '@angular/core'
import { Observable, of } from 'rxjs'
import { Router } from '@angular/router'
import { Store } from '@ngrx/store'
import * as fromAuth from '@avans/core/src/lib/auth/store'

import { getIsAuthenticated, getUserName, getAuthEntity } from '@avans/core'

@Component({
  selector: 'inzicht-mainmenu',
  templateUrl: './mainmenu.component.html'
})
export class MainMenuComponent implements OnInit {
  title = 'InZicht'

  isLoggedIn$: Observable<boolean>
  isAdmin$: Observable<boolean>
  isSuperUser$: Observable<boolean>
  fullName$: Observable<string | null>

  constructor(private readonly store: Store<fromAuth.AuthState>, private readonly router: Router) {}

  ngOnInit() {
    this.isLoggedIn$ = this.store.select(getIsAuthenticated)
    this.isAdmin$ = of(true) // this.authService.isAdminUser;
    this.isSuperUser$ = of(true) // this.authService.isSuperUser;
    this.fullName$ = this.store.select(getUserName)
  }

  onLogin() {
    this.router.navigate(['/login'])
  }

  onLogout() {
    this.store.dispatch(new fromAuth.LogOut())
    this.router.navigate(['/'])
  }
}

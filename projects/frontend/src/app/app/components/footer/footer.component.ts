import { Component, OnInit } from '@angular/core'
import { Observable, of } from 'rxjs'
import { Store } from '@ngrx/store'

@Component({
  selector: 'inzicht-footer',
  templateUrl: './footer.component.html'
})
export class FooterComponent implements OnInit {
  isLoggedIn$: Observable<boolean>
  pageLoaded$: Observable<boolean>

  constructor() {}

  ngOnInit() {
    this.isLoggedIn$ = of(true)
  }
}

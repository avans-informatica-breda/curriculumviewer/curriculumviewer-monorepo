import { Component, OnInit } from '@angular/core'
import { Store } from '@ngrx/store'

import * as fromContext from '@avans/core/src/lib/context/store'
import { Observable } from 'rxjs'

import { getYear, getFullYear, ContextState } from '@avans/core'

@Component({
  selector: 'inzicht-dashboard',
  templateUrl: './dashboard.component.html'
})
export class AppDashboardComponent implements OnInit {
  title = 'Dashboard'

  year$: Observable<number> | undefined
  fullYear$: Observable<string | null> | undefined

  constructor(private readonly store: Store<ContextState>) {}

  ngOnInit() {
    this.year$ = this.store.select(getYear)
    this.fullYear$ = this.store.select(getFullYear)
  }

  onYearSelected(year: number) {
    console.log('onYearSelected', year)
    this.store.dispatch(new fromContext.ChangeYear({ year }))
  }
}

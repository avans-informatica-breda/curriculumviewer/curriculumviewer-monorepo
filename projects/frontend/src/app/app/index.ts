import { FooterComponent } from './components/footer/footer.component'
import { MainMenuComponent } from './components/mainmenu/mainmenu.component'
import { AppDashboardComponent } from './containers/dashboard/dashboard.component'
import { MainComponent } from './containers/main/main.component'

export const components = [FooterComponent, MainMenuComponent, AppDashboardComponent, MainComponent]

export { MainComponent } from './containers/main/main.component'

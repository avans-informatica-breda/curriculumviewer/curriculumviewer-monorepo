import { ActionReducerMap, createFeatureSelector } from '@ngrx/store'
import * as fromBoks from '../../../boks/store/reducers/boks-node.reducer'
import * as fromCourse from '../../../course/store/reducers/course.reducer'
import * as fromObjective from '../../../objective/store/reducers/objective.reducer'
import * as fromModule from '../../../modules/store/reducers/module.reducer'
import * as fromEndqualification from '../../../endqualification/store/reducers/endqualification.reducer'

// Hier ontstaat de state tree
export interface ApplicationState {
  boksNodes: fromBoks.BoksState
  courses: fromCourse.CourseState
  modules: fromModule.ModuleState
  objectives: fromObjective.ObjectiveState
  endqualifications: fromEndqualification.EndqualificationState
}

// De ActionReducerMap zorgt voor typechecking. We kunnen niet zo maar functies toevoegen;
// deze moeten nu uit de State komen.
export const reducers: ActionReducerMap<ApplicationState> = {
  // koppel items aan de reducer function
  boksNodes: fromBoks.reducer,
  courses: fromCourse.reducer,
  modules: fromModule.reducer,
  objectives: fromObjective.reducer,
  endqualifications: fromEndqualification.reducer
}

export const getAppState = createFeatureSelector<ApplicationState>('application')

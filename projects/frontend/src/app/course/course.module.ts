import { NgModule } from '@angular/core'
import { HttpModule } from '@angular/http'
import { ModuleWithProviders } from '@angular/core'
import { CommonModule } from '@angular/common'
import { FormsModule, ReactiveFormsModule } from '@angular/forms'
import { NgbModule } from '@ng-bootstrap/ng-bootstrap'
import { QuillModule } from 'ngx-quill'
import { NgxDatatableModule } from '@swimlane/ngx-datatable'

import { CoreModule } from '@avans/core'
import { UiModule } from '@avans/ui'

import { CourseRoutingModule } from './course.routing'
import * as fromComponents from './components'
import * as fromContainers from './containers'
import * as fromGuards from './guards'
import * as fromServices from './services'
import { EndqualificationModule } from '../endqualification/endqualification.module'
import { environment } from '../../environments/environment'

@NgModule({
  imports: [
    CommonModule,
    HttpModule,
    FormsModule,
    ReactiveFormsModule,
    NgxDatatableModule,
    CoreModule.forRoot({ level: 1, serverUrl: environment.API_BASE_URL, production: environment.production }),
    EndqualificationModule,
    UiModule,
    QuillModule,
    NgbModule,
    CourseRoutingModule
  ],
  declarations: [...fromContainers.containers, ...fromComponents.components],
  providers: [...fromServices.services, ...fromGuards.guards],
  exports: [...fromContainers.containers, ...fromComponents.components]
})
export class CourseModule {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: CourseModule,
      providers: [...fromServices.services]
    }
  }
}

/**
 *
 */
import { Entity } from '@avans/core'
import { Person } from '@avans/core'
import { Module } from '../../modules/models'

export class Course extends Entity {
  // Het jaar waarin deze trimester gegeven wordt, bv. 2018.
  // Er zou per cohort slechts één trimester mogen zijn.
  cohort: number

  // Het jaar waarin de trimester gegeven wordt, bv. jaar 2.
  studyYear: string // number ?? -> gaf problemen bij lezen van radiobutton; nog oplossen;

  // Opeenvolgend nummer van de trimester, bv. 3.
  trimester: string // number ?? -> gaf problemen bij lezen van radiobutton; nog oplossen;

  // Regulier of verkort
  form: string

  // Afkorting van de academie
  academy: string

  // De naam van de opleiding
  education: string

  // De afkorting van de opleiding, max 3 letters (E, M, TI, BIM, I, ...)
  educationAbbrev: string

  // Benaming van de trimester, bv. 'Basisvaardigheden 1'.
  name: string

  // Korte beschrijving, 1 zin.
  shortDescription: string

  // Uitgebreide toelichting zoals in de trimesterwijzer staat.
  longDescription: string

  // Jaar/cohort van de studie, bv 2018, 2019.
  year: string

  // Id van docent trimester-eigenaar
  responsiblePerson: Person

  // De vakken/modules in deze trimester.
  modules: Module[]

  constructor(values: any) {
    super(values)
    try {
      this.name = values.name
      this.shortDescription = values.shortDescription
      this.longDescription = values.longDescription
      this.responsiblePerson = values.responsiblePerson
      this.cohort = values.cohort
      this.studyYear = values.studyYear
      this.trimester = values.trimester
      this.form = values.form
      this.academy = values.academy || 'AE&I'
      this.education = values.education || 'Informatica'
      this.educationAbbrev = values.educationAbbrev || 'I'
      this.modules = values.modules
      this.year = values.year
    } catch (e) {
      console.error('Error in Course constructor: ' + e.toString())
    }
  }

  /**
   * Build the Blackboard course code.
   */
  public get blackboardCode(): string {
    const year = this.cohort + 1
    return `${this.academy}-${this.educationAbbrev}-${this.studyYear}.${this.trimester}-${this.cohort}-${year}`
  }

  /**
   * Bereken aantal studiepunten voor deze trimester op basis van de ECs van de gekoppelde modules.
   */
  public get credits(): number {
    let credits = 0.0
    this.modules.forEach(module => (credits += module.credits))
    return credits
  }
}

import { Injectable } from '@angular/core'
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router'
import { Store } from '@ngrx/store'
import { Observable, of } from 'rxjs'
import { map, tap, filter, take, switchMap, catchError } from 'rxjs/operators'

import * as fromStore from '../store'
import { ApplicationState } from '../../app/store/reducers/app.reducer'

@Injectable()
export class CourseGuard implements CanActivate {
  constructor(private store: Store<ApplicationState>) {}

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> {
    return this.checkStore().pipe(
      switchMap(() => of(true)),
      catchError(() => of(false))
    )
  }

  canActivateChild(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> {
    return this.canActivate(route, state)
  }

  checkStore(): Observable<boolean> {
    return this.store.select(fromStore.getCoursesLoaded).pipe(
      tap(loaded => {
        if (!loaded) {
          this.store.dispatch(new fromStore.LoadCourses())
        }
      }),
      filter(loaded => loaded),
      take(1)
    )
  }
}

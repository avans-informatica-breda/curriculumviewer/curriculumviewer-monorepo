import { CourseFormComponent } from './course-form/course-form.component'
import { CourseInfoComponent } from './course-info/course-info.component'
import { CourseSelectorComponent } from './course-selector/course-selector.component'
import { topnavComponents } from './course-topnav'

export const components: any[] = [
  topnavComponents,
  CourseFormComponent,
  CourseInfoComponent,
  CourseSelectorComponent
]

export * from './course-topnav'
export * from './course-form/course-form.component'
export * from './course-info/course-info.component'
export * from './course-selector/course-selector.component'

import { Component, Input } from '@angular/core'
import { Course } from '../../models'

@Component({
  selector: 'inzicht-course-info',
  template: `
    <h3>{{ title }}</h3>
    <table class="table">
      <tbody>
        <tr>
          <td>Jaar</td>
          <td>{{ course.studyYear }}</td>
        </tr>
        <tr>
          <td>Periode</td>
          <td>{{ course.trimester }}</td>
        </tr>
        <tr>
          <td>Studiejaar</td>
          <td>{{ course.year }} - {{ course.year + 1 }}</td>
        </tr>
        <tr>
          <td>Eigenaar</td>
          <td>{{ course.responsiblePerson.firstName }} {{ course.responsiblePerson.lastName }}</td>
        </tr>
        <tr>
          <td>Vorm</td>
          <td>{{ course.form }}</td>
        </tr>
      </tbody>
    </table>
  `,
  styles: ['.table td { padding: 8px; font-size: 0.9em; }']
})
export class CourseInfoComponent {
  title = 'Periodeinfo'
  @Input() course: Course
}

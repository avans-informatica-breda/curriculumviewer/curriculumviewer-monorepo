import { Component, OnInit, OnDestroy, ChangeDetectionStrategy } from '@angular/core'
import { Router, ActivatedRoute } from '@angular/router'
import { Observable, Subscription } from 'rxjs'
import { Store } from '@ngrx/store'

import { BaseComponent, getIsAuthenticated } from '@avans/core'
import { ActionsGroup } from '@avans/ui'

import { Course } from '../../models'
import * as fromStore from '../../store'
import * as fromConstants from '../../course.constants'
import { environment } from '../../../../environments/environment'
import { ApplicationState } from '../../../app/store/reducers/app.reducer'

@Component({
  selector: 'inzicht-course-detail',
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './course-detail.component.html'
})
export class CourseDetailComponent extends BaseComponent implements OnInit, OnDestroy {
  // Logging classname tag
  readonly TAG = CourseDetailComponent.name

  // Modal identifier.
  public readonly DIALOG_ADD_SUBNODE = 'DialogAddSubnode'

  // Page title
  title: string

  // The core element of this Component
  // course$: Observable<Course>
  course: Course

  // User authentication role
  userMayEdit$: Observable<boolean>

  // Subscription on observable
  subscription: Subscription

  /**
   * Actions on this page
   */
  actions: ActionsGroup[] = [
    new ActionsGroup({
      title: 'Acties',
      actions: [
        {
          name: 'Toevoegen',
          routerLink: 'new'
        },
        {
          name: 'Verwijderen',
          routerLink: 'todo'
        }
      ]
    })
  ]

  constructor(private readonly store: Store<ApplicationState>, private readonly router: Router) {
    super({ level: 1, serverUrl: environment.API_BASE_URL, production: environment.production })
  }

  ngOnInit() {
    this.userMayEdit$ = this.store.select(getIsAuthenticated)

    this.subscription = this.store.select(fromStore.getSelectedCourse).subscribe((result: Course) => {
      this.course = result
    })
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe()
  }

  onEdit(id: number) {
    this.router.navigate([fromConstants.BASE_ROUTE, id, 'edit'])
  }
}

import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core'
import { Router, ActivatedRoute } from '@angular/router'
import { Observable } from 'rxjs'
import { NGXLogger } from 'ngx-logger'
import { Store } from '@ngrx/store'

import { ActionsGroup } from '@avans/ui'
import { getIsAuthenticated, getFullYear } from '@avans/core'

import * as fromStore from '../../store'
import * as fromConstants from '../../course.constants'

import { Course } from '../../models'
import { ApplicationState } from '../../../app/store/reducers/app.reducer'

@Component({
  selector: 'inzicht-course-list',
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './course-list.component.html'
})
export class CourseListComponent implements OnInit {
  // Class name for logging
  static TAG = CourseListComponent.name

  title = 'Periodes'

  selectedYear$: Observable<string>

  // The core element of this Component
  courses$: Observable<Course[]>

  // User authentication role
  userMayEdit$: Observable<boolean>

  columnHeaders = [
    { prop: 'studyYear', name: 'Jaar', flexGrow: 1 },
    { prop: 'trimester', name: 'Periode', flexGrow: 1 },
    { prop: 'name', name: 'Naam', flexGrow: 5 },
    { prop: 'responsiblePerson.firstName', name: 'Eigenaar', flexGrow: 3 }
  ]

  selected: any = []

  /**
   * Actions op deze page
   */
  actions: ActionsGroup[] = [
    new ActionsGroup({
      title: 'Acties',
      actions: [
        {
          name: 'Element toevoegen',
          routerLink: 'new'
        },
        {
          name: 'Leerdoel overzicht',
          routerLink: 'todo'
        }
      ]
    }),
    new ActionsGroup({
      title: 'Andere acties',
      actions: [
        {
          name: 'Elementen verwijderen',
          routerLink: 'edit'
        }
      ]
    })
  ]

  constructor(
    private readonly store: Store<ApplicationState>,
    private readonly logger: NGXLogger,
    private readonly router: Router,
    private readonly route: ActivatedRoute
  ) {}

  ngOnInit() {
    console.log('CourseList onInit')
    this.userMayEdit$ = this.store.select(getIsAuthenticated)
    this.courses$ = this.store.select(fromStore.getAllCourses)
    this.selectedYear$ = this.store.select(getFullYear)
  }

  onAddElement() {
    this.logger.debug(CourseListComponent.TAG, 'Add element')
    this.router.navigate([fromConstants.BASE_ROUTE, 'new'])
  }

  onSelect({ selected }: any) {
    if (selected.length === 1) {
      const element = selected[0] as Course
      this.logger.debug(CourseListComponent.TAG, 'onSelect id = ' + element.id)
      this.router.navigate([element.id], { relativeTo: this.route })
    }
  }
}

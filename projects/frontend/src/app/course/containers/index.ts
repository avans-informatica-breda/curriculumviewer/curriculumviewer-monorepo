import { CourseComponent } from './course.component'
import { CourseDetailComponent } from './course-detail/course-detail.component'
import { CourseListComponent } from './course-list/course-list.component'
import { CourseEditComponent } from './course-edit/course-edit.component'

export const containers: any[] = [
  CourseComponent,
  CourseDetailComponent,
  CourseListComponent,
  CourseEditComponent
]

export * from './course.component'
export * from './course-detail/course-detail.component'
export * from './course-edit/course-edit.component'
export * from './course-list/course-list.component'

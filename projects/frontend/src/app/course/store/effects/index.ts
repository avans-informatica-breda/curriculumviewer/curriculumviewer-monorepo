import { CourseEffects } from './course.effect'

export const effects: any[] = [CourseEffects]

export * from './course.effect'

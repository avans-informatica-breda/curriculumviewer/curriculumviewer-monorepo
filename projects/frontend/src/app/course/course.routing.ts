import { NgModule } from '@angular/core'
import { RouterModule, Routes } from '@angular/router'

import * as fromAuth from '@avans/core'
import { PersonGuard } from '@avans/core'

import * as fromGuards from './guards'
import * as fromEndqualificationsGuards from '../endqualification/guards'
import * as fromContainers from './containers'
import * as fromConstants from './course.constants'

const routes: Routes = [
  {
    path: fromConstants.BASE_ROUTE,
    component: fromContainers.CourseComponent,
    canActivate: [fromGuards.CourseGuard],
    children: [
      {
        path: '',
        pathMatch: 'full',
        component: fromContainers.CourseListComponent
      },
      {
        path: 'new',
        component: fromContainers.CourseEditComponent,
        canActivate: [
          fromAuth.IsLoggedInGuard,
          fromEndqualificationsGuards.EndqualificationGuard,
          PersonGuard
        ],
        canDeactivate: [fromAuth.CanDeactivateGuard],
        data: {
          title: 'Nieuwe Periode',
          optionalLearningline: true
        }
      },
      {
        path: ':courseId',
        component: fromContainers.CourseDetailComponent,
        canActivate: [fromGuards.CourseExistsGuard]
      },
      {
        path: ':courseId/edit',
        component: fromContainers.CourseEditComponent,
        canActivate: [
          fromGuards.CourseExistsGuard,
          fromEndqualificationsGuards.EndqualificationGuard,
          PersonGuard
        ],
        canDeactivate: [fromAuth.CanDeactivateGuard],
        data: {
          title: 'Wijzig Periode'
        }
      }
    ]
  },
  {
    path: fromConstants.BASE_ROUTE + '**',
    redirectTo: fromConstants.BASE_ROUTE
  }
]

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CourseRoutingModule {}

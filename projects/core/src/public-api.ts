/*
 * Public API Surface of core
 */
export { CoreModule } from './core.module'

export * from './lib/entity/models/entity.model'
export * from './lib/entity/components/basecomponent/base.component'
export * from './lib/entity/components/entity-detail/entity-detail.component'
export * from './lib/entity/services/entity.service'

export * from './lib/context/components/year-selector/year-selector.component'
export * from './lib/context/store/selectors/context.selectors'
export * from './lib/context/store/reducers/index'
// export * from './lib/context/store/reducers/year.reducers'

export * from './lib/users/services/dialog.service'
export * from './lib/users/services/user.service'

export * from './lib/auth/store/selectors/auth.selectors'
export * from './lib/auth/guards/can-deactivate.guard'
export * from './lib/auth/guards/is-logged-in.guard'

export * from './lib/alert/models/alert.model'
export * from './lib/alert/store/actions/alert.actions'

export * from './lib/person/models/person.model'
export * from './lib/person/guards/person.guard'

export * from './lib/router/store/selectors/router.selectors'
export * from './lib/router/store/effects/router.effects'

export class CoreConfig {
  // Dummy number
  level: number

  logLevel?: string

  // Base URL of backend used by EntityService
  serverUrl: string

  // When not in production, BeseComponent is debug-enabled.
  production: boolean
}

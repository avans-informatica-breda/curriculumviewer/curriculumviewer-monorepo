import { Action } from '@ngrx/store'
import { AlertInfo } from '../../models/alert.model'

export const SUCCESS = '[Alert] Alert Success'
export const ERROR = '[Alert] Alert Error'
export const WARNING = '[Alert] Alert Warn'

export class AlertSuccess implements Action {
  readonly type = SUCCESS
  constructor(public payload: AlertInfo) {}
}

export class AlertError implements Action {
  readonly type = ERROR
  constructor(public payload: AlertInfo) {}
}

export class AlertWarn implements Action {
  readonly type = WARNING
  constructor(public payload: AlertInfo) {}
}

export type AlertActions = AlertSuccess | AlertWarn | AlertError

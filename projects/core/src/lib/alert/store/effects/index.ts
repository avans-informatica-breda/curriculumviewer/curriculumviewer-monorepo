import { AlertEffects } from './alert.effects'

export const effects: any[] = [AlertEffects]

export * from './alert.effects'

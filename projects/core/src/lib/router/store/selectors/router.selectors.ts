import * as fromRouter from '@ngrx/router-store'
import { createSelector } from '@ngrx/store'
import * as fromFeature from '../reducers'

//
// Selectors - zijn nodig om door delen van de state tree te navigeren
// They return slices of the state tree.
//
export const getRouterState = createSelector(
  fromFeature.getRouterState,
  (state: fromRouter.RouterReducerState<fromFeature.RouterStateUrl>) => state.state
)

export const getRouterParams = createSelector(
  getRouterState,
  (state: fromFeature.RouterStateUrl) => state.params
)

import { Component, ChangeDetectionStrategy, OnInit, Output, EventEmitter, Input } from '@angular/core'
import { FormGroup, FormBuilder, Validators } from '@angular/forms'

@Component({
  selector: 'lib-year-selector',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: `
    <!-- ------------ Korte beschrijving --------------------------- -->
    <form [formGroup]="form">
      <div class="row">
        <div class="form-group col-xs-8">
          <label for="yearSelection">Jaar</label>
          <select formControlName="yearSelection" class="form-control year-selection" required>
            <option *ngFor="let year of availableYears" [value]="year.nr">
              {{ year.fullName }}
            </option>
          </select>
        </div>
      </div>
    </form>
  `
})
export class YearSelectorComponent implements OnInit {
  availableYears = [
    { nr: 2016, fullName: '2016-2017' },
    { nr: 2017, fullName: '2017-2018' },
    { nr: 2018, fullName: '2018-2019' },
    { nr: 2019, fullName: '2019-2020' },
    { nr: 2020, fullName: '2020-2021' }
  ]

  @Input()
  year!: number
  @Output() selectedYear = new EventEmitter<number>()

  form!: FormGroup

  constructor(private fb: FormBuilder) {}

  ngOnInit(): void {
    this.form = this.fb.group({
      yearSelection: [this.year, Validators.required]
    })

    this.form.controls.yearSelection.valueChanges.subscribe(year => {
      this.selectedYear.emit(year)
      this.form.markAsDirty()
    })
  }
}

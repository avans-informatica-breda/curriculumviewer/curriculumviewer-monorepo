import { Observable } from "rxjs";
import { Component, OnInit } from "@angular/core";
import { ActivatedRoute, Params } from "@angular/router";

// import { ActionsGroup } from '@avans/ui'

// import { UsersService } from '../../services'
import { User } from "../../models";

@Component({
	selector: "cv-user-detail",
	templateUrl: "./user-detail.component.html",
	styles: []
})
export class UserDetailComponent implements OnInit {
	title: string;
	id: number;
	user: User;
	isLoggedIn$: Observable<boolean>;
	isAdmin$: Observable<boolean>;
	isActive: boolean;
	private readonly ID = "id";

	// actions: ActionsGroup[] = [
	//   new ActionsGroup({
	//     title: 'Acties op gebruiker',
	//     actions: [
	//       {
	//         name: 'Wijzig gebruiker',
	//         routerLink: 'edit'
	//       },
	//       {
	//         name: 'Wijzig rollen',
	//         routerLink: 'edit/roles'
	//       }
	//     ]
	//   }),
	//   new ActionsGroup({
	//     title: 'Andere acties',
	//     actions: [
	//       {
	//         name: 'Toevoegen',
	//         routerLink: '/users/new'
	//       },
	//       {
	//         name: 'Verwijderen',
	//         routerLink: 'edit'
	//       }
	//     ]
	//   })
	// ]

	constructor(
		private readonly route: ActivatedRoute
	) // private readonly userService: UsersService, // private readonly authService: AuthService
	{}

	ngOnInit() {
		// this.title = this.route.snapshot.data['title'] || 'User Details'
		// this.isLoggedIn$ = this.authService.isLoggedInUser
		// this.isAdmin$ = this.authService.isAdminUser
		// this.route.params.subscribe((params: Params) => {
		//   this.id = +params[this.ID]
		//   this.userService.read(this.id).subscribe(user => {
		//     this.user = new User(user)
		//     console.dir(this.user)
		//   })
		// })
	}
}

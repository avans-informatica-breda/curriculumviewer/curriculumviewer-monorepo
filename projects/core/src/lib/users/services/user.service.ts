import { Injectable } from '@angular/core'
import { HttpClient } from '@angular/common/http'

import { EntityService } from '../../entity/services'

import { User } from '../models/user.model'
import { CoreConfig } from '../../../core.config'

@Injectable()
export class UsersService extends EntityService<User> {
  constructor(http: HttpClient, config: CoreConfig) {
    super(http, config.serverUrl, 'users')
  }
}

import { Injectable } from '@angular/core'
import { Actions, ofType, Effect } from '@ngrx/effects'

import * as fromRouter from '@avans/core/router'
import * as authActions from '../actions'
import * as fromServices from '../../services'

import { switchMap, map, catchError } from 'rxjs/operators'
import { of } from 'rxjs'

@Injectable()
export class UserEffects {
  constructor(private actions$: Actions, private userService: fromServices.UsersService) {}

  @Effect()
  loadUsers$ = this.actions$.pipe(
    ofType(authActions.LOAD_USERS),
    switchMap(() =>
      this.userService.list().pipe(
        map(users => new authActions.LoadUsersSuccess(users)),
        catchError(error => of(new authActions.LoadUsersFail(error)))
      )
    )
  )

  //
  // CreateUser the user via the service
  //
  @Effect()
  createUser$ = this.actions$.pipe(
    ofType(authActions.CREATE_USER),
    map((action: authActions.CreateUser) => action.payload),
    switchMap(fromPayload =>
      this.userService.create(fromPayload).pipe(
        map(user => new authActions.CreateUserSuccess(user)),
        catchError(error => of(new authActions.LoadUsersFail(error)))
      )
    )
  )

  //
  // CreateUserSuccess - navigate to the user after it has been created.
  //
  @Effect()
  createUserSuccess$ = this.actions$.pipe(
    ofType(authActions.CREATE_USER_SUCCESS),
    map((action: authActions.CreateUserSuccess) => action.payload),
    map(user => {
      return new fromRouter.Go({
        path: ['/products', user.id]
      })
    })
  )

  //
  // Update the user via the service
  //
  @Effect()
  updateUser$ = this.actions$.pipe(
    ofType(authActions.UPDATE_USER),
    map((action: authActions.UpdateUser) => action.payload),
    switchMap(fromPayload =>
      this.userService.update(fromPayload).pipe(
        map(user => new authActions.UpdateUserSuccess(user)),
        catchError(error => of(new authActions.UpdateUserFail(error)))
      )
    )
    // switchMap(fromPayload => this.userService.updateUser(fromPayload)),
    // map(user => new authActions.UpdateUserSuccess(user)),
    // catchError(error => of(new authActions.UpdateUserFail(error)))
  )

  @Effect()
  deleteUser$ = this.actions$.pipe(
    ofType(authActions.DELETE_USER),
    map((action: authActions.DeleteUser) => action.payload),
    switchMap(user =>
      this.userService.delete(user.id).pipe(
        // userService.remove returns nothing, so we return
        // the deleted user ourselves on success
        map(() => new authActions.DeleteUserSuccess(user)),
        catchError(error => of(new authActions.DeleteUserFail(error)))
      )
    )
  )

  //
  // CreateUserSuccess - navigate to the user after it has been created.
  //
  @Effect()
  handleUserSuccess$ = this.actions$.pipe(
    ofType(authActions.UPDATE_USER_SUCCESS, authActions.DELETE_USER_SUCCESS),
    map(() => {
      return new fromRouter.Go({
        path: ['/users']
      })
    })
  )
}

import { Action } from '@ngrx/store'
import { User } from '../../models'

//
// Load actions
//
export const LOAD_USERS = '[Authentication] Load Users'
export const LOAD_USERS_FAIL = '[Authentication] Load Users Fail'
export const LOAD_USERS_SUCCESS = '[Authentication] Load Users Success'

// Action creators
export class LoadUsers implements Action {
  readonly type = LOAD_USERS
}

export class LoadUsersFail implements Action {
  readonly type = LOAD_USERS_FAIL
  constructor(public payload: any) {}
}

export class LoadUsersSuccess implements Action {
  readonly type = LOAD_USERS_SUCCESS
  constructor(public payload: User[]) {}
}

//
// Create User Actions
//
export const CREATE_USER = '[Authentication] Create User'
export const CREATE_USER_FAIL = '[Authentication] Create User Fail'
export const CREATE_USER_SUCCESS = '[Authentication] Create User Success'

// Action creators
export class CreateUser implements Action {
  readonly type = CREATE_USER
  constructor(public payload: User) {}
}

export class CreateUserFail implements Action {
  readonly type = CREATE_USER_FAIL
  constructor(public payload: any) {}
}

export class CreateUserSuccess implements Action {
  readonly type = CREATE_USER_SUCCESS
  constructor(public payload: User) {}
}

//
// Update User Actions
//
export const UPDATE_USER = '[Authentication] Update User'
export const UPDATE_USER_FAIL = '[Authentication] Update User Fail'
export const UPDATE_USER_SUCCESS = '[Authentication] Update User Success'

// Action creators
export class UpdateUser implements Action {
  readonly type = UPDATE_USER
  constructor(public payload: User) {}
}

export class UpdateUserFail implements Action {
  readonly type = UPDATE_USER_FAIL
  constructor(public payload: any) {}
}

export class UpdateUserSuccess implements Action {
  readonly type = UPDATE_USER_SUCCESS
  constructor(public payload: User) {}
}

//
// Delete User Actions
//
export const DELETE_USER = '[Authentication] Delete User'
export const DELETE_USER_FAIL = '[Authentication] Delete User Fail'
export const DELETE_USER_SUCCESS = '[Authentication] Delete User Success'

// Action creators
export class DeleteUser implements Action {
  readonly type = DELETE_USER
  constructor(public payload: User) {}
}

export class DeleteUserFail implements Action {
  readonly type = DELETE_USER_FAIL
  constructor(public payload: any) {}
}

export class DeleteUserSuccess implements Action {
  readonly type = DELETE_USER_SUCCESS
  constructor(public payload: User) {}
}

// action types
export type UsersAction =
  | LoadUsers
  | LoadUsersFail
  | LoadUsersSuccess
  | CreateUser
  | CreateUserFail
  | CreateUserSuccess
  | UpdateUser
  | UpdateUserFail
  | UpdateUserSuccess
  | DeleteUser
  | DeleteUserFail
  | DeleteUserSuccess

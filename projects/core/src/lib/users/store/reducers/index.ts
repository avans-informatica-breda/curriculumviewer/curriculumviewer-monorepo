import * as fromUsers from './users.reducers'
import { ActionReducerMap, createFeatureSelector } from '@ngrx/store'

// Hier ontstaat de state tree
export interface UserState {
  user: fromUsers.UserState
}

// De ActionReducerMap zorgt voor typechecking. We kunnen niet zo maar functies toevoegen;
// deze moeten nu uit de Usertate komen.
export const reducers: ActionReducerMap<UserState> = {
  // koppel users aan de reducer function
  user: fromUsers.reducer
}

export const getUsers = createFeatureSelector<UserState>('users')

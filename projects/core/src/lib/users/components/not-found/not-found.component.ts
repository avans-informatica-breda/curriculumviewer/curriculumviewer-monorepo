import { Component } from '@angular/core'

@Component({
  selector: 'lib-not-found',
  templateUrl: './not-found.component.html',
  styleUrls: []
})
export class NotFoundComponent {}

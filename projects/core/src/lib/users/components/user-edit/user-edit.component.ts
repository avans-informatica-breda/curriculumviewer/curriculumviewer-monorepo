import { Component, OnInit } from '@angular/core'
import { ActivatedRoute, Router } from '@angular/router'

@Component({
  selector: 'lib-user-edit',
  templateUrl: './user-edit.component.html',
  styles: []
})
export class UserEditComponent implements OnInit {
  title: string | undefined

  constructor(private readonly route: ActivatedRoute, private readonly router: Router) {}

  ngOnInit() {
    this.title = this.route.snapshot.data.title
  }

  saveChanges() {
    console.log('save changes and reroute')
    this.router.navigate(['..'], { relativeTo: this.route })
  }
}

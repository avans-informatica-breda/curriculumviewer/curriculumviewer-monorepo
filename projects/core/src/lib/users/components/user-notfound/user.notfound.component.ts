import { Component, Input, OnInit } from '@angular/core'
import { ActivatedRoute } from '@angular/router'

@Component({
  selector: 'lib-user-not-found',
  templateUrl: './user.notfound.component.html'
})
export class UserNotFoundComponent implements OnInit {
  @Input()
  index!: number
  title!: string

  constructor(private readonly route: ActivatedRoute) {}

  ngOnInit() {
    this.title = this.route.snapshot.data.title || 'User not found'
  }
}

import { TestBed, async } from '@angular/core/testing'
import { UserTopnavComponent } from './user-topnav.component'
import { CoreModule } from '../../../../core.module'
import { StoreModule } from '@ngrx/store'
import { EffectsModule } from '@ngrx/effects'

import { reducer as AuthReducer } from '../../../auth/store/reducers/auth.reducers'
import { reducer as contextReducer } from '../../../context/store/reducers/year.reducers'
import { reducers as RouterReducer } from '../../../router/store/reducers/router.reducers'
import { StoreRouterConnectingModule } from '@ngrx/router-store'

describe('UserTopnavComponent', () => {
  beforeAll(async(() => {
    jasmine.DEFAULT_TIMEOUT_INTERVAL = 30000
  }))

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [],
      imports: [
        //
        // imports not fully working yet, needs some more attention.
        //
        CoreModule,
        //
        // StoreModule.forRoot(reducers, { metaReducers }),
        StoreModule.forRoot({}),
        //
        // EffectsModule.forRoot(effects),
        // EffectsModule.forFeature([ContextEffects, AuthEffects, RouterEffects, AlertEffects])
        //
        StoreModule.forFeature('auth', AuthReducer),
        StoreModule.forFeature('router', RouterReducer),
        StoreModule.forFeature('context', contextReducer),
        //
        StoreRouterConnectingModule.forRoot()
      ]
    }).compileComponents()
  }))

  it.skip('should create the app', async(() => {
    const fixture = TestBed.createComponent(UserTopnavComponent)
    const app = fixture.debugElement.componentInstance
    expect(app).toBeTruthy()
  }))

  /**
   * We need more testcases here.
   */
})

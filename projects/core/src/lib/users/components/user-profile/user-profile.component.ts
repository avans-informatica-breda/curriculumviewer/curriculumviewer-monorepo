import { Component, OnInit } from '@angular/core'
import { ActivatedRoute } from '@angular/router'
import { Observable, of } from 'rxjs'

import { User } from '../../models'

@Component({
  selector: 'lib-user-profile',
  templateUrl: './user-profile.component.html',
  styles: []
})
export class UserProfileComponent implements OnInit {
  title: string
  id: number
  user: User
  isLoggedIn$: Observable<boolean>
  isActive: boolean

  constructor(
    private readonly route: ActivatedRoute // private readonly userService: UsersService, // private readonly authService: AuthService
  ) {}

  ngOnInit() {
    this.title = this.route.snapshot.data.title || 'User Details'
    this.isLoggedIn$ = of(true) // this.authService.isLoggedInUser;

    // this.authService.getUserFromLocalStorage().subscribe(
    //   user => this.user = new User(user)
    //   //
    //   // Op basis van UserId alle items van deze user ophalen voor overzicht?
    //   //
    // )
  }
}

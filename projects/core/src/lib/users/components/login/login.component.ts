import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core'
import { FormControl, FormGroup, Validators } from '@angular/forms'
import { Store } from '@ngrx/store'

import * as fromStore from '../../../auth/store'
import * as fromActions from '../../../auth/store/actions'

@Component({
  selector: 'lib-login',
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup | undefined
  title = 'Sign In'

  constructor(private store: Store<fromStore.AuthState>) {}

  ngOnInit() {
    console.log('onInit')

    this.loginForm = new FormGroup({
      email: new FormControl(null, [Validators.required, this.validEmail.bind(this)]),
      password: new FormControl(null, [Validators.required, this.validPassword.bind(this)])
    })
  }

  onSubmit() {
    if (this.loginForm.valid) {
      const email = this.loginForm.value['email']
      const password = this.loginForm.value['password']
      //
      // ToDo link to our backend login process
      // this.store.dispatch(new fromActions.LogIn({ email, password }))
      //
      this.store.dispatch(new fromActions.LogInSuccess({ test: 'test' }))
    } else {
      this.store.dispatch(new fromActions.LogInFail({ errorMessage: 'Invalid form' }))
    }
  }

  validEmail(control: FormControl): { [s: string]: boolean } {
    const email = control.value
    const regexp = new RegExp('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$')
    if (regexp.test(email) !== true) {
      return { email: false }
    } else {
      return null
    }
  }

  validPassword(control: FormControl): { [s: string]: boolean } {
    const password = control.value
    const regexp = new RegExp('^[a-zA-Z]([a-zA-Z0-9]){2,14}')
    const test = regexp.test(password)
    if (regexp.test(password) !== true) {
      return { password: false }
    } else {
      return null
    }
  }
}

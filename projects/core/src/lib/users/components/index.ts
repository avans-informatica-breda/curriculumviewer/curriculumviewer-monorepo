import { NotFoundComponent } from './not-found/not-found.component'
import { NotAllowedComponent } from './not-allowed/not-allowed.component'
import { UserEditComponent } from './user-edit/user-edit.component'
import { UserNotFoundComponent } from './user-notfound/user.notfound.component'
import { UserProfileComponent } from './user-profile/user-profile.component'
import { UserTopnavComponent } from './user-topnav/user-topnav.component'
import { LoginComponent } from './login/login.component'

export const components: any[] = [
  LoginComponent,
  NotFoundComponent,
  NotAllowedComponent,
  UserEditComponent,
  UserNotFoundComponent,
  UserProfileComponent,
  UserTopnavComponent
]

export * from './login/login.component'
export * from './not-allowed/not-allowed.component'
export * from './not-found/not-found.component'
export * from './user-edit/user-edit.component'
export * from './user-notfound/user.notfound.component'
export * from './user-profile/user-profile.component'
export * from './user-topnav/user-topnav.component'

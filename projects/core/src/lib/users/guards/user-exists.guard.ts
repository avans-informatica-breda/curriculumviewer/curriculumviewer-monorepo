import { Injectable } from '@angular/core'
import { CanActivate, ActivatedRouteSnapshot } from '@angular/router'

import { Store } from '@ngrx/store'
import { Observable, of } from 'rxjs'
import { tap, filter, take, switchMap, catchError, map } from 'rxjs/operators'

import * as fromStore from '../store'
import { User } from '../models/user.model'

@Injectable()
export class UserExistsGuard implements CanActivate {
  constructor(private store: Store<fromStore.UserState>) {}

  canActivate(route: ActivatedRouteSnapshot): Observable<boolean> {
    return this.checkStore().pipe(
      switchMap(() => {
        const id = +route.params.pizzaId
        return this.hasUser(id)
      })
    )
  }

  hasUser(id: number): Observable<boolean> {
    return this.store.select(fromStore.getAllUsers).pipe(
      // Get the entity with given id, and convert to boolean
      // true or false indicates wether the pizza exists.
      map((entities: { [key: number]: User }) => !!entities[id]),
      take(1)
    )
  }

  checkStore(): Observable<boolean> {
    return this.store.select(fromStore.getUsersLoaded).pipe(
      tap(loaded => {
        if (!loaded) {
          // load items from the store
          this.store.dispatch(new fromStore.LoadUsers())
        }
      }),
      // this filter construct waits for loaded to become true
      filter(loaded => loaded),
      // this take completes the observable and unsubscribes
      take(1)
    )
  }
}

import { EntityDetailComponent } from './entity-detail/entity-detail.component'
import { BaseComponent } from './basecomponent/base.component'

export const components: any[] = [EntityDetailComponent, BaseComponent]

export * from './entity-detail/entity-detail.component'
export * from './basecomponent/base.component'

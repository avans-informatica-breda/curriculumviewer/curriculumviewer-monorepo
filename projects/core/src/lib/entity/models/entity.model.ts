import { UserInterface } from '../../users/models'
import { Person } from '../../person/models'

/**
 * Base class for all entities that are part of communication to/from services.
 */
interface EntityInterface {
  // Uniek identificatienr
  readonly id: number

  // Optionele tekst waarin status of opmerkingen over bewerkingsacties
  // kunnen worden geschreven.
  readonly administrativeRemark?: string | undefined

  // Meta informatie
  readonly createdAt: Date
  readonly createdBy: UserInterface
  readonly updatedAt: Date | undefined
  readonly lastUpdatedBy: Person | undefined
}

export abstract class Entity implements EntityInterface {
  readonly id!: number
  readonly administrativeRemark?: string | undefined
  readonly createdAt: Date
  readonly createdBy: UserInterface
  readonly updatedAt: Date | undefined
  readonly lastUpdatedBy: Person | undefined

  constructor(values: EntityInterface) {
    this.id = values.id
    this.administrativeRemark = values['administrativeRemark'] || undefined
    this.createdAt = values['createdAt'] || undefined
    this.createdBy = values['createdBy'] || undefined
    this.updatedAt = values['updatedAt'] || undefined
    this.lastUpdatedBy = values['lastUpdatedBy'] || undefined
  }
}

/**
 * Interface defining functions for (de)serializing objects from/to JSON
 */
export interface Serializer<T extends Entity> {
  fromJson(json: any): T
  toJson(resource: T): any
}

/**
 * Generic serializer immplementation class.
 */
export class EntitySerializer<T extends Entity> implements Serializer<T> {
  fromJson(json: any): T {
    return JSON.parse(json)
  }

  toJson(resource: T): any {
    return JSON.stringify(resource)
  }
}

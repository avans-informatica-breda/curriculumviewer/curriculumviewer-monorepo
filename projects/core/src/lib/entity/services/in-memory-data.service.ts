import { InMemoryDbService } from 'angular-in-memory-web-api'
// import { HboIArchitectureLayer, BloomTaxonomy } from '@inzicht-objective/models/objective.model'
import { UserRole } from '../../users/models/user.model'

export class InMemoryDataService implements InMemoryDbService {
  users: {}
  meta: {}

  constructor() {
    // Dummy user info voor gebruik in meta
    // Niet gebruikt om in te loggen.
    this.users = {
      ruud: {
        id: 12345,
        data: {
          firstname: 'Ruud',
          lastname: 'Hermans',
          email: 'rl.hermans@avans.nl'
        }
      },
      pascal: {
        id: 12346,
        data: {
          firstname: 'Pascal',
          lastname: 'van Gastel',
          email: 'p.vangastel@avans.nl'
        }
      }
    }

    // Dummy meta informatie
    this.meta = {
      createdAt: new Date(),
      createdBy: this.users['ruud']
    }
  }

  createDb() {
    const courses = [
      {
        id: 0,
        meta: {
          createdAt: new Date(),
          createdBy: this.users['ruud'],
          updatedAt: undefined,
          updatedBy: undefined,
          validFrom: new Date(Date.UTC(2018, 8, 1)),
          validUntil: new Date(Date.UTC(2019, 8, 1))
        },
        responsiblePerson: 'Ruud Hermans',
        name: 'Basisvaardigheden 1',
        cohort: 2019,
        studyYear: '1',
        periode: '1',
        academy: 'AE&I',
        form: 'Regulier',
        education: 'Informatica',
        educationAbbrev: 'I',
        shortDescription: 'Dit is de eerste periode van jaar 1.',
        longDescription:
          'Welkom! Je hebt gekozen voor de opleiding Informatica aan de Academie voor Engineering & ICT ' +
          '(AE&I) van Avans. Dit is de periodewijzer van ‘Periode 1.1: Basisvaardigheden 1’. Iedere periode ' +
          'heeft een periodewijzer waarin relevante informatie over de belangrijkste onderwijsactiviteiten ' +
          'staat.Naast de periodewijzer wordt er ook andere informatie op BlackBoard geplaatst. In deze periode ' +
          'krijg je basisvaardigheden van het vakgebied Informatica aangeleerd. Deze vaardigheden vormen het ' +
          'fundament waarop we in volgende perioden voortbouwen.Het volgen van lessen, en vooral het ' +
          'oefenen door het maken van opdrachten, zijn noodzakelijk om succesvol de opleiding te volgen.',
        modules: [
          {
            id: 0,
            name: 'Studievaardigheden',
            shortDescription:
              'Het is van belang dat je als kersverse student je snel thuis voelt op je nieuwe school, ' +
              'er de weg weet en door hebt wat er van je wordt verwacht. Je zult merken dat het hbo ' +
              'veel meer overlaat aan de zelfstandigheid van de student. Als je niet naar de les komt ' +
              'worden je ouders niet gebeld, als je je toets niet maakt is er geen docent die je achter ' +
              'je broek zit om het in te halen.',
            exams: [
              {
                name: 'Studievaardigheden',
                osiriscode: 'EIIN-STVH',
                credits: 1.0
              }
            ]
          },
          {
            id: 1,
            name: 'Beroepsoriëntatie',
            shortDescription:
              'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. ' +
              'Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus ' +
              'mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat ' +
              'massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. ' +
              'In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu ' +
              'pede mollis pretium.',
            exams: [
              {
                osiriscode: 'EIIN-BERORT',
                credits: 1.5
              }
            ]
          },
          {
            id: 2,
            name: 'Nederlands',
            shortDescription:
              'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. ' +
              'Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus ' +
              'mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat ' +
              'massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. ' +
              'In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu ' +
              'pede mollis pretium.',
            exams: [
              {
                osiriscode: 'EIIN-NEDL',
                credits: 0.5
              }
            ]
          },
          {
            id: 3,
            name: 'Webdesign',
            shortDescription:
              'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. ' +
              'Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus ' +
              'mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat ' +
              'massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. ' +
              'In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu ' +
              'pede mollis pretium.',
            exams: [
              {
                osiriscode: 'EIIN-WEBDES',
                credits: 3.0
              }
            ]
          },
          {
            id: 4,
            name: 'Programmeren 1',
            shortDescription:
              'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. ' +
              'Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus ' +
              'mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat ' +
              'massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. ' +
              'In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu ' +
              'pede mollis pretium.',
            exams: [
              {
                osiriscode: 'EIIN-PROGR1',
                credits: 4.0
              }
            ]
          },
          {
            id: 5,
            name: 'Bedrijfsprocessen',
            shortDescription:
              'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. ' +
              'Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus ' +
              'mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat ' +
              'massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. ' +
              'In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu ' +
              'pede mollis pretium.',
            exams: [
              {
                osiriscode: 'EIIN-BDRPR1',
                credits: 2.0
              }
            ]
          },
          {
            id: 6,
            name: 'Relationele Databases 1',
            shortDescription:
              'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. ' +
              'Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus ' +
              'mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat ' +
              'massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. ' +
              'In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu ' +
              'pede mollis pretium.',
            exams: [
              {
                osiriscode: 'EIIN-RELDAT1',
                credits: 3.0
              }
            ]
          }
        ]
      },
      {
        id: 1,
        meta: {
          createdAt: new Date(),
          createdBy: {
            id: 12345,
            data: {
              firstname: 'Ruud',
              lastname: 'Hermans',
              email: 'rl.hermans@avans.nl'
            }
          },
          updatedAt: undefined,
          updatedBy: undefined,
          validFrom: new Date(Date.UTC(2018, 8, 1)),
          validUntil: new Date(Date.UTC(2019, 8, 1))
        },
        name: 'Basisvaardigheden 2',
        cohort: 2019,
        studyYear: '1',
        periode: '2',
        academy: 'AE&I',
        form: 'Regulier',
        education: 'Informatica',
        educationAbbrev: 'I',
        shortDescription:
          'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. ' +
          'Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus ' +
          'mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat ' +
          'massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. ' +
          'In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu ' +
          'pede mollis pretium.',
        longDescription:
          'Met de afsluiting van periode 1.1 heb je als het goed is een begin gemaakt met de verwerving ' +
          'van de basisvaardigheden. Je kunt al aardig met Java overweg, hebt een idee van hoe je een ' +
          'website ontwerpt, hebt leren reflecteren en je kent de basisopzet van een verslag. Ook heb je ' +
          'een basisbegrip van bedrijfsprocessen en ken je de beginselen van het vakgebied rond ' +
          'gegevensopslag in een relationele database. Iedere periode heeft een periodewijzer waarin relevante ' +
          'informatie over de belangrijkste onderwijsactiviteiten staat.Naast de periodewijzer wordt ' +
          'er ook andere informatie op BlackBoard geplaatst. In deze periode krijg je basisvaardigheden ' +
          'van het vakgebied Informatica aangeleerd.Deze vaardigheden vormen het fundament waarop we ' +
          'in volgende perioden voortbouwen.Het volgen van lessen, en vooral het oefenen door het ' +
          'maken van opdrachten, zijn noodzakelijk om succesvol de opleiding te volgen.',
        responsiblePerson: 'Eefje Gijzen',
        modules: []
      }
    ]

    //
    // Modules of vakken. De studiepunten komen uit het gekoppelde examenonderdeel.
    //
    let modules = [
      {
        id: 0,
        meta: {
          createdAt: new Date(),
          createdBy: {
            id: 12345,
            data: {
              firstname: 'Ruud',
              lastname: 'Hermans',
              email: 'rl.hermans@avans.nl'
            }
          },
          updatedAt: undefined,
          updatedBy: undefined,
          validFrom: new Date(Date.UTC(2018, 8, 1)),
          validUntil: new Date(Date.UTC(2019, 8, 1))
        },
        form: 'Regulier',
        name: 'Studievaardigheden',
        shortDescription:
          'Binnen het vak Studievaardigheden leer de algemene vaardigheden die belangrijk zijn voor je studie. Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. ' +
          'Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus ' +
          'mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat ' +
          'massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. ' +
          'In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu ' +
          'pede mollis pretium.',
        longDescription:
          'Studievaardigheden zijn een onmisbaar element van een HBO studie. Meer info volgt hier uit de leerwijzer. Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. ' +
          'Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus ' +
          'mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat ' +
          'massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. ' +
          'In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu ' +
          'pede mollis pretium. Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. ' +
          'Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus ' +
          'mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat ' +
          'massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. ' +
          'In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu ' +
          'pede mollis pretium.',
        responsiblePerson: 'Evert Jan de Voogt',
        cohort: 2018,
        studyYear: 1,
        periode: 2,
        attempts: {
          firstAttempt: 'Hier de beschrijving van de toetsing. Dit is de reguliere kans, poging 1.',
          secondAttempt:
            'De herkansing vindt plaats in de volgende periode. Herkansing bestaat uit het verbeteren en aanvullen van de oorsponkelijke opdracht.'
        },
        // Examenonderdeel van deze module
        exams: [
          {
            id: 0,
            osiriscode: 'EIIN-STVH',
            name: 'Studievaardigheden',
            // examtype: [ExamType.REFLECTION],
            credits: 1.0,
            // resultscale: ExamResultScale.VLD_NVD,
            minimumRequired: '5,5', // String, want kan ook een woord zijn, VLD of Voldaan
            weightinPercentage: 100, // Max 100 %
            mutations: 'Vrijstelling voor verkorte traject.',
            lengthInMinutes: '100',
            // language: ExamLanguage.NL,
            responsiblePerson: 'Ruud Hermans'
          }
        ],
        // De course(s) waar deze module deel van uitmaakt.
        course: [
          {
            name: 'Basisvaardigheden 1',
            cohort: 2019,
            studyYear: '1',
            periode: '1',
            academy: 'AE&I',
            form: 'Regulier',
            education: 'Informatica'
          }
        ],
        boksElements: [
          {
            name: 'Object-oriënted programmeren',
            shortDescription: 'Hier meer info over dit boks element.',
            externalInfoUrl: 'http://www.wikipedia/oo-programmeren'
          },
          {
            name: 'Java',
            shortDescription: 'Hier meer info over dit boks element.',
            externalInfoUrl: 'http://www.java.com'
          }
        ],
        leaningLine: {
          name: 'Programmeren',
          shortDescription:
            'De leerlijn Programmeren heeft betrekking op alle vakken die met het vakgebied van programmeren en programmertalen te maken hebben.',
          responsiblePerson: 'Gerard Wagenaar'
        },
        objectives: [
          {
            id: 1,
            name:
              'Hier een leerdoel, waarin de student een actie onderneemt, zodanig dat de student iets leert'
          },
          {
            id: 2,
            name:
              'Hier een ander leerdoel, waarin de student een actie onderneemt, zodanig dat de student iets leert'
          },
          {
            id: 3,
            name:
              'Hier nog een leerdoel, waarin de student een actie onderneemt, zodanig dat de student iets leert'
          }
        ]
      },
      {
        id: 1,
        meta: {
          createdAt: new Date(),
          createdBy: {
            id: 12345,
            data: {
              firstname: 'Ruud',
              lastname: 'Hermans',
              email: 'rl.hermans@avans.nl'
            }
          },
          updatedAt: undefined,
          updatedBy: undefined,
          validFrom: new Date(Date.UTC(2018, 8, 1)),
          validUntil: new Date(Date.UTC(2019, 8, 1))
        },
        shortDescription:
          ' In de module Programmeren 1 leer je de fundamenten van het programmeren. Je leert hoe je variabelen kunt toepassen, hoe je methodes maakt, en hoe jij je eigen logica kunt maken en door een programma kunt laten uitvoeren.',
        longDescription:
          'In deze eerste periode van de opleiding Informatica ligt de nadruk op het leren van basisvaardigheden. Eén van die basisvaardigheden is programmeren. In de module Programmeren 1 leer je de fundamenten van het programmeren. Je leert hoe je variabelen kunt toepassen, hoe je methodes maakt, en hoe jij je eigen logica kunt maken en door een programma kunt laten uitvoeren. Je maakt ook kennis met het beheer van versies van je programmacode, en je leert hoe je je code kunt testen. Je zult zelfs een koppeling maken vanuit jouw programma naar een database. In de lessen leer je de theorie van het programmeren, maar je zult vooral zelf aan de slag gaan – programmeren is een vaardigheid die je vooral leert door het zelf veel te doen. De docent zal daarom veel voorbeelden laten zien, en gezamenlijk maak je tijdens de les opdrachten. Daarbij maken we gebruik van een omgeving waarin jij zelf kunt zien of jouw uitwerking correct is of nog fouten bevat. ',
        responsiblePerson: 'Arno Broeders',
        cohort: 2018,
        studyYear: 1,
        periode: 1,
        form: 'Regulier',
        name: 'Programmeren 1',
        exams: [
          {
            id: 0,
            osiriscode: 'EIIN-PROGR1',
            name: 'Programmeren 1',
            // examtype: [ExamType.VAARDIGHEID, ExamType.KENNIS],
            credits: 4.0,
            // resultscale: ExamResultScale.NUMERIEK,
            minimumRequired: '5,5',
            weightinPercentage: 100, // Max 100 %
            mutations: 'Vrijstelling voor verkorte traject.',
            lengthInMinutes: '100',
            // language: ExamLanguage.NL_ENG,
            responsiblePerson: 'Ruud Hermans'
          }
        ],
        // De course(s) waar deze module deel van uitmaakt.
        courses: [
          {
            name: 'Basisvaardigheden 1',
            cohort: 2019,
            studyYear: '1',
            periode: '1',
            academy: 'AE&I',
            form: 'Regulier',
            education: 'Informatica',
            blackboardCode: 'AE&I-I-1.1-2018-2019'
          }
        ],
        boksElements: [
          {
            name: 'Object-oriënted programmeren',
            shortDescription: 'Hier meer info over dit boks element.',
            externalInfoUrl: 'http://www.wikipedia/oo-programmeren'
          },
          {
            name: 'Java',
            shortDescription: 'Hier meer info over dit boks element.',
            externalInfoUrl: 'http://www.java.com'
          }
        ],
        learningline: {
          name: 'Programmeren',
          shortDescription:
            'De leerlijn Programmeren heeft betrekking op alle vakken die met het vakgebied van programmeren en programmertalen te maken hebben.',
          responsiblePerson: 'Gerard Wagenaar'
        },
        objectives: [
          {
            id: 1,
            name:
              'De student begrijpt vanuit conceptueel oogpunt wat een computer is, zodanig dat hij de begrippen processor, programmeeromgeving, besturingssyteem en geheugen kan plaatsen.'
          },
          {
            id: 2,
            name:
              'De student kan in een programmeertaal de concepten control flow en datatypes toepassen, zodanig dat hij een applicatie kan maken.'
          },
          {
            id: 3,
            name:
              'De student kan in een programmeertaal een console applicatie maken, zodanig dat hij een toepassing maakt die door invoer en uitvoer bediend kan worden.'
          },
          {
            id: 4,
            name:
              'De student kan in een programmeeromgeving gebruik maken van een debugger, zodanig dat hij inzicht verwerft in de onderliggende werking van zijn applicatie en fouten uit de programmeercode kan verwijderen.'
          },
          {
            id: 5,
            name:
              'De student kent de concepten met betrekking tot primitieve datatypen, zodanig dat hij deze op de juiste manier en in de juiste context kan toepassen.'
          },
          {
            id: 6,
            name:
              'De student gebruikt een programmeeromgeving (IDE), zodanig dat hij een applicatie kan maken, kan debuggen en kan uitvoeren'
          },
          {
            id: 7,
            name:
              'De student past versie beheer toe op zijn applicatie, zodanig dat hij versies van zijn programmeercode bijhoudt en op professionele wijze kan samenwerken bij het realiseren van een applicatie.'
          }
        ]
      },
      {
        id: 2,
        meta: {
          createdAt: new Date(),
          createdBy: {
            id: 12345,
            data: {
              firstname: 'Ruud',
              lastname: 'Hermans',
              email: 'rl.hermans@avans.nl'
            }
          },
          updatedAt: undefined,
          updatedBy: undefined,
          validFrom: new Date(Date.UTC(2018, 8, 1)),
          validUntil: new Date(Date.UTC(2019, 8, 1))
        },
        responsiblePerson: 'Alexander van den Bulck',
        cohort: 2018,
        studyYear: 1,
        periode: 3,
        exams: [],
        form: 'Regulier',
        name: 'Relationele Databases 1',
        shortDescription: 'Hier leer je engels.',
        longDescription: 'Hier de lange beschrijving.'
      },
      {
        id: 3,
        meta: {
          createdAt: new Date(),
          createdBy: {
            id: 12345,
            data: {
              firstname: 'Ruud',
              lastname: 'Hermans',
              email: 'rl.hermans@avans.nl'
            }
          },
          updatedAt: undefined,
          updatedBy: undefined,
          validFrom: new Date(Date.UTC(2018, 8, 1)),
          validUntil: new Date(Date.UTC(2019, 8, 1))
        },
        responsiblePerson: 'Ger Oosting',
        cohort: 2018,
        studyYear: 1,
        periode: 2,
        exams: [],
        form: 'Regulier',
        name: 'Web-design',
        shortDescription: 'Hier leer je engels.',
        longDescription: 'Hier de lange beschrijving.'
      }
    ]

    // Leerdoelen
    const objectives = [
      {
        id: 1,
        meta: {
          createdAt: new Date(),
          createdBy: {
            id: 12345,
            data: {
              firstname: 'Ruud',
              lastname: 'Hermans',
              email: 'rl.hermans@avans.nl'
            }
          },
          updatedAt: undefined,
          updatedBy: undefined,
          validFrom: new Date(Date.UTC(2018, 8, 1)),
          validUntil: new Date(Date.UTC(2019, 8, 1))
        },
        name:
          'De student begrijpt vanuit conceptueel oogpunt wat een computer is,' +
          'zodanig dat hij de begrippen processor, programmeeromgeving, besturingssyteem en' +
          'geheugen kan plaatsen.',
        shortDescription: 'Hier een beschrijving van dit leerdoel als toelichting',
        longDescription: 'Hier eventueel een uitgebreidere toelichting.',
        module: {
          id: 1,
          name: 'Programmeren 1',
          periode: 1,
          studyYear: 1
        },
        learningLine: {
          id: 1,
          name: 'Programmeren'
        },
        course: {
          id: 3,
          name: 'Basisvaardigheden 1',
          cohort: 2019,
          studyYear: '1',
          periode: '1',
          academy: 'AE&I',
          form: 'Regulier',
          education: 'Informatica',
          blackboardCode: 'AE&I-I-1.1-2018-2019'
        },
        endqualifications: [
          {
            areaTag: 'A',
            areaName: 'Plan',
            competenceLevel: '6',
            competenceName: 'Application Design',
            proficiencyLevel: 1
          },
          {
            areaTag: 'B',
            areaName: 'Build',
            competenceLevel: '4',
            competenceName: 'Solution Deployment',
            proficiencyLevel: 2
          }
        ],
        // bloomTaxonomy: BloomTaxonomy.NOT_APPLICABLE,
        // hboIArchitectureLayer: [HboIArchitectureLayer.SOFTWARE],
        responsiblePerson: 'Erco Argante'
      },
      {
        id: 2,
        name:
          'De student kan in een programmeertaal de concepten control flow en datatypes toepassen, zodanig dat hij een applicatie kan maken.',
        module: {
          id: 1,
          name: 'Relationele Databases 1',
          periode: 1,
          studyYear: 1
        },
        learningLine: {
          id: 1,
          name: 'Databases'
        }
      },
      {
        id: 3,
        name:
          'De student kan in een programmeertaal een console applicatie maken, zodanig dat hij een toepassing maakt die door invoer en uitvoer bediend kan worden.',
        module: {},
        learningLine: {
          id: 1,
          name: 'Programmeren'
        }
      },
      {
        id: 4,
        name:
          'De student kan in een programmeeromgeving gebruik maken van een debugger, zodanig dat hij inzicht verwerft in de onderliggende werking van zijn applicatie en fouten uit de programmeercode kan verwijderen.',
        module: {},
        learningLine: {
          id: 1,
          name: 'Databases'
        }
      },
      {
        id: 5,
        name:
          'De student kent de concepten met betrekking tot primitieve datatypen, zodanig dat hij deze op de juiste manier en in de juiste context kan toepassen.',
        learningLine: {
          id: 1,
          name: 'Professionele Vaardigheden'
        }
      },
      {
        id: 6,
        name:
          'De student gebruikt een programmeeromgeving (IDE), zodanig dat hij een applicatie kan maken, kan debuggen en kan uitvoeren',
        module: {
          id: 1,
          name: 'Business Intelligence',
          periode: 3,
          studyYear: 2
        },
        learningLine: {
          id: 1,
          name: 'Professionele Vaardigheden'
        }
      },
      {
        id: 7,
        name:
          'De student past versie beheer toe op zijn applicatie, zodanig dat hij versies van zijn programmeercode bijhoudt en op professionele wijze kan samenwerken bij het realiseren van een applicatie.',
        module: {
          id: 1,
          name: 'Business Intelligence',
          periode: 3,
          studyYear: 2
        },
        learningLine: {
          id: 1,
          name: 'Professionele Vaardigheden'
        }
      }
    ]

    //
    // BOKS Node elementen
    //
    let boksnodeelements = [
      {
        id: 123,
        // In de JSON die de API retourneert zou dit element volledig beschreven moeten zijn.
        // Hier nu verkort genoteerd vanwege efficiency.
        meta: this.meta,
        name: 'Programmeren',
        shortDescription:
          'Deze leerlijn groepeert alle leerdoelen en onderwijseenheden die betrekking hebben tot het gebied programmeren.',
        longDescription: 'Programmeren is een belangrijk onderdeel van de opleiding',
        parent: undefined,
        // Zou mooi zijn als hier het level bij kon staan.
        // if parent == undefined => level = 0
        // if parent == defined => level = (level parent + 1)
        // Misschien via trigger in database automatisch laten bepalen?
        level: 0,
        // Sommige boks elementen op level 0 zijn een leerlijn.
        // Alleen elementen op level 0 kunnen een leerzijn zijn
        isLearningLine: true,
        childNodes: [
          {
            id: 124,
            name: 'OO programmeren'
          },
          {
            id: 125,
            name: 'Functioneel programmeren'
          }
        ],
        contentNodes: []
      },
      {
        id: 124,
        meta: this.meta,
        name: 'OO programmeren',
        shortDescription: 'OO Programmeren is een belangrijk onderdeel van de opleiding',
        longDescription: 'OO Programmeren is een belangrijk onderdeel van de opleiding',
        parent: {
          id: 123,
          name: 'Programmeren'
        },
        level: 1,
        childNodes: [
          {
            id: 126,
            name: 'Java',
            shortDescription: 'Hier een toelichting.'
          },
          {
            id: 127,
            name: 'Angular',
            shortDescription: 'Hier een toelichting.'
          },
          {
            id: 128,
            name: 'C#',
            shortDescription: 'Hier een toelichting.'
          }
        ],
        isLearningLine: false
      },
      {
        id: 125,
        meta: this.meta,
        name: 'Functioneel Programmeren',
        shortDescription: 'Functioneel programmeren is een belangrijk onderdeel van de opleiding',
        longDescription: 'Hier eventueel een uitgebreidere toelichting.',
        parent: {
          id: 123,
          name: 'Programmeren'
        },
        level: 1,
        childNodes: [
          {
            id: 126,
            name: 'Functies',
            shortDescription: 'Hier een korte toelichting.'
          }
        ],
        isLearningLine: false
      },
      {
        id: 126,
        meta: this.meta,
        name: 'Java',
        shortDescription: 'Java programmeren is een belangrijk onderdeel van de opleiding',
        longDescription: 'Hier eventueel een uitgebreidere toelichting.',
        parent: {
          id: 124,
          name: 'OO Programmeren'
        },
        level: 2,
        childNodes: [
          {
            id: 129,
            name: 'Threads',
            shortDescription: 'Hier een korte toelichting.'
          },
          {
            id: 130,
            name: 'Inheritance',
            shortDescription: 'Hier een korte toelichting.'
          }
        ],
        contentNodes: [
          {
            id: 131,
            name: 'Polymorfisme',
            shortDescription: 'Hier een korte toelichting.'
          }
        ],
        isLearningLine: false
      },
      {
        id: 127,
        meta: this.meta,
        name: 'Angular',
        shortDescription: 'Angular is een belangrijk onderdeel van de opleiding',
        longDescription: 'Hier eventueel een uitgebreidere toelichting.',
        parent: {
          id: 124,
          name: 'OO Programmeren'
        },
        level: 2,
        childNodes: [],
        isLearningLine: false
      },
      {
        id: 128,
        meta: this.meta,
        name: 'C#',
        shortDescription: 'C# is een belangrijk onderdeel van de opleiding',
        longDescription: 'Hier eventueel een uitgebreidere toelichting.',
        parent: {
          id: 124,
          name: 'OO Programmeren'
        },
        level: 2,
        childNodes: [],
        isLearningLine: false
      },
      {
        id: 129,
        meta: this.meta,
        name: 'Threads',
        shortDescription: 'Threads is een belangrijk onderdeel van de opleiding',
        longDescription: 'Hier eventueel een uitgebreidere toelichting.',
        parent: {
          id: 126,
          name: 'Java'
        },
        level: 3,
        childNodes: [],
        isLearningLine: false
      },
      {
        id: 130,
        meta: this.meta,
        name: 'Inheritance',
        shortDescription: 'Inheritance is een belangrijk onderdeel van de opleiding',
        longDescription: 'Hier eventueel een uitgebreidere toelichting.',
        parent: {
          id: 126,
          name: 'Java'
        },
        level: 3,
        childNodes: [],
        isLearningLine: false
      },
      {
        id: 131,
        meta: this.meta,
        name: 'Polymorfisme',
        shortDescription: 'Polymorfisme is een belangrijk onderdeel van de opleiding',
        longDescription: 'Hier eventueel een uitgebreidere toelichting.',
        parent: {
          id: 126,
          name: 'Java'
        },
        level: 3,
        childNodes: [],
        isLearningLine: false
      },
      {
        id: 132,
        meta: this.meta,
        name: 'Databases',
        shortDescription: 'Databases is een belangrijk onderdeel van de opleiding',
        longDescription: 'Hier eventueel een uitgebreidere toelichting.',
        parent: undefined,
        level: 0,
        childNodes: [],
        isLearningLine: true
      }
    ]

    // Eindkwalificaties
    let qualifications = []

    let users = [
      {
        id: 12346,
        meta: {
          createdAt: new Date(),
          createdBy: {
            id: 12345,
            data: {
              firstname: 'Ruud',
              lastname: 'Hermans',
              email: 'rl.hermans@avans.nl'
            }
          },
          updatedAt: undefined,
          updatedBy: undefined,
          validFrom: new Date(Date.UTC(2018, 8, 1)),
          validUntil: new Date(Date.UTC(2019, 8, 1))
        },
        data: {
          firstname: 'Plain',
          lastname: 'User',
          email: 'basic@avans.nl'
        },
        roles: [UserRole.Basic],
        token: 'oijWsdfadflgkja,mRGKnwriJWRIjwrgknrfgijwrjkNIOwrg]ijw'
      },
      {
        id: 12347,
        meta: {
          createdAt: new Date(),
          createdBy: {
            id: 12345,
            data: {
              firstname: 'Ruud',
              lastname: 'Hermans',
              email: 'rl.hermans@avans.nl'
            }
          },
          updatedAt: undefined,
          updatedBy: undefined,
          validFrom: new Date(Date.UTC(2018, 8, 1)),
          validUntil: new Date(Date.UTC(2019, 8, 1))
        },
        data: {
          firstname: 'Viewer',
          lastname: 'User',
          email: 'viewer@avans.nl'
        },
        roles: [UserRole.Viewer],
        token: 'oijWEsdfmnEFIOjjNRGKnwriJWRIjwrgknrfgijwrjkNIOwrg]ijw'
      },
      {
        id: 12348,
        meta: {
          createdAt: new Date(),
          createdBy: {
            id: 12345,
            data: {
              firstname: 'Ruud',
              lastname: 'Hermans',
              email: 'rl.hermans@avans.nl'
            }
          },
          updatedAt: undefined,
          updatedBy: undefined,
          validFrom: new Date(Date.UTC(2018, 8, 1)),
          validUntil: new Date(Date.UTC(2019, 8, 1))
        },
        data: {
          firstname: 'Editor',
          lastname: 'User',
          email: 'editor@avans.nl'
        },
        roles: [UserRole.Editor],
        token: 'oijWEsdfmnEFIOjjNRGKnwriJWRIjwrgknrfgijwrjkNIOwrg]ijw'
      },
      {
        id: 12343,
        meta: {
          createdAt: new Date(),
          createdBy: {
            id: 12345,
            data: {
              firstname: 'Ruud',
              lastname: 'Hermans',
              email: 'rl.hermans@avans.nl'
            }
          },
          updatedAt: undefined,
          updatedBy: undefined,
          validFrom: new Date(Date.UTC(2018, 8, 1)),
          validUntil: new Date(Date.UTC(2019, 8, 1))
        },
        data: {
          firstname: 'Admin',
          lastname: 'User',
          email: 'admin@avans.nl'
        },
        roles: [UserRole.Admin],
        token: 'oijWEsdfmnEFIOjjNRGKnwriJWRIjwrgknrfgijwrjkNIOwrg]ijw'
      },
      {
        id: 12344,
        meta: {
          createdAt: new Date(),
          createdBy: {
            id: 12345,
            data: {
              firstname: 'Ruud',
              lastname: 'Hermans',
              email: 'rl.hermans@avans.nl'
            }
          },
          updatedAt: undefined,
          updatedBy: undefined,
          validFrom: new Date(Date.UTC(2018, 8, 1)),
          validUntil: new Date(Date.UTC(2019, 8, 1))
        },
        data: {
          firstname: 'Super',
          lastname: 'User',
          email: 'super@avans.nl'
        },
        roles: [UserRole.SuperUser],
        token: 'oijWEsdfmnEFIOjjNRGKnwriJWRIjwrgknrfgijwrjkNIOwrg]ijw'
      }
    ]

    // Demo: Hardcoded docentnamen; zouden van de API moeten komen.
    const persons = [
      { id: 1, firstName: 'Ruud', lastName: 'Hermans' },
      { id: 2, firstName: 'Pascal', lastName: 'van Gastel' },
      { id: 3, firstName: 'Jan', lastName: 'Montizaan' },
      { id: 4, firstName: 'Gitta', lastName: 'de Vaan' },
      { id: 5, firstName: 'Eefje', lastName: 'Gijzen' },
      { id: 6, firstName: 'Erco', lastName: 'Argante' },
      { id: 7, firstName: 'Arno', lastName: 'Broeders' },
      { id: 8, firstName: 'Robin', lastName: 'Schellius' },
      { id: 9, firstName: 'Johan', lastName: 'Smarius' },
      { id: 10, firstName: 'Peter', lastName: 'Vos' },
      { id: 11, firstName: 'Heidie', lastName: 'Tops' },
      { id: 12, firstName: 'Ger', lastName: 'Oosting' },
      { id: 13, firstName: 'Evert', lastName: 'Jan de Voogt' },
      { id: 14, firstName: 'Gerard', lastName: 'Wagenaar' },
      { id: 15, firstName: 'Qurratulain', lastName: 'Mubarak' },
      { id: 16, firstName: 'Erik', lastName: 'Kuiper' }
    ]

    return {
      courses,
      modules,
      boksnodeelements,
      objectives,
      qualifications,
      users,
      persons
    }
  }
}

import { Injectable } from '@angular/core'
import { Observable, of, EMPTY, throwError } from 'rxjs'
import { HttpClient, HttpHeaders } from '@angular/common/http'
import { tap, catchError, map } from 'rxjs/operators'
import { NGXLogger } from 'ngx-logger'

import { EntityService } from '../../entity/services'
import { User, UserRole, StorageUser } from '../../users/models'
import { CoreConfig } from '../../../core.config'

@Injectable({
  providedIn: 'root'
})
export class AuthService extends EntityService<User> {
  // Logging classname tag
  static readonly TAG = AuthService.name

  // store the URL so we can redirect after logging in
  public readonly redirectUrl = '/dashboard'

  private readonly headers = new HttpHeaders({
    'Content-Type': 'application/json'
  })

  constructor(private readonly logger: NGXLogger, http: HttpClient, private config: CoreConfig) {
    super(http, config.serverUrl, 'users')
  }

  login(email: string, password: string): Observable<User> {
    this.logger.debug(AuthService.TAG, 'login')
    const API_LOGIN = this.config.serverUrl + '/login'
    this.logger.debug(AuthService.TAG, 'login via hardcoded URL ', API_LOGIN)
    return this.http.post(API_LOGIN, { email, password }, { headers: this.headers }).pipe(
      map(result => result as User),
      catchError(this.handleError)
    )
  }

  public logout(): Observable<boolean> {
    this.logger.debug(AuthService.TAG, 'logout - remove local user')
    localStorage.removeItem('userName')
    localStorage.removeItem('userRoles')
    localStorage.removeItem('token')
    return of(true)
  }

  public getUserFromLocalStorage(): Observable<StorageUser> {
    console.log('getuser from storage')
    const token = localStorage.getItem('token')
    if (!token || token === null) {
      return throwError({ message: 'No user in local storage' })
    }

    const userName = localStorage.getItem('userName')
    const userRoles = localStorage.getItem('userRoles')

    const storedUser: StorageUser = {
      userName,
      userRoles: [],
      token
    }

    return of(storedUser)
  }

  public saveUserToLocalStorage(user: StorageUser): Observable<boolean> {
    localStorage.setItem('userName', 'TESTSTRING!') // user.userName)
    // localStorage.setItem('userRoles', user.userRoles)
    localStorage.setItem('token', 'TESTSTRING!') // user.token)
    return of(true)
  }
}

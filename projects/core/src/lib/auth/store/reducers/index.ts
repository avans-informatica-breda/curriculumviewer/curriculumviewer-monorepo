import { ActionReducerMap, createFeatureSelector } from '@ngrx/store'
import * as fromState from './auth.reducers'

// Hier ontstaat de state tree
export interface AuthState {
  user: fromState.AuthenticationState
}

// De ActionReducerMap zorgt voor typechecking. We kunnen niet zo maar functies toevoegen;
// deze moeten nu uit de Authtate komen.
export const reducers: ActionReducerMap<AuthState> = {
  // koppel users aan de reducer function
  user: fromState.reducer
}

export const getAuthState = createFeatureSelector<AuthState>('auth')

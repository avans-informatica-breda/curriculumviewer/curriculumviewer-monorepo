import * as fromActions from '../actions'

export interface AuthenticationState {
  token: string | null
  userName: string | null
  userRoles: number[]
  isAuthenticated: boolean
  message: string | null
  loaded: boolean
  loading: boolean
}

export const initialState: AuthenticationState = {
  token: null,
  userName: null,
  userRoles: [],
  isAuthenticated: false,
  message: null,
  loaded: false,
  loading: false
}

export function reducer(
  state = initialState,
  action: fromActions.All
): AuthenticationState {
  switch (action.type) {
    //
    //
    //
    case fromActions.AuthActionTypes.IS_LOGGEDIN:
    case fromActions.AuthActionTypes.LOGIN: {
      return {
        ...state,
        loading: true
      }
    }

    //
    //
    //
    case fromActions.AuthActionTypes.IS_LOGGEDIN_SUCCESS:
    case fromActions.AuthActionTypes.LOGIN_SUCCESS: {
      const { userName, userRoles, token } = action.payload
      return {
        ...state,
        token,
        userName,
        userRoles,
        isAuthenticated: true,
        message: null,
        loading: false,
        loaded: true
      }
    }

    //
    //
    //
    case fromActions.AuthActionTypes.IS_LOGGEDIN_FAIL:
    case fromActions.AuthActionTypes.LOGIN_FAIL: {
      const { message } = action.payload
      return {
        ...state,
        message,
        token: null,
        userName: null,
        isAuthenticated: false,
        loading: false,
        loaded: true
      }
    }

    case fromActions.AuthActionTypes.LOGOUT: {
      return {
        ...state,
        loading: true
      }
    }

    case fromActions.AuthActionTypes.LOGOUT_SUCCESS:
    case fromActions.AuthActionTypes.LOGOUT_FAILED: {
      return {
        ...initialState,
        loaded: true
      }
    }

    //
    //
    //
    default: {
      return state
    }
  }
}

// Selector functions: get the pieces of our state that we need
export const getAuthEntity = (state: AuthenticationState) => state
export const getAuthLoading = (state: AuthenticationState) => state.loading
export const getAuthLoaded = (state: AuthenticationState) => state.loaded
export const getIsAuthenticated = (state: AuthenticationState) =>
  state.isAuthenticated
export const getAuthUserName = (state: AuthenticationState) => state.userName
export const getAuthUserRoles = (state: AuthenticationState) => state.userRoles
export const getAuthToken = (state: AuthenticationState) => state.token
export const getAuthMessage = (state: AuthenticationState) => state.message

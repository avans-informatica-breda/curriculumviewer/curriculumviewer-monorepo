import { ActionReducerMap, createFeatureSelector } from '@ngrx/store'
import * as fromPersones from './person.reducer'

// Hier ontstaat de state tree
export interface PersonState {
  personElements: fromPersones.PersonState
}

// De ActionReducerMap zorgt voor typechecking. We kunnen niet zo maar functies toevoegen;
// deze moeten nu uit de Personstate komen.
export const reducers: ActionReducerMap<PersonState> = {
  // koppel items aan de reducer function
  personElements: fromPersones.reducer
}

export const getPersonsState = createFeatureSelector<PersonState>('person')

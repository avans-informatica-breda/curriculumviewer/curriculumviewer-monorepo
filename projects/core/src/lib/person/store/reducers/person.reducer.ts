import * as fromPerson from '../actions'
import { Person } from '../../models'

//
// PersonState interface
//
export interface PersonState {
  entities: {
    [id: number]: Person
  }
  loaded: boolean
  loading: boolean
}

//
// Initialisation
//
export const initialState: PersonState = {
  entities: {},
  loaded: false,
  loading: false
}

//
//
//
export function reducer(
  state: PersonState = initialState,
  action: fromPerson.PersonsAction
): PersonState {
  switch (action.type) {
    case fromPerson.LOAD_PERSONS: {
      return {
        ...state,
        loading: true
      }
    }

    case fromPerson.LOAD_PERSONS_SUCCESS: {
      const personElements = action.payload
      const entities = personElements.reduce(
        (
          entities: { [id: number]: Person },
          personElement: Person
          // entities, personElement
        ) => {
          return {
            ...entities,
            [personElement.id]: personElement
          }
        },
        {
          ...state.entities
        }
      )
      return {
        ...state,
        loading: false,
        loaded: true,
        entities
      }
    }

    case fromPerson.LOAD_PERSONS_FAIL: {
      return {
        ...state,
        loading: false,
        loaded: false
      }
    }

    case fromPerson.CREATE_PERSON_SUCCESS:
    case fromPerson.UPDATE_PERSON_SUCCESS: {
      const personElement = action.payload
      const entities = {
        ...state.entities,
        [personElement.id]: personElement
      }
      return {
        ...state,
        entities
      }
    }

    case fromPerson.DELETE_PERSON_SUCCESS: {
      const personElement = action.payload
      // destructure the personElement from the state object
      // select the personElement by id and name that 'removed'
      // the remainder are the enteties, without the removed.
      // ES6 destructuring syntax!
      const { [personElement.id]: removed, ...entities } = state.entities

      return {
        ...state,
        entities
      }
    }

    default: {
      return state
    }
  }
}

// Selector functions: get the pieces of our state that we need
export const getPersonsEntities = (state: PersonState) => state.entities
export const getPersonsLoading = (state: PersonState) => state.loading
export const getPersonsLoaded = (state: PersonState) => state.loaded

import { Action } from '@ngrx/store'
import { Person } from '../../models'

//
// Load actions
//
export const LOAD_PERSONS = '[Persons] Load Persons'
export const LOAD_PERSONS_FAIL = '[Persons] Load Persons Fail'
export const LOAD_PERSONS_SUCCESS = '[Persons] Load Persons Success'

// Action creators
export class LoadPersons implements Action {
  readonly type = LOAD_PERSONS
}

export class LoadPersonsFail implements Action {
  readonly type = LOAD_PERSONS_FAIL
  constructor(public payload: any) {}
}

export class LoadPersonsSuccess implements Action {
  readonly type = LOAD_PERSONS_SUCCESS
  constructor(public payload: Person[]) {}
}

//
// Create Actions
//
export const CREATE_PERSON = '[Persons] Create Person'
export const CREATE_PERSON_FAIL = '[Persons] Create Person Fail'
export const CREATE_PERSON_SUCCESS = '[Persons] Create Person Success'

// Action creators
export class CreatePerson implements Action {
  readonly type = CREATE_PERSON
  constructor(public payload: Person) {}
}

export class CreatePersonFail implements Action {
  readonly type = CREATE_PERSON_FAIL
  constructor(public payload: any) {}
}

export class CreatePersonSuccess implements Action {
  readonly type = CREATE_PERSON_SUCCESS
  constructor(public payload: Person) {}
}

//
// Update Actions
//
export const UPDATE_PERSON = '[Persons] Update Person'
export const UPDATE_PERSON_FAIL = '[Persons] Update Person Fail'
export const UPDATE_PERSON_SUCCESS = '[Persons] Update Person Success'

// Action creators
export class UpdatePerson implements Action {
  readonly type = UPDATE_PERSON
  constructor(public payload: Person) {}
}

export class UpdatePersonFail implements Action {
  readonly type = UPDATE_PERSON_FAIL
  constructor(public payload: any) {}
}

export class UpdatePersonSuccess implements Action {
  readonly type = UPDATE_PERSON_SUCCESS
  constructor(public payload: Person) {}
}

//
// Delete Actions
//
export const DELETE_PERSON = '[Persons] Delete Person'
export const DELETE_PERSON_FAIL = '[Persons] Delete Person Fail'
export const DELETE_PERSON_SUCCESS = '[Persons] Delete Person Success'

// Action creators
export class DeletePerson implements Action {
  readonly type = DELETE_PERSON
  constructor(public payload: Person) {}
}

export class DeletePersonFail implements Action {
  readonly type = DELETE_PERSON_FAIL
  constructor(public payload: any) {}
}

export class DeletePersonSuccess implements Action {
  readonly type = DELETE_PERSON_SUCCESS
  constructor(public payload: Person) {}
}

// action types
export type PersonsAction =
  | LoadPersons
  | LoadPersonsFail
  | LoadPersonsSuccess
  | CreatePerson
  | CreatePersonFail
  | CreatePersonSuccess
  | UpdatePerson
  | UpdatePersonFail
  | UpdatePersonSuccess
  | DeletePerson
  | DeletePersonFail
  | DeletePersonSuccess

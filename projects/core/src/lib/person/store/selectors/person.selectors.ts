import { createSelector } from '@ngrx/store'

import * as fromRouter from '../../../router/store'
import * as fromFeature from '../reducers'
import * as fromPersons from '../reducers/person.reducer'
import { Person } from '../../models'

//
// Selectors - zijn nodig om door delen van de state tree te navigeren
// They return slices of the state tree.
//
export const getPersonState = createSelector(
  fromFeature.getPersonsState,
  (state: fromFeature.PersonState) => state.personElements
)

export const getPersonsEntities = createSelector(
  getPersonState,
  fromPersons.getPersonsEntities
)

// Get the selected item based on id from the route
export const getSelectedPerson = createSelector(
  getPersonsEntities,
  fromRouter.getRouterState,
  (entities, router: any): Person => {
    const params: any[] = router.state.params.filter((item: any) => item.personId)
    return router.state && params && params[0] && entities[params[0].personId]
  }
)

export const getAllPersons = createSelector(
  getPersonsEntities,
  entities => {
    // Return an array version of our entities object
    // so that we can iterate over it via ngFor in HTML.
    return Object.keys(entities).map(id => entities[parseInt(id, 10)])
  }
)

export const getPersonsLoading = createSelector(
  getPersonState,
  fromPersons.getPersonsLoading
)

export const getPersonsLoaded = createSelector(
  getPersonState,
  fromPersons.getPersonsLoaded
)

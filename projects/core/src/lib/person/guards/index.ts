import { PersonGuard } from './person.guard'
import { PersonExistsGuard } from './person-exists.guard'

export const guards: any[] = [PersonGuard, PersonExistsGuard]

export * from './person.guard'
export * from './person-exists.guard'

import { Injectable } from '@angular/core'
import { Person } from '../models/person.model'
import { EntityService } from '../../entity/services'
import { HttpClient } from '@angular/common/http'
import { CoreConfig } from '../../../core.config'

@Injectable()
export class PersonService extends EntityService<Person> {
  constructor(httpClient: HttpClient, config: CoreConfig) {
    super(httpClient, config.serverUrl, 'persons')
  }
}

import { NgModule } from '@angular/core'
import { RouterModule, Routes } from '@angular/router'

import * as fromGuards from './guards'
// import * as fromEndqualificationsGuards from '../endqualification/guards'
import * as fromContainers from './containers'
import * as fromAuth from '@avans/core'
import * as fromConstants from './person.constants'

const routes: Routes = [
  {
    path: fromConstants.BASE_ROUTE,
    component: fromContainers.PersonComponent,
    canActivate: [fromGuards.PersonGuard],
    children: [
      {
        path: '',
        pathMatch: 'full',
        component: fromContainers.PersonListComponent
      },
      {
        path: 'new',
        component: fromContainers.PersonEditComponent,
        canActivate: [
          fromAuth.IsLoggedInGuard
          // fromEndqualificationsGuards.EndqualificationGuard
        ],
        data: {
          title: 'Nieuwe Persoon',
          optionalLearningline: true
        }
      },
      {
        path: ':personId',
        component: fromContainers.PersonDetailComponent,
        canActivate: [fromGuards.PersonExistsGuard]
      },
      {
        path: ':personId/edit',
        component: fromContainers.PersonEditComponent,
        canActivate: [
          fromGuards.PersonExistsGuard
          // fromEndqualificationsGuards.EndqualificationGuard
        ],
        data: {
          title: 'Wijzig Persoon'
        }
      }
    ]
  },
  {
    path: fromConstants.BASE_ROUTE + '**',
    redirectTo: fromConstants.BASE_ROUTE
  }
]

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PersonRoutingModule {}

import { Component } from '@angular/core'

@Component({
  selector: 'lib-person',
  template: `
    <cv-person-topnav></cv-person-topnav>
    <div class="app">
      <div class="app__content">
        <div class="app__container">
          <router-outlet></router-outlet>
        </div>
      </div>
    </div>
  `
})
export class PersonComponent {}

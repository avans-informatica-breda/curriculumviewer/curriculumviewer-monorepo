export * from './person.component'
export * from './person-detail/person-detail.component'
export * from './person-edit/person-edit.component'
export * from './person-list/person-list.component'

import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core'
import { Router, ActivatedRoute } from '@angular/router'
import { Observable } from 'rxjs'
import { NGXLogger } from 'ngx-logger'
import { Store } from '@ngrx/store'

import { ActionsGroup } from '@avans/ui'

import * as fromStore from '../../store'
import * as fromAuth from '../../../auth/store'
import * as fromConstants from '../../person.constants'

import { Person } from '../../models'

@Component({
  selector: 'lib-person-list',
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './person-list.component.html'
})
export class PersonListComponent implements OnInit {
  // Class name for logging
  static TAG = PersonListComponent.name

  title = 'Docenten'

  // The core element of this Component
  persons$: Observable<Person[]>

  // User authentication role
  userMayEdit$: Observable<boolean>

  columnHeaders = [
    { prop: 'id', name: 'Id', flexGrow: 1 },
    { prop: 'firstName', name: 'Naam', flexGrow: 1 },
    { prop: 'lastName', name: '', flexGrow: 4 }
  ]

  selected = []

  /**
   * Actions op deze page
   */
  actions: ActionsGroup[] = [
    new ActionsGroup({
      title: 'Acties',
      actions: [
        {
          name: 'Element toevoegen',
          routerLink: 'new'
        },
        {
          name: 'Leerdoel overzicht',
          routerLink: 'todo'
        }
      ]
    }),
    new ActionsGroup({
      title: 'Andere acties',
      actions: [
        {
          name: 'Elementen verwijderen',
          routerLink: 'edit'
        }
      ]
    })
  ]

  constructor(
    private readonly store: Store<fromStore.PersonState>,
    private readonly logger: NGXLogger,
    private readonly router: Router,
    private readonly route: ActivatedRoute
  ) {}

  ngOnInit() {
    this.userMayEdit$ = this.store.select(fromAuth.getIsAuthenticated)
    this.persons$ = this.store.select(fromStore.getAllPersons)
  }

  onAddElement() {
    this.logger.debug(PersonListComponent.TAG, 'Add element')
    this.router.navigate([fromConstants.BASE_ROUTE, 'new'])
  }

  onSelect({ selected }) {
    if (selected.length === 1) {
      const element = selected[0] as Person
      this.logger.debug(PersonListComponent.TAG, 'onSelect id = ' + element.id)
      this.router.navigate([element.id], { relativeTo: this.route })
    }
  }
}

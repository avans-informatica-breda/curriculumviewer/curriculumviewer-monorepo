import {
  Component,
  Input,
  Output,
  EventEmitter,
  OnInit,
  OnChanges,
  SimpleChanges,
  ChangeDetectionStrategy
} from '@angular/core'
import { FormControl, FormGroup, FormBuilder, Validators } from '@angular/forms'

import { Person } from '../../models/person.model'

@Component({
  selector: 'lib-person-form',
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './person-form.component.html'
})
export class PersonFormComponent implements OnInit, OnChanges {
  exists = false

  @Input() person: Person

  @Output() selected = new EventEmitter<Person>()
  @Output() create = new EventEmitter<Person>()
  @Output() update = new EventEmitter<Person>()
  @Output() remove = new EventEmitter<Person>()
  @Output() cancel = new EventEmitter<void>()

  form = this.fb.group({
    name: ['', Validators.required],
    shortDescription: ['', Validators.required],
    longDescription: [''],
    administrativeRemark: ['']
  })

  constructor(private fb: FormBuilder) {}

  ngOnInit(): void {}

  get nameControl() {
    return this.form.get('name') as FormControl
  }

  get nameControlInvalid() {
    return this.nameControl.hasError('required') && this.nameControl.touched
  }

  get shortDescriptionControl() {
    return this.form.get('shortDescription') as FormControl
  }

  get shortDescriptionControlInvalid() {
    return this.shortDescriptionControl.hasError('required') && this.shortDescriptionControl.touched
  }

  get longDescriptionControl() {
    return this.form.get('longDescription') as FormControl
  }

  get longDescriptionControlInvalid() {
    return this.longDescriptionControl.hasError('required') && this.longDescriptionControl.touched
  }

  get administrativeRemark() {
    return this.form.get('administrativeRemark') as FormControl
  }

  ngOnChanges(changes: SimpleChanges) {
    if (this.person && this.person.id) {
      this.exists = true
      this.form.patchValue(this.person)
    }
    // this.form
    //   .get('toppings')
    //   .valueChanges.pipe(
    //     map(toppings => toppings.map((topping: Topping) => topping.id))
    //   )
    //   .subscribe(value => this.selected.emit(value))
  }

  createItem(form: FormGroup) {
    console.log('createItem')
    const { value, valid } = form
    if (valid) {
      this.create.emit(value)
    }
  }

  updateItem(form: FormGroup) {
    const { value, valid, touched } = form
    if (touched && valid) {
      this.update.emit({ ...this.person, ...value })
    }
  }

  removeItem(form: FormGroup) {
    const { value } = form
    this.remove.emit({ ...this.person, ...value })
  }

  onCancel() {
    this.cancel.emit()
  }
}

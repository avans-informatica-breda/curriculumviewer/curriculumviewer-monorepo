import { Component, Output, EventEmitter, OnInit, ChangeDetectionStrategy, Input } from '@angular/core'
import { Store } from '@ngrx/store'

import { Person } from '../../models'
import * as fromStore from '../../store'
import { Observable } from 'rxjs'

@Component({
  selector: 'lib-person-selector',
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './person-selector.component.html'
})
export class PersonSelectorComponent implements OnInit {
  @Input() currentPersonId: number
  @Output() selected = new EventEmitter<number>()

  persons$: Observable<Person[]>
  selectedPerson: number

  constructor(private readonly store: Store<fromStore.PersonState>) {}

  ngOnInit(): void {
    this.persons$ = this.store.select(fromStore.getAllPersons)
    this.selectedPerson = this.currentPersonId
  }

  onSelectionChanged() {
    this.selected.emit(this.selectedPerson)
  }
}

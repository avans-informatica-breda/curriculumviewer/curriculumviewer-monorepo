import { NgModule, ModuleWithProviders } from '@angular/core'
import { HttpModule } from '@angular/http'
import { CommonModule } from '@angular/common'
import { FormsModule, ReactiveFormsModule } from '@angular/forms'

import { StoreModule } from '@ngrx/store'
import { EffectsModule } from '@ngrx/effects'
import { LoggerModule, NgxLoggerLevel } from 'ngx-logger'
import { QuillModule } from 'ngx-quill'

import { AlertEffects } from './lib/alert/store/effects/alert.effects'
import { RouterEffects } from './lib/router/store/effects/router.effects'
import { AuthEffects } from './lib/auth/store/effects/auth.effects'
import { PersonEffects } from './lib/person/store/effects/person.effect'
import { ContextEffects } from './lib/context/store/effects/context.effect'

import { reducers as authReducer } from './lib/auth/store/reducers/index'
import { reducers as personReducer } from './lib/person/store/reducers/index'
import { reducers as contextReducer } from './lib/context/store/reducers/index'
import { reducers as routerReducer, CustomSerializer } from './lib/router/store/reducers/router.reducers'

import { IsLoggedInGuard } from './lib/auth/guards/is-logged-in.guard'
import { CanDeactivateGuard } from './lib/auth/guards/can-deactivate.guard'
import { AuthService } from './lib/auth/services/auth.service'

import { YearSelectorComponent } from './lib/context/components/year-selector/year-selector.component'
import { BaseComponent } from './lib/entity/components/basecomponent/base.component'
import { EntityDetailComponent } from './lib/entity/components/entity-detail/entity-detail.component'

import { PersonFormComponent } from './lib/person/components/person-form/person-form.component'
import { PersonSelectorComponent } from './lib/person/components/person-selector/person-selector.component'
import { PersonTopnavComponent } from './lib/person/components/person-topnav/person-topnav.component'
import { PersonService } from './lib/person/services'

import { LoginComponent } from './lib/users/components/login/login.component'
import { UsersComponent } from './lib/users/containers/user.component'
import { UsersDashboardComponent } from './lib/users/containers/user-dashboard/user-dashboard.component'
import { UserDetailComponent } from './lib/users/containers/user-detail/user-detail.component'
import { UserListComponent } from './lib/users/containers/user-list/user-list.component'
import { UserItemComponent } from './lib/users/containers/user-list/user-item/user-item.component'
import { UserEditComponent } from './lib/users/components/user-edit/user-edit.component'
import { UserNotFoundComponent } from './lib/users/components/user-notfound/user.notfound.component'
import { UserProfileComponent } from './lib/users/components/user-profile/user-profile.component'
import { UserRoleNamePipe } from './lib/users/models/user.model'
import { RouterModule } from '@angular/router'
import { UserTopnavComponent, NotFoundComponent, NotAllowedComponent } from './lib/users/components'
import { CoreConfig } from './core.config'
import { RouterStateSerializer } from '@ngrx/router-store'
import { PersonGuard } from './lib/person/guards'

@NgModule({
  imports: [
    FormsModule,
    ReactiveFormsModule,
    RouterModule,
    CommonModule,
    QuillModule,
    HttpModule,
    LoggerModule.forRoot({
      serverLoggingUrl: 'http://localhost:3000/api/logs',
      level: NgxLoggerLevel.DEBUG,
      serverLogLevel: NgxLoggerLevel.OFF,
      disableConsoleLogging: true,
      httpResponseType: 'text' as 'json'
    }),
    StoreModule.forFeature('auth', authReducer),
    StoreModule.forFeature('person', personReducer),
    StoreModule.forFeature('context', contextReducer),
    EffectsModule.forFeature([ContextEffects, AuthEffects, PersonEffects, RouterEffects, AlertEffects])
  ],
  declarations: [
    PersonSelectorComponent,
    PersonFormComponent,
    PersonTopnavComponent,
    YearSelectorComponent,
    EntityDetailComponent,
    BaseComponent,
    NotFoundComponent,
    NotAllowedComponent,
    LoginComponent,
    UserNotFoundComponent,
    UsersComponent,
    UsersDashboardComponent,
    UserProfileComponent,
    UserEditComponent,
    UserDetailComponent,
    UserListComponent,
    UserItemComponent,
    UserTopnavComponent,
    UserRoleNamePipe
  ],
  providers: [
    AuthService,
    PersonGuard,
    PersonService,
    IsLoggedInGuard,
    { provide: RouterStateSerializer, useClass: CustomSerializer }
  ],
  exports: [
    PersonSelectorComponent,
    PersonFormComponent,
    PersonTopnavComponent,
    YearSelectorComponent,
    EntityDetailComponent,
    BaseComponent,
    LoginComponent,
    NotFoundComponent,
    NotAllowedComponent,
    UsersComponent,
    UsersDashboardComponent,
    UserProfileComponent,
    UserEditComponent,
    UserDetailComponent,
    UserListComponent,
    UserItemComponent,
    UserTopnavComponent,
    UserNotFoundComponent
  ]
})
export class CoreModule {
  static forRoot(config: CoreConfig | null | undefined): ModuleWithProviders {
    return {
      ngModule: CoreModule,
      providers: [
        { provide: CoreConfig, useValue: config || {} },
        { provide: RouterStateSerializer, useClass: CustomSerializer }
      ]
    }
  }
}

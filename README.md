# Avans Informatica Breda - Angular monorepo project structure

This repository holds the source code for one of many curriculum viewer projects. The goal of this project is to provide insight into our curriculum. The projects in this repository follow the Angular mono-repository file structure.

## Requirements

In order to run this system locally, the following applications are required.

- Git
- Node 11
- npm ^6
- Angular CLI

## Building

To install the required dependencies, open a console window and navigate to the cloned root of this repository. Then:

```
npm install
```

To build the system, type:

```
npm build
```

This uses Lerna to build the `core` and `ui` libraries. The result is a `dist` folder in the root of your repo.

## Testing

To run the tests, type

```
npm test
```

Testing is not exhausive at all; the system only shows how to implement the simplest tests.

## Running the server

Type `npm start` or `ng serve` to run the frontend in dev-mode. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

Run `npm run build` to build the project libraries, and `npm run build:prod` to build the frontend. The build artifacts will be stored in the `dist/` directory. The packages in this folder are ready to publish on NPM if required.

## Further help

Useful reading:

- [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
- [HowTo Integrate Jest into Angular](https://blog.angularindepth.com/integrate-jest-into-an-angular-application-and-library-163b01d977ce)
- [How to build and publish an Angular module](https://medium.com/@cyrilletuzi/how-to-build-and-publish-an-angular-module-7ad19c0b4464)

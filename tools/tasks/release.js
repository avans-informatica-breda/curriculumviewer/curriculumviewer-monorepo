'use strict'
var __importDefault =
  (this && this.__importDefault) ||
  function(mod) {
    return mod && mod.__esModule ? mod : { default: mod }
  }
Object.defineProperty(exports, '__esModule', { value: true })
const npm_publish_1 = __importDefault(require('@lerna/npm-publish'))
const path_1 = require('path')
const child_process_1 = require('child_process')
const fs_extra_1 = require('fs-extra')
function updateDistPackageJson(directory, newDirectory) {
  const srcPkgJsonPath = path_1.resolve(directory, 'package.json')
  const distPkgJsonPath = path_1.resolve(newDirectory, 'package.json')
  const srcPkgJson = fs_extra_1.readJsonSync(srcPkgJsonPath)
  // update the dist package json
  const { version, dependencies, peerDependencies, devDependencies } = srcPkgJson
  const distPkgJson = Object.assign({}, fs_extra_1.readJsonSync(distPkgJsonPath), {
    version,
    dependencies,
    devDependencies,
    peerDependencies
  })
  fs_extra_1.writeJsonSync(distPkgJsonPath, distPkgJson, { spaces: 2 })
}
// fail ci build if there is nothing to be released
try {
  child_process_1.execSync('lerna updated')
} catch (_a) {
  console.error('No libraries to release.')
  process.exit(1)
}
const originalNpmPack = npm_publish_1.default.npmPack
const npmPack = (rootManifest, packages, _options) => {
  // modify the packages array in place
  // because pkg.location is readonly so we can't modify each pkg
  // and the packages array needs to be shared between the two calls into the npm-publish helper library:
  // - first, npmPack, which attaches pkg.tarball = $(npm pack --json pkg.location) to each
  // - second, npmPublish, which reads the same (===) array and expects to find pkg.tarball.filename,
  //	 and runs `npm publish ${pkg.tarball.filename}`
  // it is a little bad that the lib depends on array mutations happening between otherwise unrelated calls, but what can you do
  for (let i = 0; i < packages.length; i++) {
    let pkg = packages[i]
    const location = path_1.join(pkg.location, 'dist')
    updateDistPackageJson(pkg.location, location)
    // you could use log output to make a test suite for the monkey-patching
    packages[i] = Object.assign({}, pkg, {
      location,
      // and also there is some weirdness with ...pkg not including all properties
      // this solves it for whatever reason
      rootPath: pkg.rootPath,
      version: pkg.version
    })
  }
  return originalNpmPack(rootManifest, packages)
}
// somewhat re-implement https://github.com/lerna/lerna/blob/master/utils/npm-publish/npm-publish.js
const modulePath = path_1.resolve('./node_modules/@lerna/npm-publish/npm-publish.js')
const cachedModuleExports = require('module')._cache[modulePath].exports
// just monkey-patching npmPack is not enough or even useful -- that API is not used directly,
// it is only used through makePacker
cachedModuleExports.npmPack = npmPack
// original redundantly specifies opts here
// passing no opts will cause originalNpmPack to create opts = makePackOptions(rootManifest)
// which is what we want
cachedModuleExports.makePacker = rootManifest => packages => npmPack(rootManifest, packages)
process.argv.splice(0, 2, 'publish')
const publishCmd = require('@lerna/publish/command')
require('@lerna/cli')()
  .command(publishCmd)
  .parse(process.argv)

'use strict'
var __awaiter =
  (this && this.__awaiter) ||
  function(thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function(resolve, reject) {
      function fulfilled(value) {
        try {
          step(generator.next(value))
        } catch (e) {
          reject(e)
        }
      }
      function rejected(value) {
        try {
          step(generator['throw'](value))
        } catch (e) {
          reject(e)
        }
      }
      function step(result) {
        result.done
          ? resolve(result.value)
          : new P(function(resolve) {
              resolve(result.value)
            }).then(fulfilled, rejected)
      }
      step((generator = generator.apply(thisArg, _arguments || [])).next())
    })
  }
Object.defineProperty(exports, '__esModule', { value: true })
const ng_packagr_1 = require('ng-packagr')
const path_1 = require('path')
const rxjs_1 = require('rxjs')
const operators_1 = require('rxjs/operators')
const scss_bundle_1 = require('scss-bundle')
const fs_extra_1 = require('fs-extra')
exports.isWatchMode = !!process.env.WATCH_MODE
/** Bundles all SCSS files into a single file */
function bundleScss() {
  return __awaiter(this, void 0, void 0, function*() {
    console.info('Starting Bundling SCSS')
    const { found, bundledContent, imports } = yield new scss_bundle_1.Bundler().bundle(
      './src/_theming.scss',
      ['./!(dist|node_modules)/**/*.scss'],
      undefined,
      ['^~']
    )
    if (imports) {
      const cwd = process.cwd()
      const filesNotFound = imports
        .filter(x => !x.found && !x.ignored)
        .map(x => path_1.relative(cwd, x.filePath))
      if (filesNotFound.length) {
        console.error(`SCSS imports failed \n\n${filesNotFound.join('\n - ')}\n`)
        throw new Error('One or more SCSS imports failed')
      }
    }
    if (found) {
      yield fs_extra_1.writeFile('./dist/_theming.scss', bundledContent)
    }
    console.info('Finished Bundling SCSS')
  })
}
ng_packagr_1
  .ngPackagr()
  .forProject('./ng-package.js')
  .withTsConfig('../../tsconfig.build.json')
  .withOptions({
    watch: exports.isWatchMode
  })
  .buildAsObservable()
  .pipe(
    operators_1.switchMap(bundleScss),
    operators_1.catchError(() => {
      if (!exports.isWatchMode) {
        process.exitCode = 1
      }
      return rxjs_1.NEVER
    })
  )
  .subscribe()
